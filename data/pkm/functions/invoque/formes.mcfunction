execute as @e[tag=!combat,tag=peutMega] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Mega Gemme\",\"color\":\"light_purple\"}"}}}}] run tag @s add megaFormes
execute as @e[tag=!combat,tag=estMega] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Mega Gemme\",\"color\":\"light_purple\"}"}}}}] run tag @s add megaFormes
execute as @e[tag=megaFormes] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Mega Gemme\",\"color\":\"light_purple\"}"}}}}]


execute as @e[tag=dracaufeu,tag=!combat,tag=peutMega,tag=captPokeball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_pokeball
execute as @e[tag=dracaufeu,tag=!combat,tag=peutMega,tag=captMasterball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_masterball
execute as @e[tag=dracaufeu,tag=!combat,tag=peutMega,tag=captHonorball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_honorball
execute as @e[tag=dracaufeu,tag=!combat,tag=peutMega,tag=captSuperball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_superball
execute as @e[tag=dracaufeu,tag=!combat,tag=peutMega,tag=captHyperball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_hyperball

execute as @e[tag=laggron,tag=!combat,tag=peutMega,tag=captPokeball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_laggron/mega_laggron_pokeball
execute as @e[tag=laggron,tag=!combat,tag=peutMega,tag=captMasterball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_laggron/mega_laggron_masterball
execute as @e[tag=laggron,tag=!combat,tag=peutMega,tag=captHonorball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_laggron/mega_laggron_honorball
execute as @e[tag=laggron,tag=!combat,tag=peutMega,tag=captSuperball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_laggron/mega_laggron_superball
execute as @e[tag=laggron,tag=!combat,tag=peutMega,tag=captHyperball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_laggron/mega_laggron_hyperball

execute as @e[tag=brasegali,tag=!combat,tag=peutMega,tag=captPokeball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_pokeball
execute as @e[tag=brasegali,tag=!combat,tag=peutMega,tag=captMasterball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_masterball
execute as @e[tag=brasegali,tag=!combat,tag=peutMega,tag=captHonorball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_honorball
execute as @e[tag=brasegali,tag=!combat,tag=peutMega,tag=captSuperball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_superball
execute as @e[tag=brasegali,tag=!combat,tag=peutMega,tag=captHyperball,tag=megaFormes] at @s run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_hyperball


execute as @e[tag=dracaufeu,tag=!combat,tag=estMega,tag=captPokeball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu/dracaufeu_pokeball
execute as @e[tag=dracaufeu,tag=!combat,tag=estMega,tag=captMasterball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu/dracaufeu_masterball
execute as @e[tag=dracaufeu,tag=!combat,tag=estMega,tag=captHonorball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu/dracaufeu_honorball
execute as @e[tag=dracaufeu,tag=!combat,tag=estMega,tag=captSuperball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu/dracaufeu_superball
execute as @e[tag=dracaufeu,tag=!combat,tag=estMega,tag=captHyperball,tag=megaFormes] at @s run function pkm:summon/dresseur/dracaufeu/dracaufeu_hyperball

execute as @e[tag=laggron,tag=!combat,tag=estMega,tag=captPokeball,tag=megaFormes] at @s run function pkm:summon/dresseur/laggron/laggron_pokeball
execute as @e[tag=laggron,tag=!combat,tag=estMega,tag=captMasterball,tag=megaFormes] at @s run function pkm:summon/dresseur/laggron/laggron_masterball
execute as @e[tag=laggron,tag=!combat,tag=estMega,tag=captHonorball,tag=megaFormes] at @s run function pkm:summon/dresseur/laggron/laggron_honorball
execute as @e[tag=laggron,tag=!combat,tag=estMega,tag=captSuperball,tag=megaFormes] at @s run function pkm:summon/dresseur/laggron/laggron_superball
execute as @e[tag=laggron,tag=!combat,tag=estMega,tag=captHyperball,tag=megaFormes] at @s run function pkm:summon/dresseur/laggron/laggron_hyperball

execute as @e[tag=brasegali,tag=!combat,tag=estMega,tag=captPokeball,tag=megaFormes] at @s run function pkm:summon/dresseur/brasegali/brasegali_pokeball
execute as @e[tag=brasegali,tag=!combat,tag=estMega,tag=captMasterball,tag=megaFormes] at @s run function pkm:summon/dresseur/brasegali/brasegali_masterball
execute as @e[tag=brasegali,tag=!combat,tag=estMega,tag=captHonorball,tag=megaFormes] at @s run function pkm:summon/dresseur/brasegali/brasegali_honorball
execute as @e[tag=brasegali,tag=!combat,tag=estMega,tag=captSuperball,tag=megaFormes] at @s run function pkm:summon/dresseur/brasegali/brasegali_superball
execute as @e[tag=brasegali,tag=!combat,tag=estMega,tag=captHyperball,tag=megaFormes] at @s run function pkm:summon/dresseur/brasegali/brasegali_hyperball




execute at @e[tag=megaFormes] as @p run function give:item/mega_gemme
kill @e[tag=megaFormes]

#################################Primal##########################
execute as @e[tag=!combat,tag=peutPrimal,tag=groudon] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Feu\",\"color\":\"green\"}"}}}}] run tag @s add primalGroudon
execute as @e[tag=!combat,tag=estPrimal,tag=groudon] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Feu\",\"color\":\"green\"}"}}}}] run tag @s add primalGroudon
execute as @e[tag=primalGroudon] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Feu\",\"color\":\"green\"}"}}}}]


execute as @e[tag=groudon,tag=!combat,tag=peutPrimal,tag=captPokeball,tag=primalGroudon] at @s run function pkm:summon/dresseur/primo_groudon/primo_groudon_pokeball
execute as @e[tag=groudon,tag=!combat,tag=peutPrimal,tag=captMasterball,tag=primalGroudon] at @s run function pkm:summon/dresseur/primo_groudon/primo_groudon_masterball
execute as @e[tag=groudon,tag=!combat,tag=peutPrimal,tag=captHonorball,tag=primalGroudon] at @s run function pkm:summon/dresseur/primo_groudon/primo_groudon_honorball
execute as @e[tag=groudon,tag=!combat,tag=peutPrimal,tag=captSuperball,tag=primalGroudon] at @s run function pkm:summon/dresseur/primo_groudon/primo_groudon_superball
execute as @e[tag=groudon,tag=!combat,tag=peutPrimal,tag=captHyperball,tag=primalGroudon] at @s run function pkm:summon/dresseur/primo_groudon/primo_groudon_hyperball

execute as @e[tag=groudon,tag=!combat,tag=estPrimal,tag=captPokeball,tag=primalGroudon] at @s run function pkm:summon/dresseur/groudon/groudon_pokeball
execute as @e[tag=groudon,tag=!combat,tag=estPrimal,tag=captMasterball,tag=primalGroudon] at @s run function pkm:summon/dresseur/groudon/groudon_masterball
execute as @e[tag=groudon,tag=!combat,tag=estPrimal,tag=captHonorball,tag=primalGroudon] at @s run function pkm:summon/dresseur/groudon/groudon_honorball
execute as @e[tag=groudon,tag=!combat,tag=estPrimal,tag=captSuperball,tag=primalGroudon] at @s run function pkm:summon/dresseur/groudon/groudon_superball
execute as @e[tag=groudon,tag=!combat,tag=estPrimal,tag=captHyperball,tag=primalGroudon] at @s run function pkm:summon/dresseur/groudon/groudon_hyperball


execute at @e[tag=primalGroudon] as @p run function give:item/orbe/orbe_feu
kill @e[tag=primalGroudon]



execute as @e[tag=!combat,tag=peutPrimal,tag=kyogre] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Eau\",\"color\":\"green\"}"}}}}] run tag @s add primalKyogre
execute as @e[tag=!combat,tag=estPrimal,tag=kyogre] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Eau\",\"color\":\"green\"}"}}}}] run tag @s add primalKyogre
execute as @e[tag=primalKyogre] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Eau\",\"color\":\"green\"}"}}}}]


execute as @e[tag=kyogre,tag=!combat,tag=peutPrimal,tag=captPokeball,tag=primalKyogre] at @s run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_pokeball
execute as @e[tag=kyogre,tag=!combat,tag=peutPrimal,tag=captMasterball,tag=primalKyogre] at @s run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_masterball
execute as @e[tag=kyogre,tag=!combat,tag=peutPrimal,tag=captHonorball,tag=primalKyogre] at @s run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_honorball
execute as @e[tag=kyogre,tag=!combat,tag=peutPrimal,tag=captSuperball,tag=primalKyogre] at @s run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_superball
execute as @e[tag=kyogre,tag=!combat,tag=peutPrimal,tag=captHyperball,tag=primalKyogre] at @s run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_hyperball

execute as @e[tag=kyogre,tag=!combat,tag=estPrimal,tag=captPokeball,tag=primalKyogre] at @s run function pkm:summon/dresseur/kyogre/kyogre_pokeball
execute as @e[tag=kyogre,tag=!combat,tag=estPrimal,tag=captMasterball,tag=primalKyogre] at @s run function pkm:summon/dresseur/kyogre/kyogre_masterball
execute as @e[tag=kyogre,tag=!combat,tag=estPrimal,tag=captHonorball,tag=primalKyogre] at @s run function pkm:summon/dresseur/kyogre/kyogre_honorball
execute as @e[tag=kyogre,tag=!combat,tag=estPrimal,tag=captSuperball,tag=primalKyogre] at @s run function pkm:summon/dresseur/kyogre/kyogre_superball
execute as @e[tag=kyogre,tag=!combat,tag=estPrimal,tag=captHyperball,tag=primalKyogre] at @s run function pkm:summon/dresseur/kyogre/kyogre_hyperball

execute at @e[tag=primalKyogre] as @p run function give:item/orbe/orbe_eau
kill @e[tag=primalKyogre]







execute as @e[tag=!combat,tag=necrozma_ailes] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Dragon\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaU
execute as @e[tag=!combat,tag=necrozma_criniere] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Dragon\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaU
execute as @e[tag=!combat,tag=ultra_necrozma] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Dragon\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaU
execute as @e[tag=necrozmaU] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Dragon\",\"color\":\"green\"}"}}}}]


execute as @e[tag=necrozma_ailes,tag=!combat,tag=captPokeball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_pokeball
execute as @e[tag=necrozma_ailes,tag=!combat,tag=captMasterball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_masterball
execute as @e[tag=necrozma_ailes,tag=!combat,tag=captHonorball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_honorball
execute as @e[tag=necrozma_ailes,tag=!combat,tag=captSuperball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_superball
execute as @e[tag=necrozma_ailes,tag=!combat,tag=captHyperball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_hyperball

execute as @e[tag=ultra_necrozma,tag=!combat,tag=captPokeball,tag=necrozmaU,tag=ailes] at @s run function pkm:summon/dresseur/necrozma_ailes/necrozma_ailes_pokeball
execute as @e[tag=ultra_necrozma,tag=!combat,tag=captMasterball,tag=necrozmaU,tag=ailes] at @s run function pkm:summon/dresseur/necrozma_ailes/necrozma_ailes_masterball
execute as @e[tag=ultra_necrozma,tag=!combat,tag=captHonorball,tag=necrozmaU,tag=ailes] at @s run function pkm:summon/dresseur/necrozma_ailes/necrozma_ailes_honorball
execute as @e[tag=ultra_necrozma,tag=!combat,tag=captSuperball,tag=necrozmaU,tag=ailes] at @s run function pkm:summon/dresseur/necrozma_ailes/necrozma_ailes_superball
execute as @e[tag=ultra_necrozma,tag=!combat,tag=captHyperball,tag=necrozmaU,tag=ailes] at @s run function pkm:summon/dresseur/necrozma_ailes/necrozma_ailes_hyperball




execute as @e[tag=necrozma_criniere,tag=!combat,tag=captPokeball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_pokeball
execute as @e[tag=necrozma_criniere,tag=!combat,tag=captMasterball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_masterball
execute as @e[tag=necrozma_criniere,tag=!combat,tag=captHonorball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_honorball
execute as @e[tag=necrozma_criniere,tag=!combat,tag=captSuperball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_superball
execute as @e[tag=necrozma_criniere,tag=!combat,tag=captHyperball,tag=necrozmaU] at @s run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_hyperball

execute as @e[tag=ultra_necrozma,tag=!combat,tag=captPokeball,tag=necrozmaU,tag=criniere] at @s run function pkm:summon/dresseur/necrozma_criniere/necrozma_criniere_pokeball
execute as @e[tag=ultra_necrozma,tag=!combat,tag=captMasterball,tag=necrozmaU,tag=criniere] at @s run function pkm:summon/dresseur/necrozma_criniere/necrozma_criniere_masterball
execute as @e[tag=ultra_necrozma,tag=!combat,tag=captHonorball,tag=necrozmaU,tag=criniere] at @s run function pkm:summon/dresseur/necrozma_criniere/necrozma_criniere_honorball
execute as @e[tag=ultra_necrozma,tag=!combat,tag=captSuperball,tag=necrozmaU,tag=criniere] at @s run function pkm:summon/dresseur/necrozma_criniere/necrozma_criniere_superball
execute as @e[tag=ultra_necrozma,tag=!combat,tag=captHyperball,tag=necrozmaU,tag=criniere] at @s run function pkm:summon/dresseur/necrozma_criniere/necrozma_criniere_hyperball



execute as @e[tag=necrozma_ailes,tag=!combat,tag=necrozmaU] at @s run tag @e[tag=ultra_necrozma,tag=!ailes,tag=!criniere,distance=..1] add ailes
execute as @e[tag=necrozma_criniere,tag=!combat,tag=necrozmaU] at @s run tag @e[tag=ultra_necrozma,tag=!ailes,tag=!criniere,distance=..1] add criniere
execute at @e[tag=necrozmaU] as @p run function give:item/orbe/orbe_dragon
kill @e[tag=necrozmaU]


##################################Fusion##########################
#ne peut pas detransformer

execute as @e[tag=kyurem,tag=!combat,tag=captHonorball] at @s if entity @e[tag=zekrom,tag=captHonorball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremHonorN
execute as @e[tag=kyurem,tag=!combat,tag=captPokeball] at @s if entity @e[tag=zekrom,tag=captPokeball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremPokeN
execute as @e[tag=kyurem,tag=!combat,tag=captMasterball] at @s if entity @e[tag=zekrom,tag=captMasterball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremMasterN
execute as @e[tag=kyurem,tag=!combat,tag=captSuperball] at @s if entity @e[tag=zekrom,tag=captSuperball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremSuperN
execute as @e[tag=kyurem,tag=!combat,tag=captHyperball] at @s if entity @e[tag=zekrom,tag=captHyperball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremHyperN


execute as @e[tag=kyurem,tag=!combat,tag=kyuremHonorN] run function give:spawn/kyurem_noir/kyurem_noir_honorball
execute as @e[tag=kyurem,tag=!combat,tag=kyuremPokeN] run function give:spawn/kyurem_noir/kyurem_noir_pokeball
execute as @e[tag=kyurem,tag=!combat,tag=kyuremMasterN] run function give:spawn/kyurem_noir/kyurem_noir_masterball
execute as @e[tag=kyurem,tag=!combat,tag=kyuremSuperN] run function give:spawn/kyurem_noir/kyurem_noir_superball
execute as @e[tag=kyurem,tag=!combat,tag=kyuremHyperN] run function give:spawn/kyurem_noir/kyurem_noir_hyperball


execute at @e[tag=kyurem,tag=!combat,tag=kyuremHonorN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremPokeN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremMasterN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremSuperN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremHyperN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1

execute at @e[tag=kyurem,tag=!combat,tag=kyuremHonorN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremPokeN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremMasterN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremSuperN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremHyperN] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1

execute as @e[tag=kyurem,tag=!combat,tag=kyuremHonorN] at @s run kill @e[tag=zekrom,distance=..2]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremPokeN] at @s run kill @e[tag=zekrom,distance=..2]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremMasterN] at @s run kill @e[tag=zekrom,distance=..2]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremSuperN] at @s run kill @e[tag=zekrom,distance=..2]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremHyperN] at @s run kill @e[tag=zekrom,distance=..2]

execute as @e[tag=kyurem,tag=!combat,tag=kyuremHonorN] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremPokeN] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremMasterN] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremSuperN] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremHyperN] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]

kill @e[tag=kyurem,tag=!combat,tag=kyuremHonorN]
kill @e[tag=kyurem,tag=!combat,tag=kyuremPokeN]
kill @e[tag=kyurem,tag=!combat,tag=kyuremMasterN]
kill @e[tag=kyurem,tag=!combat,tag=kyuremSuperN]
kill @e[tag=kyurem,tag=!combat,tag=kyuremHyperN]




execute as @e[tag=kyurem,tag=!combat,tag=captHonorball] at @s if entity @e[tag=reshiram,tag=captHonorball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremHonorB
execute as @e[tag=kyurem,tag=!combat,tag=captPokeball] at @s if entity @e[tag=reshiram,tag=captPokeball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremPokeB
execute as @e[tag=kyurem,tag=!combat,tag=captMasterball] at @s if entity @e[tag=reshiram,tag=captMasterball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremMasterB
execute as @e[tag=kyurem,tag=!combat,tag=captSuperball] at @s if entity @e[tag=reshiram,tag=captSuperball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremSuperB
execute as @e[tag=kyurem,tag=!combat,tag=captHyperball] at @s if entity @e[tag=reshiram,tag=captHyperball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add kyuremHyperB


execute as @e[tag=kyurem,tag=!combat,tag=kyuremHonorB] run function give:spawn/kyurem_blanc/kyurem_blanc_honorball
execute as @e[tag=kyurem,tag=!combat,tag=kyuremPokeB] run function give:spawn/kyurem_blanc/kyurem_blanc_pokeball
execute as @e[tag=kyurem,tag=!combat,tag=kyuremMasterB] run function give:spawn/kyurem_blanc/kyurem_blanc_masterball
execute as @e[tag=kyurem,tag=!combat,tag=kyuremSuperB] run function give:spawn/kyurem_blanc/kyurem_blanc_superball
execute as @e[tag=kyurem,tag=!combat,tag=kyuremHyperB] run function give:spawn/kyurem_blanc/kyurem_blanc_hyperball


execute at @e[tag=kyurem,tag=!combat,tag=kyuremHonorB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremPokeB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremMasterB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremSuperB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremHyperB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1

execute at @e[tag=kyurem,tag=!combat,tag=kyuremHonorB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremPokeB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremMasterB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremSuperB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute at @e[tag=kyurem,tag=!combat,tag=kyuremHyperB] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1

execute as @e[tag=kyurem,tag=!combat,tag=kyuremHonorB] at @s run kill @e[tag=reshiram,distance=..2]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremPokeB] at @s run kill @e[tag=reshiram,distance=..2]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremMasterB] at @s run kill @e[tag=reshiram,distance=..2]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremSuperB] at @s run kill @e[tag=reshiram,distance=..2]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremHyperB] at @s run kill @e[tag=reshiram,distance=..2]

execute as @e[tag=kyurem,tag=!combat,tag=kyuremHonorB] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremPokeB] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremMasterB] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremSuperB] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]
execute as @e[tag=kyurem,tag=!combat,tag=kyuremHyperB] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]

kill @e[tag=kyurem,tag=!combat,tag=kyuremHonorB]
kill @e[tag=kyurem,tag=!combat,tag=kyuremPokeB]
kill @e[tag=kyurem,tag=!combat,tag=kyuremMasterB]
kill @e[tag=kyurem,tag=!combat,tag=kyuremSuperB]
kill @e[tag=kyurem,tag=!combat,tag=kyuremHyperB]










execute as @e[tag=necrozma,tag=!combat,tag=captHonorball] at @s if entity @e[tag=lunala,tag=captHonorball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaHonorA
execute as @e[tag=necrozma,tag=!combat,tag=captPokeball] at @s if entity @e[tag=lunala,tag=captPokeball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaPokeA
execute as @e[tag=necrozma,tag=!combat,tag=captMasterball] at @s if entity @e[tag=lunala,tag=captMasterball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaMasterA
execute as @e[tag=necrozma,tag=!combat,tag=captSuperball] at @s if entity @e[tag=lunala,tag=captSuperball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaSuperA
execute as @e[tag=necrozma,tag=!combat,tag=captHyperball] at @s if entity @e[tag=lunala,tag=captHyperball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaHyperA


execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHonorA] run function give:spawn/necrozma_ailes/necrozma_ailes_honorball
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaPokeA] run function give:spawn/necrozma_ailes/necrozma_ailes_pokeball
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaMasterA] run function give:spawn/necrozma_ailes/necrozma_ailes_masterball
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaSuperA] run function give:spawn/necrozma_ailes/necrozma_ailes_superball
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHyperA] run function give:spawn/necrozma_ailes/necrozma_ailes_hyperball


execute at @e[tag=necrozma,tag=!combat,tag=necrozmaHonorA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaPokeA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaMasterA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaSuperA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaHyperA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1

execute at @e[tag=necrozma,tag=!combat,tag=necrozmaHonorA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaPokeA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaMasterA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaSuperA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaHyperA] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1

execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHonorA] at @s run kill @e[tag=lunala,distance=..2]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaPokeA] at @s run kill @e[tag=lunala,distance=..2]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaMasterA] at @s run kill @e[tag=lunala,distance=..2]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaSuperA] at @s run kill @e[tag=lunala,distance=..2]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHyperA] at @s run kill @e[tag=lunala,distance=..2]

execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHonorA] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaPokeA] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaMasterA] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaSuperA] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHyperA] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]

kill @e[tag=necrozma,tag=!combat,tag=necrozmaHonorA]
kill @e[tag=necrozma,tag=!combat,tag=necrozmaPokeA]
kill @e[tag=necrozma,tag=!combat,tag=necrozmaMasterA]
kill @e[tag=necrozma,tag=!combat,tag=necrozmaSuperA]
kill @e[tag=necrozma,tag=!combat,tag=necrozmaHyperA]




execute as @e[tag=necrozma,tag=!combat,tag=captHonorball] at @s if entity @e[tag=solgaleo,tag=captHonorball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaHonorC
execute as @e[tag=necrozma,tag=!combat,tag=captPokeball] at @s if entity @e[tag=solgaleo,tag=captPokeball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaPokeC
execute as @e[tag=necrozma,tag=!combat,tag=captMasterball] at @s if entity @e[tag=solgaleo,tag=captMasterball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaMasterC
execute as @e[tag=necrozma,tag=!combat,tag=captSuperball] at @s if entity @e[tag=solgaleo,tag=captSuperball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaSuperC
execute as @e[tag=necrozma,tag=!combat,tag=captHyperball] at @s if entity @e[tag=solgaleo,tag=captHyperball,distance=..2] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add necrozmaHyperC


execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHonorC] run function give:spawn/necrozma_criniere/necrozma_criniere_honorball
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaPokeC] run function give:spawn/necrozma_criniere/necrozma_criniere_pokeball
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaMasterC] run function give:spawn/necrozma_criniere/necrozma_criniere_masterball
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaSuperC] run function give:spawn/necrozma_criniere/necrozma_criniere_superball
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHyperC] run function give:spawn/necrozma_criniere/necrozma_criniere_hyperball


execute at @e[tag=necrozma,tag=!combat,tag=necrozmaHonorC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaPokeC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaMasterC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaSuperC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaHyperC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1

execute at @e[tag=necrozma,tag=!combat,tag=necrozmaHonorC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaPokeC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaMasterC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaSuperC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute at @e[tag=necrozma,tag=!combat,tag=necrozmaHyperC] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1

execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHonorC] at @s run kill @e[tag=solgaleo,distance=..2]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaPokeC] at @s run kill @e[tag=solgaleo,distance=..2]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaMasterC] at @s run kill @e[tag=solgaleo,distance=..2]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaSuperC] at @s run kill @e[tag=solgaleo,distance=..2]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHyperC] at @s run kill @e[tag=solgaleo,distance=..2]

execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHonorC] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaPokeC] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaMasterC] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaSuperC] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]
execute as @e[tag=necrozma,tag=!combat,tag=necrozmaHyperC] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]

kill @e[tag=necrozma,tag=!combat,tag=necrozmaHonorC]
kill @e[tag=necrozma,tag=!combat,tag=necrozmaPokeC]
kill @e[tag=necrozma,tag=!combat,tag=necrozmaMasterC]
kill @e[tag=necrozma,tag=!combat,tag=necrozmaSuperC]
kill @e[tag=necrozma,tag=!combat,tag=necrozmaHyperC]




##########################Plusieurs Formes Pokemon###############
execute as @e[tag=genesect,tag=capture,tag=!combat] run function pkm:abillitys/genesect 





