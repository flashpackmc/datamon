

execute as @e[type=#pkm:spawn_pkm,tag=!pkm] run function pkm:fonctions/random/rnd_spawn




execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/swamp if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/boreas
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=1..200},tag=!pkm] at @s if predicate pkm:biome/swamp if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/coatox
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=201..600},tag=!pkm] at @s if predicate pkm:biome/swamp if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/gobou


execute as @e[type=drowned,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/river if predicate pkm:nuit run function pkm:summon/sauvage/crehelf
execute as @e[type=drowned,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/river if predicate pkm:jour run function pkm:summon/sauvage/crefadet
execute as @e[type=drowned,scores={RND_Spawn=1..100},tag=!pkm] at @s if predicate pkm:biome/river run function pkm:summon/sauvage/minidraco
execute as @e[type=drowned,scores={RND_Spawn=101..400},tag=!pkm] at @s if predicate pkm:biome/river run function pkm:summon/sauvage/magicarpe
execute as @e[type=drowned,scores={RND_Spawn=401..600},tag=!pkm] at @s if predicate pkm:biome/river run function pkm:summon/sauvage/aquali


execute as @e[type=drowned,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/deep_ocean run function pkm:summon/sauvage/kyogre


execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/savanna if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/fulguris
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=1..300},tag=!pkm] at @s if predicate pkm:biome/savanna if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/poussifeu
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=301..600},tag=!pkm] at @s if predicate pkm:biome/savanna if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/salameche
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=601..700},tag=!pkm] at @s if predicate pkm:biome/savanna if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/voltali


execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=1..100},tag=!pkm] at @s if predicate pkm:biome/desert if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/nymphali


execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=1..100},tag=!pkm] at @s if predicate pkm:biome/forest if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/phyllali


execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/plains if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/crefollet
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=1..200},tag=!pkm] at @s if predicate pkm:biome/plains if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/pachirisu
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=201..400},tag=!pkm] at @s if predicate pkm:biome/plains if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/evoli


execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/dark_forest if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/marshadow
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=1..100},tag=!pkm] at @s if predicate pkm:biome/dark_forest if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/mimiqui
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=101..400},tag=!pkm] at @s if predicate pkm:biome/dark_forest if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/skelenox
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=401..500},tag=!pkm] at @s if predicate pkm:biome/dark_forest if predicate pkm:hauteur if predicate pkm:nuit unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/noctali
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=401..500},tag=!pkm] at @s if predicate pkm:biome/dark_forest if predicate pkm:hauteur if predicate pkm:jour unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/mentali


execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=1..100},tag=!pkm] at @s if predicate pkm:biome/ice_spikes if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/givrali
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/ice_spikes if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/kyurem


execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/basalt_deltas run function pkm:summon/sauvage/groudon
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=1..100},tag=!pkm] at @s if predicate pkm:biome/basalt_deltas run function pkm:summon/sauvage/pyroli


execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/badlands if predicate pkm:hauteur if predicate pkm:jour unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/magearna

execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=0},tag=!pkm] at @s if predicate pkm:biome/birch_forest if predicate pkm:hauteur unless block ~ ~-1 ~ #pkm:caverne run function pkm:summon/sauvage/genesect


execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=11..100},tag=!pkm] at @s in minecraft:the_end run function pkm:summon/sauvage/porygon
execute as @e[type=#pkm:spawn_pkm,scores={RND_Spawn=1..10},tag=!pkm] at @s in minecraft:the_end run function pkm:summon/sauvage/porygon_2

#############################legendaire#####################
execute at @e[type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Electrique\",\"color\":\"green\"}"}}}}] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:dragon_breath",Count:3b}}] run function pkm:summon/sauvage/zekrom
execute at @e[type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Feu\",\"color\":\"green\"}"}}}}] if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:dragon_breath",Count:3b}}] run function pkm:summon/sauvage/reshiram

execute at @e[tag=zekrom,tag=!trouve,tag=!capture] run kill @e[type=item,distance=..1]
execute at @e[tag=reshiram,tag=!trouve,tag=!capture] run kill @e[type=item,distance=..1]









############################################LVL#################
execute unless entity @e[tag=legendaire,tag=!trouve,tag=!capture] run tag @e[tag=pkm,tag=!capture,tag=!trouve,tag=!legendaire,limit=1] add enTrouve

scoreboard players set max RND_Lvl 16
execute as @e[tag=enTrouve] run function pkm:fonctions/random/rnd_lvl
scoreboard players set max RND_Lvl 100
scoreboard players set @e[tag=pkm,tag=!capture,tag=!trouve,tag=legendaire] RND_Lvl 70 
scoreboard players set @e[tag=enTrouve,tag=categorie1] multiplieur2 0
scoreboard players set @e[tag=enTrouve,tag=categorie2] multiplieur2 15
scoreboard players set @e[tag=enTrouve,tag=categorie3] multiplieur2 30
scoreboard players operation @e[tag=enTrouve] RND_Lvl += @e[tag=enTrouve] multiplieur2




tag @e[tag=pkm,tag=!capture,tag=!trouve,tag=legendaire,limit=1] add svgVie
tag @e[tag=pkm,tag=!capture,tag=enTrouve,limit=1] add svgVie

tag @e[tag=svgVie,limit=1] add svgVie2

#sert a appeler une fois par pkm sauvage, cela permet de ne pas kill quand le sauvage spawn directement en combat
execute if entity @e[tag=svgVie] run schedule function pkm:invoque/lvl_sauvage 2
execute as @e[tag=svgVie,limit=1] run function pkm:invoque/origine_stat
scoreboard players set @e[tag=svgVie,limit=1] boost_atk 6
scoreboard players set @e[tag=svgVie,limit=1] boost_def 6
scoreboard players set @e[tag=svgVie,limit=1] boost_atkSpec 6
scoreboard players set @e[tag=svgVie,limit=1] boost_defSpec 6
scoreboard players set @e[tag=svgVie,limit=1] boost_vitesse 6




tag @e[tag=svgVie,limit=1] remove svgVie
execute as @e[tag=legendaire,tag=!trouve,tag=!capture] run tellraw @a [{"text":" Un ","color":"light_purple","bold":true},{"selector":"@s"},{"text":" est apparu!","color":"light_purple","bold":true}]

tag @e[tag=pkm,tag=!capture,tag=!trouve,tag=legendaire] add trouve
tag @e[tag=pkm,tag=!capture,tag=enTrouve,limit=1] add trouve
tag @e[tag=pkm,tag=!capture,tag=enTrouve,limit=1] remove enTrouve

scoreboard players set max RND_Lvl 100
tp @e[type=#pkm:spawn_pkm,tag=!pkm] ~ ~-600 ~
kill @e[type=#pkm:spawn_pkm,tag=!pkm]