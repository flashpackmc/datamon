tag @e[tag=capture,tag=!combat,tag=minidraco,scores={LVL=15..}] add evolv
tag @e[tag=capture,tag=!combat,tag=draco,scores={LVL=45..}] add evolv
tag @e[tag=capture,tag=!combat,tag=salameche,scores={LVL=16..}] add evolv
tag @e[tag=capture,tag=!combat,tag=reptincel,scores={LVL=36..}] add evolv
tag @e[tag=capture,tag=!combat,tag=skelenox,scores={LVL=16..}] add evolv
tag @e[tag=capture,tag=!combat,tag=gobou,scores={LVL=16..}] add evolv
tag @e[tag=capture,tag=!combat,tag=flobio,scores={LVL=36..}] add evolv
tag @e[tag=capture,tag=!combat,tag=poussifeu,scores={LVL=16..}] add evolv
tag @e[tag=capture,tag=!combat,tag=galifeu,scores={LVL=36..}] add evolv
tag @e[tag=capture,tag=!combat,tag=skelenox,scores={LVL=37..}] add evolv
tag @e[tag=capture,tag=!combat,tag=teraclope,scores={LVL=50..}] add evolv
tag @e[tag=capture,tag=!combat,tag=porygon,scores={LVL=25..}] add evolv
tag @e[tag=capture,tag=!combat,tag=porygon_2,scores={LVL=40..}] add evolv
tag @e[tag=capture,tag=!combat,tag=magicarpe,scores={LVL=20..}] add evolv

execute as @e[tag=minidraco,tag=evolv,tag=captPokeball] run function give:spawn/draco/draco_pokeball
execute as @e[tag=minidraco,tag=evolv,tag=captHonorball] run function give:spawn/draco/draco_honorball
execute as @e[tag=minidraco,tag=evolv,tag=captMasterball] run function give:spawn/draco/draco_masterball
execute as @e[tag=minidraco,tag=evolv,tag=captSuperball] run function give:spawn/draco/draco_superball
execute as @e[tag=minidraco,tag=evolv,tag=captHyperball] run function give:spawn/draco/draco_hyperball

execute as @e[tag=draco,tag=evolv,tag=captPokeball] run function give:spawn/dracolosse/dracolosse_pokeball
execute as @e[tag=draco,tag=evolv,tag=captHonorball] run function give:spawn/dracolosse/dracolosse_honorball
execute as @e[tag=draco,tag=evolv,tag=captMasterball] run function give:spawn/dracolosse/dracolosse_masterball
execute as @e[tag=draco,tag=evolv,tag=captSuperball] run function give:spawn/dracolosse/dracolosse_superball
execute as @e[tag=draco,tag=evolv,tag=captHyperball] run function give:spawn/dracolosse/dracolosse_hyperball


execute as @e[tag=salameche,tag=evolv,tag=captPokeball] run function give:spawn/reptincel/reptincel_pokeball
execute as @e[tag=salameche,tag=evolv,tag=captHonorball] run function give:spawn/reptincel/reptincel_honorball
execute as @e[tag=salameche,tag=evolv,tag=captMasterball] run function give:spawn/reptincel/reptincel_masterball
execute as @e[tag=salameche,tag=evolv,tag=captSuperball] run function give:spawn/reptincel/reptincel_superball
execute as @e[tag=salameche,tag=evolv,tag=captHyperball] run function give:spawn/reptincel/reptincel_hyperball

execute as @e[tag=reptincel,tag=evolv,tag=captPokeball] run function give:spawn/dracaufeu/dracaufeu_pokeball
execute as @e[tag=reptincel,tag=evolv,tag=captHonorball] run function give:spawn/dracaufeu/dracaufeu_honorball
execute as @e[tag=reptincel,tag=evolv,tag=captMasterball] run function give:spawn/dracaufeu/dracaufeu_masterball
execute as @e[tag=reptincel,tag=evolv,tag=captSuperball] run function give:spawn/dracaufeu/dracaufeu_superball
execute as @e[tag=reptincel,tag=evolv,tag=captHyperball] run function give:spawn/dracaufeu/dracaufeu_hyperball

execute as @e[tag=poussifeu,tag=evolv,tag=captPokeball] run function give:spawn/galifeu/galifeu_pokeball
execute as @e[tag=poussifeu,tag=evolv,tag=captHonorball] run function give:spawn/galifeu/galifeu_honorball
execute as @e[tag=poussifeu,tag=evolv,tag=captMasterball] run function give:spawn/galifeu/galifeu_masterball
execute as @e[tag=poussifeu,tag=evolv,tag=captSuperball] run function give:spawn/galifeu/galifeu_superball
execute as @e[tag=poussifeu,tag=evolv,tag=captHyperball] run function give:spawn/galifeu/galifeu_hyperball

execute as @e[tag=galifeu,tag=evolv,tag=captPokeball] run function give:spawn/brasegali/brasegali_pokeball
execute as @e[tag=galifeu,tag=evolv,tag=captHonorball] run function give:spawn/brasegali/brasegali_honorball
execute as @e[tag=galifeu,tag=evolv,tag=captMasterball] run function give:spawn/brasegali/brasegali_masterball
execute as @e[tag=galifeu,tag=evolv,tag=captSuperball] run function give:spawn/brasegali/brasegali_superball
execute as @e[tag=galifeu,tag=evolv,tag=captHyperball] run function give:spawn/brasegali/brasegali_hyperball

execute as @e[tag=flobio,tag=evolv,tag=captPokeball] run function give:spawn/laggron/laggron_pokeball
execute as @e[tag=flobio,tag=evolv,tag=captHonorball] run function give:spawn/laggron/laggron_honorball
execute as @e[tag=flobio,tag=evolv,tag=captMasterball] run function give:spawn/laggron/laggron_masterball
execute as @e[tag=flobio,tag=evolv,tag=captSuperball] run function give:spawn/laggron/laggron_superball
execute as @e[tag=flobio,tag=evolv,tag=captHyperball] run function give:spawn/laggron/laggron_hyperball

execute as @e[tag=gobou,tag=evolv,tag=captPokeball] run function give:spawn/flobio/flobio_pokeball
execute as @e[tag=gobou,tag=evolv,tag=captHonorball] run function give:spawn/flobio/flobio_honorball
execute as @e[tag=gobou,tag=evolv,tag=captMasterball] run function give:spawn/flobio/flobio_masterball
execute as @e[tag=gobou,tag=evolv,tag=captSuperball] run function give:spawn/flobio/flobio_superball
execute as @e[tag=gobou,tag=evolv,tag=captHyperball] run function give:spawn/flobio/flobio_hyperball

execute as @e[tag=magicarpe,tag=evolv,tag=captPokeball] run function give:spawn/leviator/leviator_pokeball
execute as @e[tag=magicarpe,tag=evolv,tag=captHonorball] run function give:spawn/leviator/leviator_honorball
execute as @e[tag=magicarpe,tag=evolv,tag=captMasterball] run function give:spawn/leviator/leviator_masterball
execute as @e[tag=magicarpe,tag=evolv,tag=captSuperball] run function give:spawn/leviator/leviator_superball
execute as @e[tag=magicarpe,tag=evolv,tag=captHyperball] run function give:spawn/leviator/leviator_hyperball

execute as @e[tag=teraclope,tag=evolv,tag=captPokeball] run function give:spawn/noctunoir/noctunoir_pokeball
execute as @e[tag=teraclope,tag=evolv,tag=captHonorball] run function give:spawn/noctunoir/noctunoir_honorball
execute as @e[tag=teraclope,tag=evolv,tag=captMasterball] run function give:spawn/noctunoir/noctunoir_masterball
execute as @e[tag=teraclope,tag=evolv,tag=captSuperball] run function give:spawn/noctunoir/noctunoir_superball
execute as @e[tag=teraclope,tag=evolv,tag=captHyperball] run function give:spawn/noctunoir/noctunoir_hyperball

execute as @e[tag=skelenox,tag=evolv,tag=captPokeball] run function give:spawn/teraclope/teraclope_pokeball
execute as @e[tag=skelenox,tag=evolv,tag=captHonorball] run function give:spawn/teraclope/teraclope_honorball
execute as @e[tag=skelenox,tag=evolv,tag=captMasterball] run function give:spawn/teraclope/teraclope_masterball
execute as @e[tag=skelenox,tag=evolv,tag=captSuperball] run function give:spawn/teraclope/teraclope_superball
execute as @e[tag=skelenox,tag=evolv,tag=captHyperball] run function give:spawn/teraclope/teraclope_hyperball

execute as @e[tag=porygon,tag=evolv,tag=captPokeball] run function give:spawn/porygon_2/porygon_2_pokeball
execute as @e[tag=porygon,tag=evolv,tag=captHonorball] run function give:spawn/porygon_2/porygon_2_honorball
execute as @e[tag=porygon,tag=evolv,tag=captMasterball] run function give:spawn/porygon_2/porygon_2_masterball
execute as @e[tag=porygon,tag=evolv,tag=captSuperball] run function give:spawn/porygon_2/porygon_2_superball
execute as @e[tag=porygon,tag=evolv,tag=captHyperball] run function give:spawn/porygon_2/porygon_2_hyperball

execute as @e[tag=porygon_2,tag=evolv,tag=captPokeball] run function give:spawn/porygon_z/porygon_z_pokeball
execute as @e[tag=porygon_2,tag=evolv,tag=captHonorball] run function give:spawn/porygon_z/porygon_z_honorball
execute as @e[tag=porygon_2,tag=evolv,tag=captMasterball] run function give:spawn/porygon_z/porygon_z_masterball
execute as @e[tag=porygon_2,tag=evolv,tag=captSuperball] run function give:spawn/porygon_z/porygon_z_superball
execute as @e[tag=porygon_2,tag=evolv,tag=captHyperball] run function give:spawn/porygon_z/porygon_z_hyperball



#####################Evoli############
execute as @e[tag=!combat,tag=evoli,tag=!evoliPyro,tag=!evoliAqua,tag=!evoliVolt,tag=!evoliMent,tag=!evoliNoct,tag=!evoliPhyl,tag=!evoliGivr,tag=!evoliNymp] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Feu\",\"color\":\"green\"}"}}}}] run tag @s add evoliPyro
execute as @e[tag=!combat,tag=evoli,tag=!evoliPyro,tag=!evoliAqua,tag=!evoliVolt,tag=!evoliMent,tag=!evoliNoct,tag=!evoliPhyl,tag=!evoliGivr,tag=!evoliNymp] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Eau\",\"color\":\"green\"}"}}}}] run tag @s add evoliAqua
execute as @e[tag=!combat,tag=evoli,tag=!evoliPyro,tag=!evoliAqua,tag=!evoliVolt,tag=!evoliMent,tag=!evoliNoct,tag=!evoliPhyl,tag=!evoliGivr,tag=!evoliNymp] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Electrique\",\"color\":\"green\"}"}}}}] run tag @s add evoliVolt
execute as @e[tag=!combat,tag=evoli,tag=!evoliPyro,tag=!evoliAqua,tag=!evoliVolt,tag=!evoliMent,tag=!evoliNoct,tag=!evoliPhyl,tag=!evoliGivr,tag=!evoliNymp] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}] run tag @s add evoliMent
execute as @e[tag=!combat,tag=evoli,tag=!evoliPyro,tag=!evoliAqua,tag=!evoliVolt,tag=!evoliMent,tag=!evoliNoct,tag=!evoliPhyl,tag=!evoliGivr,tag=!evoliNymp] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Tenebre\",\"color\":\"green\"}"}}}}] run tag @s add evoliNoct
execute as @e[tag=!combat,tag=evoli,tag=!evoliPyro,tag=!evoliAqua,tag=!evoliVolt,tag=!evoliMent,tag=!evoliNoct,tag=!evoliPhyl,tag=!evoliGivr,tag=!evoliNymp] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Plante\",\"color\":\"green\"}"}}}}] run tag @s add evoliPhyl
execute as @e[tag=!combat,tag=evoli,tag=!evoliPyro,tag=!evoliAqua,tag=!evoliVolt,tag=!evoliMent,tag=!evoliNoct,tag=!evoliPhyl,tag=!evoliGivr,tag=!evoliNymp] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}] run tag @s add evoliGivr
execute as @e[tag=!combat,tag=evoli,tag=!evoliPyro,tag=!evoliAqua,tag=!evoliVolt,tag=!evoliMent,tag=!evoliNoct,tag=!evoliPhyl,tag=!evoliGivr,tag=!evoliNymp] at @s if entity @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Fee\",\"color\":\"green\"}"}}}}] run tag @s add evoliNymp

execute as @e[tag=evoliPyro] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Feu\",\"color\":\"green\"}"}}}}]
execute as @e[tag=evoliAqua] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Eau\",\"color\":\"green\"}"}}}}]
execute as @e[tag=evoliVolt] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Electrique\",\"color\":\"green\"}"}}}}]
execute as @e[tag=evoliMent] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Psy\",\"color\":\"green\"}"}}}}]
execute as @e[tag=evoliNoct] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Tenebre\",\"color\":\"green\"}"}}}}]
execute as @e[tag=evoliPhyl] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Plante\",\"color\":\"green\"}"}}}}]
execute as @e[tag=evoliGivr] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Glace\",\"color\":\"green\"}"}}}}]
execute as @e[tag=evoliNymp] at @s run kill @e[type=item,distance=..1,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Orbe Fee\",\"color\":\"green\"}"}}}}]


tag @e[tag=evoliPyro] add evolv
tag @e[tag=evoliAqua] add evolv
tag @e[tag=evoliVolt] add evolv
tag @e[tag=evoliMent] add evolv
tag @e[tag=evoliNoct] add evolv
tag @e[tag=evoliPhyl] add evolv
tag @e[tag=evoliGivr] add evolv
tag @e[tag=evoliNymp] add evolv



execute as @e[tag=evoliPyro,tag=captPokeball] run function give:spawn/pyroli/pyroli_pokeball
execute as @e[tag=evoliPyro,tag=captHonorball] run function give:spawn/pyroli/pyroli_honorball
execute as @e[tag=evoliPyro,tag=captMasterball] run function give:spawn/pyroli/pyroli_masterball
execute as @e[tag=evoliPyro,tag=captSuperball] run function give:spawn/pyroli/pyroli_superball
execute as @e[tag=evoliPyro,tag=captHyperball] run function give:spawn/pyroli/pyroli_hyperball

execute as @e[tag=evoliAqua,tag=captPokeball] run function give:spawn/aquali/aquali_pokeball
execute as @e[tag=evoliAqua,tag=captHonorball] run function give:spawn/aquali/aquali_honorball
execute as @e[tag=evoliAqua,tag=captMasterball] run function give:spawn/aquali/aquali_masterball
execute as @e[tag=evoliAqua,tag=captSuperball] run function give:spawn/aquali/aquali_superball
execute as @e[tag=evoliAqua,tag=captHyperball] run function give:spawn/aquali/aquali_hyperball

execute as @e[tag=evoliVolt,tag=captPokeball] run function give:spawn/voltali/voltali_pokeball
execute as @e[tag=evoliVolt,tag=captHonorball] run function give:spawn/voltali/voltali_honorball
execute as @e[tag=evoliVolt,tag=captMasterball] run function give:spawn/voltali/voltali_masterball
execute as @e[tag=evoliVolt,tag=captSuperball] run function give:spawn/voltali/voltali_superball
execute as @e[tag=evoliVolt,tag=captHyperball] run function give:spawn/voltali/voltali_hyperball

execute as @e[tag=evoliMent,tag=captPokeball] run function give:spawn/mentali/mentali_pokeball
execute as @e[tag=evoliMent,tag=captHonorball] run function give:spawn/mentali/mentali_honorball
execute as @e[tag=evoliMent,tag=captMasterball] run function give:spawn/mentali/mentali_masterball
execute as @e[tag=evoliMent,tag=captSuperball] run function give:spawn/mentali/mentali_superball
execute as @e[tag=evoliMent,tag=captHyperball] run function give:spawn/mentali/mentali_hyperball

execute as @e[tag=evoliNoct,tag=captPokeball] run function give:spawn/noctali/noctali_pokeball
execute as @e[tag=evoliNoct,tag=captHonorball] run function give:spawn/noctali/noctali_honorball
execute as @e[tag=evoliNoct,tag=captMasterball] run function give:spawn/noctali/noctali_masterball
execute as @e[tag=evoliNoct,tag=captSuperball] run function give:spawn/noctali/noctali_superball
execute as @e[tag=evoliNoct,tag=captHyperball] run function give:spawn/noctali/noctali_hyperball

execute as @e[tag=evoliPhyl,tag=captPokeball] run function give:spawn/phyllali/phyllali_pokeball
execute as @e[tag=evoliPhyl,tag=captHonorball] run function give:spawn/phyllali/phyllali_honorball
execute as @e[tag=evoliPhyl,tag=captMasterball] run function give:spawn/phyllali/phyllali_masterball
execute as @e[tag=evoliPhyl,tag=captSuperball] run function give:spawn/phyllali/phyllali_superball
execute as @e[tag=evoliPhyl,tag=captHyperball] run function give:spawn/phyllali/phyllali_hyperball

execute as @e[tag=evoliGivr,tag=captPokeball] run function give:spawn/givrali/givrali_pokeball
execute as @e[tag=evoliGivr,tag=captHonorball] run function give:spawn/givrali/givrali_honorball
execute as @e[tag=evoliGivr,tag=captMasterball] run function give:spawn/givrali/givrali_masterball
execute as @e[tag=evoliGivr,tag=captSuperball] run function give:spawn/givrali/givrali_superball
execute as @e[tag=evoliGivr,tag=captHyperball] run function give:spawn/givrali/givrali_hyperball

execute as @e[tag=evoliNymp,tag=captPokeball] run function give:spawn/nymphali/nymphali_pokeball
execute as @e[tag=evoliNymp,tag=captHonorball] run function give:spawn/nymphali/nymphali_honorball
execute as @e[tag=evoliNymp,tag=captMasterball] run function give:spawn/nymphali/nymphali_masterball
execute as @e[tag=evoliNymp,tag=captSuperball] run function give:spawn/nymphali/nymphali_superball
execute as @e[tag=evoliNymp,tag=captHyperball] run function give:spawn/nymphali/nymphali_hyperball



###############################################################################################
execute if entity @e[tag=evolv] run tellraw @p[tag=!combat] {"text":" Quoi?","color":"none"}
execute if entity @e[tag=evolv] run tellraw @p[tag=!combat] [{"text":" ","color":"none"},{"selector":"@e[tag=evolv]"},{"text":" évolue!","color":"none"}]
execute at @e[tag=evolv] run playsound minecraft:ui.toast.challenge_complete record @p[distance=..5] 

execute if entity @e[tag=evolv,tag=captPokeball] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute if entity @e[tag=evolv,tag=captMasterball] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute if entity @e[tag=evolv,tag=captHonorball] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute if entity @e[tag=evolv,tag=captSuperball] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute if entity @e[tag=evolv,tag=captHyperball] run clear @p minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1

kill @e[tag=evolv]