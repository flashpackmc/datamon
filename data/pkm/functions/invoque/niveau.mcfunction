tag @e[tag=capture,tag=!combat,scores={XP=15..}] add monteLVL
scoreboard players add @e[tag=monteLVL,scores={LVL=..99}] LVL 1
scoreboard players set @e[tag=monteLVL] XP 0

execute if entity @e[tag=monteLVL] run tellraw @p[tag=!combat] [{"text":" ","color":"none"},{"selector":"@e[tag=monteLVL]"},{"text":" monte au N. ","color":"none"},{"score":{"name":"@e[tag=monteLVL,limit=1]","objective":"LVL"}},{"text":"!","color":"none"}]
execute at @e[tag=monteLVL] run playsound minecraft:ui.toast.challenge_complete record @p[distance=..5] 

tag @e[tag=monteLVL] remove monteLVL