execute if entity @e[type=item,nbt={Item:{id:"minecraft:red_dye",Count:1b}},distance=..1] if entity @e[type=item,nbt={Item:{id:"minecraft:stone_button",Count:1b}},distance=..1] run tag @s add pokeCraft
execute if entity @e[type=item,nbt={Item:{id:"minecraft:blue_dye",Count:1b}},distance=..1] if entity @e[type=item,nbt={Item:{id:"minecraft:stone_button",Count:1b}},distance=..1] run tag @s add superCraft
execute if entity @e[type=item,nbt={Item:{id:"minecraft:black_dye",Count:1b}},distance=..1] if entity @e[type=item,nbt={Item:{id:"minecraft:stone_button",Count:1b}},distance=..1] run tag @s add hyperCraft
execute if entity @e[type=item,nbt={Item:{id:"minecraft:white_dye",Count:1b}},distance=..1] if entity @e[type=item,nbt={Item:{id:"minecraft:stone_button",Count:1b}},distance=..1] run tag @s add honorCraft



##############give
execute if entity @s[tag=pokeCraft] as @p run function give:item/pokeball
execute if entity @s[tag=pokeCraft] as @p run function give:item/pokeball
execute if entity @s[tag=pokeCraft] as @p run function give:item/pokeball

execute if entity @s[tag=superCraft] as @p run function give:item/superball
execute if entity @s[tag=superCraft] as @p run function give:item/superball

execute if entity @s[tag=hyperCraft] as @p run function give:item/hyperball

execute if entity @s[tag=honorCraft] as @p run function give:item/honorball



execute if entity @s[tag=pokeCraft] run kill @e[type=item,distance=..1]
execute if entity @s[tag=superCraft] run kill @e[type=item,distance=..1]
execute if entity @s[tag=hyperCraft] run kill @e[type=item,distance=..1]
execute if entity @s[tag=honorCraft] run kill @e[type=item,distance=..1]

kill @s[tag=pokeCraft]
kill @s[tag=superCraft]
kill @s[tag=hyperCraft]
kill @s[tag=honorCraft]