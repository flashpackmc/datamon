execute at @s run summon area_effect_cloud ~ ~-1 ~ {Tags:["random"],Age:1}
execute store result score @s RND_Lvl run data get entity @e[type=area_effect_cloud,tag=random,limit=1] UUID[0] 
scoreboard players operation @s RND_Lvl %= max RND_Lvl
kill @e[type=area_effect_cloud,tag=random]

scoreboard players set @s[scores={RND_Lvl=0}] RND_Lvl 1