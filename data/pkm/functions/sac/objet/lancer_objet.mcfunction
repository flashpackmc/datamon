tellraw @a[tag=combat,tag=en_objet,tag=!messObjet] {"text":" Droppez votre objet"}
tellraw @a[tag=combat,tag=en_objet,tag=!messObjet] {"text":" Annule","color":"yellow","clickEvent":{"action":"run_command","value":"/tag @a[tag=combat,tag=en_objet] add en_objet1"}}
tellraw @a[tag=combat,tag=en_objet,tag=!messObjet] {"text":" "}

execute as @a[tag=combat,tag=en_objet] run tag @s add messObjet



execute at @a[tag=combat,tag=en_objet] as @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Potion\",\"color\":\"light_purple\"}"}}}}] run tag @s add drop_objet
execute at @a[tag=combat,tag=en_objet] as @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Super Potion\",\"color\":\"gold\"}"}}}}] run tag @s add drop_objet
execute at @a[tag=combat,tag=en_objet] as @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Hyper Potion\",\"color\":\"dark_purple\"}"}}}}] run tag @s add drop_objet




execute as @e[tag=capture,tag=pkm,tag=!en_objet] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Potion\",\"color\":\"light_purple\"}"}}}}] run tag @s add potion
execute as @e[tag=capture,tag=pkm,tag=!en_objet] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Super Potion\",\"color\":\"gold\"}"}}}}] run tag @s add superpotion
execute as @e[tag=capture,tag=pkm,tag=!en_objet] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Hyper Potion\",\"color\":\"dark_purple\"}"}}}}] run tag @s add hyperpotion





execute as @e[tag=pkm,tag=!en_objet,tag=capture] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Potion\",\"color\":\"light_purple\"}"}}}}] run tag @s add en_objet
execute as @e[tag=pkm,tag=!en_objet,tag=capture] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Super Potion\",\"color\":\"gold\"}"}}}}] run tag @s add en_objet
execute as @e[tag=pkm,tag=!en_objet,tag=capture] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Hyper Potion\",\"color\":\"dark_purple\"}"}}}}] run tag @s add en_objet



effect give @e[tag=potion] instant_damage 1 2 true 
effect give @e[tag=superpotion] instant_damage 1 3 true 
effect give @e[tag=hyperpotion] instant_damage 1 4 true 







execute as @e[tag=en_objet,tag=pkm] at @s run kill @e[distance=..3,type=item,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Potion\",\"color\":\"light_purple\"}"}}}}]
execute as @e[tag=en_objet,tag=pkm] at @s run kill @e[distance=..3,type=item,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Super Potion\",\"color\":\"gold\"}"}}}}]
execute as @e[tag=en_objet,tag=pkm] at @s run kill @e[distance=..3,type=item,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Hyper Potion\",\"color\":\"dark_purple\"}"}}}}]




execute as @e[tag=en_objet,tag=pkm] at @s run playsound minecraft:item.trident.hit_ground record @p[distance=..5] 
execute if entity @e[tag=potion] run tellraw @a[tag=combat] ["",{"text":" "},{"selector":"@e[tag=potion]","color":"green"},{"text":" a été soigné de 16 PV !","bold":false}]
execute if entity @e[tag=superpotion] run tellraw @a[tag=combat] ["",{"text":" "},{"selector":"@e[tag=superpotion]","color":"green"},{"text":" a été soigné de 32 PV !","bold":false}]
execute if entity @e[tag=hyperpotion] run tellraw @a[tag=combat] ["",{"text":" "},{"selector":"@e[tag=hyperpotion]","color":"green"},{"text":" a été soigné de 64 PV !","bold":false}]

execute if entity @e[tag=en_objet,tag=pkm] run schedule function pkm:combat/attaque_pkm/sauvage/liste_pkm_attaque 2
#le schedule permet de se soigne avant d'infliger les degats

#le set tour 1 est dans pkm:combat/attaque_pkm/sauvage/liste_pkm_attaque

execute if entity @e[tag=en_objet,tag=pkm] run tag @p remove messSac
execute if entity @e[tag=en_objet,tag=pkm] run tag @p remove en_sac
execute if entity @e[tag=en_objet,tag=pkm] run tag @p remove messObjet
execute if entity @e[tag=en_objet,tag=pkm] run tag @e remove en_objet

tag @e[tag=potion] remove potion
tag @e[tag=superpotion] remove superpotion
tag @e[tag=hyperpotion] remove hyperpotion


tag @a[tag=en_objet1] remove messObjet
tag @a[tag=en_objet1] remove messSac
tag @a[tag=en_objet1] remove en_objet
tag @a[tag=en_objet1] remove en_objet1

