execute as @e[tag=leviator,tag=en_capture,tag=pokeball] run function give:spawn/leviator/leviator_pokeball
execute as @e[tag=leviator,tag=en_capture,tag=masterball] run function give:spawn/leviator/leviator_masterball
execute as @e[tag=leviator,tag=en_capture,tag=honorball] run function give:spawn/leviator/leviator_honorball
execute as @e[tag=leviator,tag=en_capture,tag=superball] run function give:spawn/leviator/leviator_superball
execute as @e[tag=leviator,tag=en_capture,tag=hyperball] run function give:spawn/leviator/leviator_hyperball


execute as @e[tag=noctali,tag=en_capture,tag=pokeball] run function give:spawn/noctali/noctali_pokeball
execute as @e[tag=noctali,tag=en_capture,tag=masterball] run function give:spawn/noctali/noctali_masterball
execute as @e[tag=noctali,tag=en_capture,tag=honorball] run function give:spawn/noctali/noctali_honorball
execute as @e[tag=noctali,tag=en_capture,tag=superball] run function give:spawn/noctali/noctali_superball
execute as @e[tag=noctali,tag=en_capture,tag=hyperball] run function give:spawn/noctali/noctali_hyperball

execute as @e[tag=dracaufeu,tag=en_capture,tag=pokeball] run function give:spawn/dracaufeu/dracaufeu_pokeball
execute as @e[tag=dracaufeu,tag=en_capture,tag=masterball] run function give:spawn/dracaufeu/dracaufeu_masterball
execute as @e[tag=dracaufeu,tag=en_capture,tag=honorball] run function give:spawn/dracaufeu/dracaufeu_honorball
execute as @e[tag=dracaufeu,tag=en_capture,tag=superball] run function give:spawn/dracaufeu/dracaufeu_superball
execute as @e[tag=dracaufeu,tag=en_capture,tag=hyperball] run function give:spawn/dracaufeu/dracaufeu_hyperball

execute as @e[tag=pierroteknik,tag=en_capture,tag=pokeball] run function give:spawn/pierroteknik/pierroteknik_pokeball
execute as @e[tag=pierroteknik,tag=en_capture,tag=masterball] run function give:spawn/pierroteknik/pierroteknik_masterball
execute as @e[tag=pierroteknik,tag=en_capture,tag=honorball] run function give:spawn/pierroteknik/pierroteknik_honorball
execute as @e[tag=pierroteknik,tag=en_capture,tag=superball] run function give:spawn/pierroteknik/pierroteknik_superball
execute as @e[tag=pierroteknik,tag=en_capture,tag=hyperball] run function give:spawn/pierroteknik/pierroteknik_hyperball

execute as @e[tag=bamboiselle,tag=en_capture,tag=pokeball] run function give:spawn/bamboiselle/bamboiselle_pokeball
execute as @e[tag=bamboiselle,tag=en_capture,tag=masterball] run function give:spawn/bamboiselle/bamboiselle_masterball
execute as @e[tag=bamboiselle,tag=en_capture,tag=honorball] run function give:spawn/bamboiselle/bamboiselle_honorball
execute as @e[tag=bamboiselle,tag=en_capture,tag=superball] run function give:spawn/bamboiselle/bamboiselle_superball
execute as @e[tag=bamboiselle,tag=en_capture,tag=hyperball] run function give:spawn/bamboiselle/bamboiselle_hyperball

execute as @e[tag=mouscoto,tag=en_capture,tag=pokeball] run function give:spawn/mouscoto/mouscoto_pokeball
execute as @e[tag=mouscoto,tag=en_capture,tag=masterball] run function give:spawn/mouscoto/mouscoto_masterball
execute as @e[tag=mouscoto,tag=en_capture,tag=honorball] run function give:spawn/mouscoto/mouscoto_honorball
execute as @e[tag=mouscoto,tag=en_capture,tag=superball] run function give:spawn/mouscoto/mouscoto_superball
execute as @e[tag=mouscoto,tag=en_capture,tag=hyperball] run function give:spawn/mouscoto/mouscoto_hyperball

execute as @e[tag=cancrelove,tag=en_capture,tag=pokeball] run function give:spawn/cancrelove/cancrelove_pokeball
execute as @e[tag=cancrelove,tag=en_capture,tag=masterball] run function give:spawn/cancrelove/cancrelove_masterball
execute as @e[tag=cancrelove,tag=en_capture,tag=honorball] run function give:spawn/cancrelove/cancrelove_honorball
execute as @e[tag=cancrelove,tag=en_capture,tag=superball] run function give:spawn/cancrelove/cancrelove_superball
execute as @e[tag=cancrelove,tag=en_capture,tag=hyperball] run function give:spawn/cancrelove/cancrelove_hyperball

execute as @e[tag=katagami,tag=en_capture,tag=pokeball] run function give:spawn/katagami/katagami_pokeball
execute as @e[tag=katagami,tag=en_capture,tag=masterball] run function give:spawn/katagami/katagami_masterball
execute as @e[tag=katagami,tag=en_capture,tag=honorball] run function give:spawn/katagami/katagami_honorball
execute as @e[tag=katagami,tag=en_capture,tag=superball] run function give:spawn/katagami/katagami_superball
execute as @e[tag=katagami,tag=en_capture,tag=hyperball] run function give:spawn/katagami/katagami_hyperball

execute as @e[tag=demeteros,tag=en_capture,tag=pokeball] run function give:spawn/demeteros/demeteros_pokeball
execute as @e[tag=demeteros,tag=en_capture,tag=masterball] run function give:spawn/demeteros/demeteros_masterball
execute as @e[tag=demeteros,tag=en_capture,tag=honorball] run function give:spawn/demeteros/demeteros_honorball
execute as @e[tag=demeteros,tag=en_capture,tag=superball] run function give:spawn/demeteros/demeteros_superball
execute as @e[tag=demeteros,tag=en_capture,tag=hyperball] run function give:spawn/demeteros/demeteros_hyperball

execute as @e[tag=boreas,tag=en_capture,tag=pokeball] run function give:spawn/boreas/boreas_pokeball
execute as @e[tag=boreas,tag=en_capture,tag=masterball] run function give:spawn/boreas/boreas_masterball
execute as @e[tag=boreas,tag=en_capture,tag=honorball] run function give:spawn/boreas/boreas_honorball
execute as @e[tag=boreas,tag=en_capture,tag=superball] run function give:spawn/boreas/boreas_superball
execute as @e[tag=boreas,tag=en_capture,tag=hyperball] run function give:spawn/boreas/boreas_hyperball

execute as @e[tag=fulguris,tag=en_capture,tag=pokeball] run function give:spawn/fulguris/fulguris_pokeball
execute as @e[tag=fulguris,tag=en_capture,tag=masterball] run function give:spawn/fulguris/fulguris_masterball
execute as @e[tag=fulguris,tag=en_capture,tag=honorball] run function give:spawn/fulguris/fulguris_honorball
execute as @e[tag=fulguris,tag=en_capture,tag=superball] run function give:spawn/fulguris/fulguris_superball
execute as @e[tag=fulguris,tag=en_capture,tag=hyperball] run function give:spawn/fulguris/fulguris_hyperball

execute as @e[tag=magearna,tag=en_capture,tag=pokeball] run function give:spawn/magearna/magearna_pokeball
execute as @e[tag=magearna,tag=en_capture,tag=masterball] run function give:spawn/magearna/magearna_masterball
execute as @e[tag=magearna,tag=en_capture,tag=honorball] run function give:spawn/magearna/magearna_honorball
execute as @e[tag=magearna,tag=en_capture,tag=superball] run function give:spawn/magearna/magearna_superball
execute as @e[tag=magearna,tag=en_capture,tag=hyperball] run function give:spawn/magearna/magearna_hyperball

execute as @e[tag=regigigas,tag=en_capture,tag=pokeball] run function give:spawn/regigigas/regigigas_pokeball
execute as @e[tag=regigigas,tag=en_capture,tag=masterball] run function give:spawn/regigigas/regigigas_masterball
execute as @e[tag=regigigas,tag=en_capture,tag=honorball] run function give:spawn/regigigas/regigigas_honorball
execute as @e[tag=regigigas,tag=en_capture,tag=superball] run function give:spawn/regigigas/regigigas_superball
execute as @e[tag=regigigas,tag=en_capture,tag=hyperball] run function give:spawn/regigigas/regigigas_hyperball

execute as @e[tag=marshadow,tag=en_capture,tag=pokeball] run function give:spawn/marshadow/marshadow_pokeball
execute as @e[tag=marshadow,tag=en_capture,tag=masterball] run function give:spawn/marshadow/marshadow_masterball
execute as @e[tag=marshadow,tag=en_capture,tag=honorball] run function give:spawn/marshadow/marshadow_honorball
execute as @e[tag=marshadow,tag=en_capture,tag=superball] run function give:spawn/marshadow/marshadow_superball
execute as @e[tag=marshadow,tag=en_capture,tag=hyperball] run function give:spawn/marshadow/marshadow_hyperball

execute as @e[tag=minidraco,tag=en_capture,tag=pokeball] run function give:spawn/minidraco/minidraco_pokeball
execute as @e[tag=minidraco,tag=en_capture,tag=masterball] run function give:spawn/minidraco/minidraco_masterball
execute as @e[tag=minidraco,tag=en_capture,tag=honorball] run function give:spawn/minidraco/minidraco_honorball
execute as @e[tag=minidraco,tag=en_capture,tag=superball] run function give:spawn/minidraco/minidraco_superball
execute as @e[tag=minidraco,tag=en_capture,tag=hyperball] run function give:spawn/minidraco/minidraco_hyperball

execute as @e[tag=draco,tag=en_capture,tag=pokeball] run function give:spawn/draco/draco_pokeball
execute as @e[tag=draco,tag=en_capture,tag=masterball] run function give:spawn/draco/draco_masterball
execute as @e[tag=draco,tag=en_capture,tag=honorball] run function give:spawn/draco/draco_honorball
execute as @e[tag=draco,tag=en_capture,tag=superball] run function give:spawn/draco/draco_superball
execute as @e[tag=draco,tag=en_capture,tag=hyperball] run function give:spawn/draco/draco_hyperball

execute as @e[tag=dracolosse,tag=en_capture,tag=pokeball] run function give:spawn/dracolosse/dracolosse_pokeball
execute as @e[tag=dracolosse,tag=en_capture,tag=masterball] run function give:spawn/dracolosse/dracolosse_masterball
execute as @e[tag=dracolosse,tag=en_capture,tag=honorball] run function give:spawn/dracolosse/dracolosse_honorball
execute as @e[tag=dracolosse,tag=en_capture,tag=superball] run function give:spawn/dracolosse/dracolosse_superball
execute as @e[tag=dracolosse,tag=en_capture,tag=hyperball] run function give:spawn/dracolosse/dracolosse_hyperball

execute as @e[tag=genesect,tag=en_capture,tag=pokeball] run function give:spawn/genesect/genesect_pokeball
execute as @e[tag=genesect,tag=en_capture,tag=masterball] run function give:spawn/genesect/genesect_masterball
execute as @e[tag=genesect,tag=en_capture,tag=honorball] run function give:spawn/genesect/genesect_honorball
execute as @e[tag=genesect,tag=en_capture,tag=superball] run function give:spawn/genesect/genesect_superball
execute as @e[tag=genesect,tag=en_capture,tag=hyperball] run function give:spawn/genesect/genesect_hyperball

execute as @e[tag=crehelf,tag=en_capture,tag=pokeball] run function give:spawn/crehelf/crehelf_pokeball
execute as @e[tag=crehelf,tag=en_capture,tag=masterball] run function give:spawn/crehelf/crehelf_masterball
execute as @e[tag=crehelf,tag=en_capture,tag=honorball] run function give:spawn/crehelf/crehelf_honorball
execute as @e[tag=crehelf,tag=en_capture,tag=superball] run function give:spawn/crehelf/crehelf_superball
execute as @e[tag=crehelf,tag=en_capture,tag=hyperball] run function give:spawn/crehelf/crehelf_hyperball

execute as @e[tag=crefollet,tag=en_capture,tag=pokeball] run function give:spawn/crefollet/crefollet_pokeball
execute as @e[tag=crefollet,tag=en_capture,tag=masterball] run function give:spawn/crefollet/crefollet_masterball
execute as @e[tag=crefollet,tag=en_capture,tag=honorball] run function give:spawn/crefollet/crefollet_honorball
execute as @e[tag=crefollet,tag=en_capture,tag=superball] run function give:spawn/crefollet/crefollet_superball
execute as @e[tag=crefollet,tag=en_capture,tag=hyperball] run function give:spawn/crefollet/crefollet_hyperball

execute as @e[tag=crefadet,tag=en_capture,tag=pokeball] run function give:spawn/crefadet/crefadet_pokeball
execute as @e[tag=crefadet,tag=en_capture,tag=masterball] run function give:spawn/crefadet/crefadet_masterball
execute as @e[tag=crefadet,tag=en_capture,tag=honorball] run function give:spawn/crefadet/crefadet_honorball
execute as @e[tag=crefadet,tag=en_capture,tag=superball] run function give:spawn/crefadet/crefadet_superball
execute as @e[tag=crefadet,tag=en_capture,tag=hyperball] run function give:spawn/crefadet/crefadet_hyperball

execute as @e[tag=zekrom,tag=en_capture,tag=pokeball] run function give:spawn/zekrom/zekrom_pokeball
execute as @e[tag=zekrom,tag=en_capture,tag=masterball] run function give:spawn/zekrom/zekrom_masterball
execute as @e[tag=zekrom,tag=en_capture,tag=honorball] run function give:spawn/zekrom/zekrom_honorball
execute as @e[tag=zekrom,tag=en_capture,tag=superball] run function give:spawn/zekrom/zekrom_superball
execute as @e[tag=zekrom,tag=en_capture,tag=hyperball] run function give:spawn/zekrom/zekrom_hyperball

execute as @e[tag=noctunoir,tag=en_capture,tag=pokeball] run function give:spawn/noctunoir/noctunoir_pokeball
execute as @e[tag=noctunoir,tag=en_capture,tag=masterball] run function give:spawn/noctunoir/noctunoir_masterball
execute as @e[tag=noctunoir,tag=en_capture,tag=honorball] run function give:spawn/noctunoir/noctunoir_honorball
execute as @e[tag=noctunoir,tag=en_capture,tag=superball] run function give:spawn/noctunoir/noctunoir_superball
execute as @e[tag=noctunoir,tag=en_capture,tag=hyperball] run function give:spawn/noctunoir/noctunoir_hyperball

execute as @e[tag=magicarpe,tag=en_capture,tag=pokeball] run function give:spawn/magicarpe/magicarpe_pokeball
execute as @e[tag=magicarpe,tag=en_capture,tag=masterball] run function give:spawn/magicarpe/magicarpe_masterball
execute as @e[tag=magicarpe,tag=en_capture,tag=honorball] run function give:spawn/magicarpe/magicarpe_honorball
execute as @e[tag=magicarpe,tag=en_capture,tag=superball] run function give:spawn/magicarpe/magicarpe_superball
execute as @e[tag=magicarpe,tag=en_capture,tag=hyperball] run function give:spawn/magicarpe/magicarpe_hyperball

execute as @e[tag=kyurem,tag=en_capture,tag=pokeball] run function give:spawn/kyurem/kyurem_pokeball
execute as @e[tag=kyurem,tag=en_capture,tag=masterball] run function give:spawn/kyurem/kyurem_masterball
execute as @e[tag=kyurem,tag=en_capture,tag=honorball] run function give:spawn/kyurem/kyurem_honorball
execute as @e[tag=kyurem,tag=en_capture,tag=superball] run function give:spawn/kyurem/kyurem_superball
execute as @e[tag=kyurem,tag=en_capture,tag=hyperball] run function give:spawn/kyurem/kyurem_hyperball

execute as @e[tag=coatox,tag=en_capture,tag=pokeball] run function give:spawn/coatox/coatox_pokeball
execute as @e[tag=coatox,tag=en_capture,tag=masterball] run function give:spawn/coatox/coatox_masterball
execute as @e[tag=coatox,tag=en_capture,tag=honorball] run function give:spawn/coatox/coatox_honorball
execute as @e[tag=coatox,tag=en_capture,tag=superball] run function give:spawn/coatox/coatox_superball
execute as @e[tag=coatox,tag=en_capture,tag=hyperball] run function give:spawn/coatox/coatox_hyperball

execute as @e[tag=pachirisu,tag=en_capture,tag=pokeball] run function give:spawn/pachirisu/pachirisu_pokeball
execute as @e[tag=pachirisu,tag=en_capture,tag=masterball] run function give:spawn/pachirisu/pachirisu_masterball
execute as @e[tag=pachirisu,tag=en_capture,tag=honorball] run function give:spawn/pachirisu/pachirisu_honorball
execute as @e[tag=pachirisu,tag=en_capture,tag=superball] run function give:spawn/pachirisu/pachirisu_superball
execute as @e[tag=pachirisu,tag=en_capture,tag=hyperball] run function give:spawn/pachirisu/pachirisu_hyperball

execute as @e[tag=mimiqui,tag=en_capture,tag=pokeball] run function give:spawn/mimiqui/mimiqui_pokeball
execute as @e[tag=mimiqui,tag=en_capture,tag=masterball] run function give:spawn/mimiqui/mimiqui_masterball
execute as @e[tag=mimiqui,tag=en_capture,tag=honorball] run function give:spawn/mimiqui/mimiqui_honorball
execute as @e[tag=mimiqui,tag=en_capture,tag=superball] run function give:spawn/mimiqui/mimiqui_superball
execute as @e[tag=mimiqui,tag=en_capture,tag=hyperball] run function give:spawn/mimiqui/mimiqui_hyperball

execute as @e[tag=laggron,tag=en_capture,tag=pokeball] run function give:spawn/laggron/laggron_pokeball
execute as @e[tag=laggron,tag=en_capture,tag=masterball] run function give:spawn/laggron/laggron_masterball
execute as @e[tag=laggron,tag=en_capture,tag=honorball] run function give:spawn/laggron/laggron_honorball
execute as @e[tag=laggron,tag=en_capture,tag=superball] run function give:spawn/laggron/laggron_superball
execute as @e[tag=laggron,tag=en_capture,tag=hyperball] run function give:spawn/laggron/laggron_hyperball

execute as @e[tag=flobio,tag=en_capture,tag=pokeball] run function give:spawn/flobio/flobio_pokeball
execute as @e[tag=flobio,tag=en_capture,tag=masterball] run function give:spawn/flobio/flobio_masterball
execute as @e[tag=flobio,tag=en_capture,tag=honorball] run function give:spawn/flobio/flobio_honorball
execute as @e[tag=flobio,tag=en_capture,tag=superball] run function give:spawn/flobio/flobio_superball
execute as @e[tag=flobio,tag=en_capture,tag=hyperball] run function give:spawn/flobio/flobio_hyperball

execute as @e[tag=gobou,tag=en_capture,tag=pokeball] run function give:spawn/gobou/gobou_pokeball
execute as @e[tag=gobou,tag=en_capture,tag=masterball] run function give:spawn/gobou/gobou_masterball
execute as @e[tag=gobou,tag=en_capture,tag=honorball] run function give:spawn/gobou/gobou_honorball
execute as @e[tag=gobou,tag=en_capture,tag=superball] run function give:spawn/gobou/gobou_superball
execute as @e[tag=gobou,tag=en_capture,tag=hyperball] run function give:spawn/gobou/gobou_hyperball

execute as @e[tag=brasegali,tag=en_capture,tag=pokeball] run function give:spawn/brasegali/brasegali_pokeball
execute as @e[tag=brasegali,tag=en_capture,tag=masterball] run function give:spawn/brasegali/brasegali_masterball
execute as @e[tag=brasegali,tag=en_capture,tag=honorball] run function give:spawn/brasegali/brasegali_honorball
execute as @e[tag=brasegali,tag=en_capture,tag=superball] run function give:spawn/brasegali/brasegali_superball
execute as @e[tag=brasegali,tag=en_capture,tag=hyperball] run function give:spawn/brasegali/brasegali_hyperball

execute as @e[tag=necrozma,tag=en_capture,tag=pokeball] run function give:spawn/necrozma/necrozma_pokeball
execute as @e[tag=necrozma,tag=en_capture,tag=masterball] run function give:spawn/necrozma/necrozma_masterball
execute as @e[tag=necrozma,tag=en_capture,tag=honorball] run function give:spawn/necrozma/necrozma_honorball
execute as @e[tag=necrozma,tag=en_capture,tag=superball] run function give:spawn/necrozma/necrozma_superball
execute as @e[tag=necrozma,tag=en_capture,tag=hyperball] run function give:spawn/necrozma/necrozma_hyperball

execute as @e[tag=galifeu,tag=en_capture,tag=pokeball] run function give:spawn/galifeu/galifeu_pokeball
execute as @e[tag=galifeu,tag=en_capture,tag=masterball] run function give:spawn/galifeu/galifeu_masterball
execute as @e[tag=galifeu,tag=en_capture,tag=honorball] run function give:spawn/galifeu/galifeu_honorball
execute as @e[tag=galifeu,tag=en_capture,tag=superball] run function give:spawn/galifeu/galifeu_superball
execute as @e[tag=galifeu,tag=en_capture,tag=hyperball] run function give:spawn/galifeu/galifeu_hyperball

execute as @e[tag=poussifeu,tag=en_capture,tag=pokeball] run function give:spawn/poussifeu/poussifeu_pokeball
execute as @e[tag=poussifeu,tag=en_capture,tag=masterball] run function give:spawn/poussifeu/poussifeu_masterball
execute as @e[tag=poussifeu,tag=en_capture,tag=honorball] run function give:spawn/poussifeu/poussifeu_honorball
execute as @e[tag=poussifeu,tag=en_capture,tag=superball] run function give:spawn/poussifeu/poussifeu_superball
execute as @e[tag=poussifeu,tag=en_capture,tag=hyperball] run function give:spawn/poussifeu/poussifeu_hyperball

execute as @e[tag=reptincel,tag=en_capture,tag=pokeball] run function give:spawn/reptincel/reptincel_pokeball
execute as @e[tag=reptincel,tag=en_capture,tag=masterball] run function give:spawn/reptincel/reptincel_masterball
execute as @e[tag=reptincel,tag=en_capture,tag=honorball] run function give:spawn/reptincel/reptincel_honorball
execute as @e[tag=reptincel,tag=en_capture,tag=superball] run function give:spawn/reptincel/reptincel_superball
execute as @e[tag=reptincel,tag=en_capture,tag=hyperball] run function give:spawn/reptincel/reptincel_hyperball

execute as @e[tag=salameche,tag=en_capture,tag=pokeball] run function give:spawn/salameche/salameche_pokeball
execute as @e[tag=salameche,tag=en_capture,tag=masterball] run function give:spawn/salameche/salameche_masterball
execute as @e[tag=salameche,tag=en_capture,tag=honorball] run function give:spawn/salameche/salameche_honorball
execute as @e[tag=salameche,tag=en_capture,tag=superball] run function give:spawn/salameche/salameche_superball
execute as @e[tag=salameche,tag=en_capture,tag=hyperball] run function give:spawn/salameche/salameche_hyperball

execute as @e[tag=teraclope,tag=en_capture,tag=pokeball] run function give:spawn/teraclope/teraclope_pokeball
execute as @e[tag=teraclope,tag=en_capture,tag=masterball] run function give:spawn/teraclope/teraclope_masterball
execute as @e[tag=teraclope,tag=en_capture,tag=honorball] run function give:spawn/teraclope/teraclope_honorball
execute as @e[tag=teraclope,tag=en_capture,tag=superball] run function give:spawn/teraclope/teraclope_superball
execute as @e[tag=teraclope,tag=en_capture,tag=hyperball] run function give:spawn/teraclope/teraclope_hyperball

execute as @e[tag=skelenox,tag=en_capture,tag=pokeball] run function give:spawn/skelenox/skelenox_pokeball
execute as @e[tag=skelenox,tag=en_capture,tag=masterball] run function give:spawn/skelenox/skelenox_masterball
execute as @e[tag=skelenox,tag=en_capture,tag=honorball] run function give:spawn/skelenox/skelenox_honorball
execute as @e[tag=skelenox,tag=en_capture,tag=superball] run function give:spawn/skelenox/skelenox_superball
execute as @e[tag=skelenox,tag=en_capture,tag=hyperball] run function give:spawn/skelenox/skelenox_hyperball

execute as @e[tag=lunala,tag=en_capture,tag=pokeball] run function give:spawn/lunala/lunala_pokeball
execute as @e[tag=lunala,tag=en_capture,tag=masterball] run function give:spawn/lunala/lunala_masterball
execute as @e[tag=lunala,tag=en_capture,tag=honorball] run function give:spawn/lunala/lunala_honorball
execute as @e[tag=lunala,tag=en_capture,tag=superball] run function give:spawn/lunala/lunala_superball
execute as @e[tag=lunala,tag=en_capture,tag=hyperball] run function give:spawn/lunala/lunala_hyperball

execute as @e[tag=groudon,tag=en_capture,tag=pokeball] run function give:spawn/groudon/groudon_pokeball
execute as @e[tag=groudon,tag=en_capture,tag=masterball] run function give:spawn/groudon/groudon_masterball
execute as @e[tag=groudon,tag=en_capture,tag=honorball] run function give:spawn/groudon/groudon_honorball
execute as @e[tag=groudon,tag=en_capture,tag=superball] run function give:spawn/groudon/groudon_superball
execute as @e[tag=groudon,tag=en_capture,tag=hyperball] run function give:spawn/groudon/groudon_hyperball

execute as @e[tag=mentali,tag=en_capture,tag=pokeball] run function give:spawn/mentali/mentali_pokeball
execute as @e[tag=mentali,tag=en_capture,tag=masterball] run function give:spawn/mentali/mentali_masterball
execute as @e[tag=mentali,tag=en_capture,tag=honorball] run function give:spawn/mentali/mentali_honorball
execute as @e[tag=mentali,tag=en_capture,tag=superball] run function give:spawn/mentali/mentali_superball
execute as @e[tag=mentali,tag=en_capture,tag=hyperball] run function give:spawn/mentali/mentali_hyperball

execute as @e[tag=givrali,tag=en_capture,tag=pokeball] run function give:spawn/givrali/givrali_pokeball
execute as @e[tag=givrali,tag=en_capture,tag=masterball] run function give:spawn/givrali/givrali_masterball
execute as @e[tag=givrali,tag=en_capture,tag=honorball] run function give:spawn/givrali/givrali_honorball
execute as @e[tag=givrali,tag=en_capture,tag=superball] run function give:spawn/givrali/givrali_superball
execute as @e[tag=givrali,tag=en_capture,tag=hyperball] run function give:spawn/givrali/givrali_hyperball

execute as @e[tag=nymphali,tag=en_capture,tag=pokeball] run function give:spawn/nymphali/nymphali_pokeball
execute as @e[tag=nymphali,tag=en_capture,tag=masterball] run function give:spawn/nymphali/nymphali_masterball
execute as @e[tag=nymphali,tag=en_capture,tag=honorball] run function give:spawn/nymphali/nymphali_honorball
execute as @e[tag=nymphali,tag=en_capture,tag=superball] run function give:spawn/nymphali/nymphali_superball
execute as @e[tag=nymphali,tag=en_capture,tag=hyperball] run function give:spawn/nymphali/nymphali_hyperball

execute as @e[tag=evoli,tag=en_capture,tag=pokeball] run function give:spawn/evoli/evoli_pokeball
execute as @e[tag=evoli,tag=en_capture,tag=masterball] run function give:spawn/evoli/evoli_masterball
execute as @e[tag=evoli,tag=en_capture,tag=honorball] run function give:spawn/evoli/evoli_honorball
execute as @e[tag=evoli,tag=en_capture,tag=superball] run function give:spawn/evoli/evoli_superball
execute as @e[tag=evoli,tag=en_capture,tag=hyperball] run function give:spawn/evoli/evoli_hyperball

execute as @e[tag=pyroli,tag=en_capture,tag=pokeball] run function give:spawn/pyroli/pyroli_pokeball
execute as @e[tag=pyroli,tag=en_capture,tag=masterball] run function give:spawn/pyroli/pyroli_masterball
execute as @e[tag=pyroli,tag=en_capture,tag=honorball] run function give:spawn/pyroli/pyroli_honorball
execute as @e[tag=pyroli,tag=en_capture,tag=superball] run function give:spawn/pyroli/pyroli_superball
execute as @e[tag=pyroli,tag=en_capture,tag=hyperball] run function give:spawn/pyroli/pyroli_hyperball

execute as @e[tag=voltali,tag=en_capture,tag=pokeball] run function give:spawn/voltali/voltali_pokeball
execute as @e[tag=voltali,tag=en_capture,tag=masterball] run function give:spawn/voltali/voltali_masterball
execute as @e[tag=voltali,tag=en_capture,tag=honorball] run function give:spawn/voltali/voltali_honorball
execute as @e[tag=voltali,tag=en_capture,tag=superball] run function give:spawn/voltali/voltali_superball
execute as @e[tag=voltali,tag=en_capture,tag=hyperball] run function give:spawn/voltali/voltali_hyperball

execute as @e[tag=phyllali,tag=en_capture,tag=pokeball] run function give:spawn/phyllali/phyllali_pokeball
execute as @e[tag=phyllali,tag=en_capture,tag=masterball] run function give:spawn/phyllali/phyllali_masterball
execute as @e[tag=phyllali,tag=en_capture,tag=honorball] run function give:spawn/phyllali/phyllali_honorball
execute as @e[tag=phyllali,tag=en_capture,tag=superball] run function give:spawn/phyllali/phyllali_superball
execute as @e[tag=phyllali,tag=en_capture,tag=hyperball] run function give:spawn/phyllali/phyllali_hyperball

execute as @e[tag=aquali,tag=en_capture,tag=pokeball] run function give:spawn/aquali/aquali_pokeball
execute as @e[tag=aquali,tag=en_capture,tag=masterball] run function give:spawn/aquali/aquali_masterball
execute as @e[tag=aquali,tag=en_capture,tag=honorball] run function give:spawn/aquali/aquali_honorball
execute as @e[tag=aquali,tag=en_capture,tag=superball] run function give:spawn/aquali/aquali_superball
execute as @e[tag=aquali,tag=en_capture,tag=hyperball] run function give:spawn/aquali/aquali_hyperball

execute as @e[tag=reshiram,tag=en_capture,tag=pokeball] run function give:spawn/reshiram/reshiram_pokeball
execute as @e[tag=reshiram,tag=en_capture,tag=masterball] run function give:spawn/reshiram/reshiram_masterball
execute as @e[tag=reshiram,tag=en_capture,tag=honorball] run function give:spawn/reshiram/reshiram_honorball
execute as @e[tag=reshiram,tag=en_capture,tag=superball] run function give:spawn/reshiram/reshiram_superball
execute as @e[tag=reshiram,tag=en_capture,tag=hyperball] run function give:spawn/reshiram/reshiram_hyperball

execute as @e[tag=porygon_z,tag=en_capture,tag=pokeball] run function give:spawn/porygon_z/porygon_z_pokeball
execute as @e[tag=porygon_z,tag=en_capture,tag=masterball] run function give:spawn/porygon_z/porygon_z_masterball
execute as @e[tag=porygon_z,tag=en_capture,tag=honorball] run function give:spawn/porygon_z/porygon_z_honorball
execute as @e[tag=porygon_z,tag=en_capture,tag=superball] run function give:spawn/porygon_z/porygon_z_superball
execute as @e[tag=porygon_z,tag=en_capture,tag=hyperball] run function give:spawn/porygon_z/porygon_z_hyperball

execute as @e[tag=porygon_2,tag=en_capture,tag=pokeball] run function give:spawn/porygon_2/porygon_2_pokeball
execute as @e[tag=porygon_2,tag=en_capture,tag=masterball] run function give:spawn/porygon_2/porygon_2_masterball
execute as @e[tag=porygon_2,tag=en_capture,tag=honorball] run function give:spawn/porygon_2/porygon_2_honorball
execute as @e[tag=porygon_2,tag=en_capture,tag=superball] run function give:spawn/porygon_2/porygon_2_superball
execute as @e[tag=porygon_2,tag=en_capture,tag=hyperball] run function give:spawn/porygon_2/porygon_2_hyperball

execute as @e[tag=porygon,tag=en_capture,tag=pokeball] run function give:spawn/porygon/porygon_pokeball
execute as @e[tag=porygon,tag=en_capture,tag=masterball] run function give:spawn/porygon/porygon_masterball
execute as @e[tag=porygon,tag=en_capture,tag=honorball] run function give:spawn/porygon/porygon_honorball
execute as @e[tag=porygon,tag=en_capture,tag=superball] run function give:spawn/porygon/porygon_superball
execute as @e[tag=porygon,tag=en_capture,tag=hyperball] run function give:spawn/porygon/porygon_hyperball