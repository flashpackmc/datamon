execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=kyogre] run function give:spawn/kyogre/kyogre_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=kyogre] run function give:spawn/kyogre/kyogre_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=kyogre] run function give:spawn/kyogre/kyogre_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=kyogre] run function give:spawn/kyogre/kyogre_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=kyogre] run function give:spawn/kyogre/kyogre_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=groudon] run function give:spawn/groudon/groudon_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=groudon] run function give:spawn/groudon/groudon_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=groudon] run function give:spawn/groudon/groudon_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=groudon] run function give:spawn/groudon/groudon_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=groudon] run function give:spawn/groudon/groudon_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=leviator] run function give:spawn/leviator/leviator_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=leviator] run function give:spawn/leviator/leviator_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=leviator] run function give:spawn/leviator/leviator_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=leviator] run function give:spawn/leviator/leviator_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=leviator] run function give:spawn/leviator/leviator_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=pierroteknik] run function give:spawn/pierroteknik/pierroteknik_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=pierroteknik] run function give:spawn/pierroteknik/pierroteknik_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=pierroteknik] run function give:spawn/pierroteknik/pierroteknik_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=pierroteknik] run function give:spawn/pierroteknik/pierroteknik_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=pierroteknik] run function give:spawn/pierroteknik/pierroteknik_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=bamboiselle] run function give:spawn/bamboiselle/bamboiselle_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=bamboiselle] run function give:spawn/bamboiselle/bamboiselle_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=bamboiselle] run function give:spawn/bamboiselle/bamboiselle_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=bamboiselle] run function give:spawn/bamboiselle/bamboiselle_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=bamboiselle] run function give:spawn/bamboiselle/bamboiselle_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=katagami] run function give:spawn/katagami/katagami_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=katagami] run function give:spawn/katagami/katagami_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=katagami] run function give:spawn/katagami/katagami_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=katagami] run function give:spawn/katagami/katagami_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=katagami] run function give:spawn/katagami/katagami_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=mouscoto] run function give:spawn/mouscoto/mouscoto_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=mouscoto] run function give:spawn/mouscoto/mouscoto_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=mouscoto] run function give:spawn/mouscoto/mouscoto_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=mouscoto] run function give:spawn/mouscoto/mouscoto_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=mouscoto] run function give:spawn/mouscoto/mouscoto_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=marshadow] run function give:spawn/marshadow/marshadow_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=marshadow] run function give:spawn/marshadow/marshadow_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=marshadow] run function give:spawn/marshadow/marshadow_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=marshadow] run function give:spawn/marshadow/marshadow_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=marshadow] run function give:spawn/marshadow/marshadow_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=cancrelove] run function give:spawn/cancrelove/cancrelove_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=cancrelove] run function give:spawn/cancrelove/cancrelove_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=cancrelove] run function give:spawn/cancrelove/cancrelove_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=cancrelove] run function give:spawn/cancrelove/cancrelove_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=cancrelove] run function give:spawn/cancrelove/cancrelove_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=demeteros] run function give:spawn/demeteros/demeteros_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=demeteros] run function give:spawn/demeteros/demeteros_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=demeteros] run function give:spawn/demeteros/demeteros_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=demeteros] run function give:spawn/demeteros/demeteros_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=demeteros] run function give:spawn/demeteros/demeteros_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=boreas] run function give:spawn/boreas/boreas_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=boreas] run function give:spawn/boreas/boreas_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=boreas] run function give:spawn/boreas/boreas_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=boreas] run function give:spawn/boreas/boreas_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=boreas] run function give:spawn/boreas/boreas_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=fulguris] run function give:spawn/fulguris/fulguris_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=fulguris] run function give:spawn/fulguris/fulguris_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=fulguris] run function give:spawn/fulguris/fulguris_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=fulguris] run function give:spawn/fulguris/fulguris_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=fulguris] run function give:spawn/fulguris/fulguris_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=magearna] run function give:spawn/magearna/magearna_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=magearna] run function give:spawn/magearna/magearna_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=magearna] run function give:spawn/magearna/magearna_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=magearna] run function give:spawn/magearna/magearna_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=magearna] run function give:spawn/magearna/magearna_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=regigigas] run function give:spawn/regigigas/regigigas_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=regigigas] run function give:spawn/regigigas/regigigas_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=regigigas] run function give:spawn/regigigas/regigigas_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=regigigas] run function give:spawn/regigigas/regigigas_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=regigigas] run function give:spawn/regigigas/regigigas_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=nymphali] run function give:spawn/nymphali/nymphali_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=nymphali] run function give:spawn/nymphali/nymphali_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=nymphali] run function give:spawn/nymphali/nymphali_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=nymphali] run function give:spawn/nymphali/nymphali_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=nymphali] run function give:spawn/nymphali/nymphali_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=pyroli] run function give:spawn/pyroli/pyroli_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=pyroli] run function give:spawn/pyroli/pyroli_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=pyroli] run function give:spawn/pyroli/pyroli_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=pyroli] run function give:spawn/pyroli/pyroli_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=pyroli] run function give:spawn/pyroli/pyroli_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=phyllali] run function give:spawn/phyllali/phyllali_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=phyllali] run function give:spawn/phyllali/phyllali_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=phyllali] run function give:spawn/phyllali/phyllali_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=phyllali] run function give:spawn/phyllali/phyllali_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=phyllali] run function give:spawn/phyllali/phyllali_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=givrali] run function give:spawn/givrali/givrali_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=givrali] run function give:spawn/givrali/givrali_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=givrali] run function give:spawn/givrali/givrali_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=givrali] run function give:spawn/givrali/givrali_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=givrali] run function give:spawn/givrali/givrali_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=evoli] run function give:spawn/evoli/evoli_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=evoli] run function give:spawn/evoli/evoli_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=evoli] run function give:spawn/evoli/evoli_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=evoli] run function give:spawn/evoli/evoli_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=evoli] run function give:spawn/evoli/evoli_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=kyurem_blanc] run function give:spawn/kyurem_blanc/kyurem_blanc_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=kyurem_blanc] run function give:spawn/kyurem_blanc/kyurem_blanc_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=kyurem_blanc] run function give:spawn/kyurem_blanc/kyurem_blanc_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=kyurem_blanc] run function give:spawn/kyurem_blanc/kyurem_blanc_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=kyurem_blanc] run function give:spawn/kyurem_blanc/kyurem_blanc_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=crehelf] run function give:spawn/crehelf/crehelf_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=crehelf] run function give:spawn/crehelf/crehelf_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=crehelf] run function give:spawn/crehelf/crehelf_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=crehelf] run function give:spawn/crehelf/crehelf_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=crehelf] run function give:spawn/crehelf/crehelf_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=crefollet] run function give:spawn/crefollet/crefollet_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=crefollet] run function give:spawn/crefollet/crefollet_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=crefollet] run function give:spawn/crefollet/crefollet_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=crefollet] run function give:spawn/crefollet/crefollet_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=crefollet] run function give:spawn/crefollet/crefollet_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=crefadet] run function give:spawn/crefadet/crefadet_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=crefadet] run function give:spawn/crefadet/crefadet_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=crefadet] run function give:spawn/crefadet/crefadet_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=crefadet] run function give:spawn/crefadet/crefadet_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=crefadet] run function give:spawn/crefadet/crefadet_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=ultra_necrozma,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=ultra_necrozma,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=ultra_necrozma,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=ultra_necrozma,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=ultra_necrozma,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=ultra_necrozma,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=ultra_necrozma,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=ultra_necrozma,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=ultra_necrozma,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=ultra_necrozma,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=necrozma_criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=necrozma_criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=necrozma_criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=necrozma_criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=necrozma_criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=voltali] run function give:spawn/voltali/voltali_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=voltali] run function give:spawn/voltali/voltali_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=voltali] run function give:spawn/voltali/voltali_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=voltali] run function give:spawn/voltali/voltali_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=voltali] run function give:spawn/voltali/voltali_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=reshiram] run function give:spawn/reshiram/reshiram_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=reshiram] run function give:spawn/reshiram/reshiram_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=reshiram] run function give:spawn/reshiram/reshiram_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=reshiram] run function give:spawn/reshiram/reshiram_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=reshiram] run function give:spawn/reshiram/reshiram_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=mentali] run function give:spawn/mentali/mentali_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=mentali] run function give:spawn/mentali/mentali_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=mentali] run function give:spawn/mentali/mentali_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=mentali] run function give:spawn/mentali/mentali_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=mentali] run function give:spawn/mentali/mentali_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=aquali] run function give:spawn/aquali/aquali_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=aquali] run function give:spawn/aquali/aquali_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=aquali] run function give:spawn/aquali/aquali_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=aquali] run function give:spawn/aquali/aquali_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=aquali] run function give:spawn/aquali/aquali_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=porygon_z] run function give:spawn/porygon_z/porygon_z_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=porygon_z] run function give:spawn/porygon_z/porygon_z_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=porygon_z] run function give:spawn/porygon_z/porygon_z_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=porygon_z] run function give:spawn/porygon_z/porygon_z_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=porygon_z] run function give:spawn/porygon_z/porygon_z_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=porygon_2] run function give:spawn/porygon_2/porygon_2_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=porygon_2] run function give:spawn/porygon_2/porygon_2_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=porygon_2] run function give:spawn/porygon_2/porygon_2_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=porygon_2] run function give:spawn/porygon_2/porygon_2_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=porygon_2] run function give:spawn/porygon_2/porygon_2_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=porygon] run function give:spawn/porygon/porygon_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=porygon] run function give:spawn/porygon/porygon_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=porygon] run function give:spawn/porygon/porygon_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=porygon] run function give:spawn/porygon/porygon_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=porygon] run function give:spawn/porygon/porygon_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=noctali] run function give:spawn/noctali/noctali_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=noctali] run function give:spawn/noctali/noctali_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=noctali] run function give:spawn/noctali/noctali_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=noctali] run function give:spawn/noctali/noctali_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=noctali] run function give:spawn/noctali/noctali_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=dracaufeu] run function give:spawn/dracaufeu/dracaufeu_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=dracaufeu] run function give:spawn/dracaufeu/dracaufeu_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=dracaufeu] run function give:spawn/dracaufeu/dracaufeu_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=dracaufeu] run function give:spawn/dracaufeu/dracaufeu_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=dracaufeu] run function give:spawn/dracaufeu/dracaufeu_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=pachirisu] run function give:spawn/pachirisu/pachirisu_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=pachirisu] run function give:spawn/pachirisu/pachirisu_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=pachirisu] run function give:spawn/pachirisu/pachirisu_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=pachirisu] run function give:spawn/pachirisu/pachirisu_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=pachirisu] run function give:spawn/pachirisu/pachirisu_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=mimiqui] run function give:spawn/mimiqui/mimiqui_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=mimiqui] run function give:spawn/mimiqui/mimiqui_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=mimiqui] run function give:spawn/mimiqui/mimiqui_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=mimiqui] run function give:spawn/mimiqui/mimiqui_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=mimiqui] run function give:spawn/mimiqui/mimiqui_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=magicarpe] run function give:spawn/magicarpe/magicarpe_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=magicarpe] run function give:spawn/magicarpe/magicarpe_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=magicarpe] run function give:spawn/magicarpe/magicarpe_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=magicarpe] run function give:spawn/magicarpe/magicarpe_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=magicarpe] run function give:spawn/magicarpe/magicarpe_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=teraclope] run function give:spawn/teraclope/teraclope_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=teraclope] run function give:spawn/teraclope/teraclope_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=teraclope] run function give:spawn/teraclope/teraclope_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=teraclope] run function give:spawn/teraclope/teraclope_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=teraclope] run function give:spawn/teraclope/teraclope_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=noctunoir] run function give:spawn/noctunoir/noctunoir_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=noctunoir] run function give:spawn/noctunoir/noctunoir_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=noctunoir] run function give:spawn/noctunoir/noctunoir_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=noctunoir] run function give:spawn/noctunoir/noctunoir_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=noctunoir] run function give:spawn/noctunoir/noctunoir_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=skelenox] run function give:spawn/skelenox/skelenox_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=skelenox] run function give:spawn/skelenox/skelenox_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=skelenox] run function give:spawn/skelenox/skelenox_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=skelenox] run function give:spawn/skelenox/skelenox_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=skelenox] run function give:spawn/skelenox/skelenox_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=minidraco] run function give:spawn/minidraco/minidraco_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=minidraco] run function give:spawn/minidraco/minidraco_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=minidraco] run function give:spawn/minidraco/minidraco_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=minidraco] run function give:spawn/minidraco/minidraco_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=minidraco] run function give:spawn/minidraco/minidraco_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=draco] run function give:spawn/draco/draco_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=draco] run function give:spawn/draco/draco_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=draco] run function give:spawn/draco/draco_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=draco] run function give:spawn/draco/draco_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=draco] run function give:spawn/draco/draco_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=dracolosse] run function give:spawn/dracolosse/dracolosse_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=dracolosse] run function give:spawn/dracolosse/dracolosse_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=dracolosse] run function give:spawn/dracolosse/dracolosse_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=dracolosse] run function give:spawn/dracolosse/dracolosse_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=dracolosse] run function give:spawn/dracolosse/dracolosse_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=laggron] run function give:spawn/laggron/laggron_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=laggron] run function give:spawn/laggron/laggron_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=laggron] run function give:spawn/laggron/laggron_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=laggron] run function give:spawn/laggron/laggron_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=laggron] run function give:spawn/laggron/laggron_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=reptincel] run function give:spawn/reptincel/reptincel_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=reptincel] run function give:spawn/reptincel/reptincel_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=reptincel] run function give:spawn/reptincel/reptincel_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=reptincel] run function give:spawn/reptincel/reptincel_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=reptincel] run function give:spawn/reptincel/reptincel_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=salameche] run function give:spawn/salameche/salameche_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=salameche] run function give:spawn/salameche/salameche_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=salameche] run function give:spawn/salameche/salameche_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=salameche] run function give:spawn/salameche/salameche_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=salameche] run function give:spawn/salameche/salameche_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=genesect] run function give:spawn/genesect/genesect_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=genesect] run function give:spawn/genesect/genesect_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=genesect] run function give:spawn/genesect/genesect_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=genesect] run function give:spawn/genesect/genesect_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=genesect] run function give:spawn/genesect/genesect_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=brasegali] run function give:spawn/brasegali/brasegali_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=brasegali] run function give:spawn/brasegali/brasegali_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=brasegali] run function give:spawn/brasegali/brasegali_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=brasegali] run function give:spawn/brasegali/brasegali_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=brasegali] run function give:spawn/brasegali/brasegali_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=flobio] run function give:spawn/flobio/flobio_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=flobio] run function give:spawn/flobio/flobio_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=flobio] run function give:spawn/flobio/flobio_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=flobio] run function give:spawn/flobio/flobio_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=flobio] run function give:spawn/flobio/flobio_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=galifeu] run function give:spawn/galifeu/galifeu_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=galifeu] run function give:spawn/galifeu/galifeu_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=galifeu] run function give:spawn/galifeu/galifeu_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=galifeu] run function give:spawn/galifeu/galifeu_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=galifeu] run function give:spawn/galifeu/galifeu_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=poussifeu] run function give:spawn/poussifeu/poussifeu_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=poussifeu] run function give:spawn/poussifeu/poussifeu_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=poussifeu] run function give:spawn/poussifeu/poussifeu_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=poussifeu] run function give:spawn/poussifeu/poussifeu_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=poussifeu] run function give:spawn/poussifeu/poussifeu_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=gobou] run function give:spawn/gobou/gobou_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=gobou] run function give:spawn/gobou/gobou_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=gobou] run function give:spawn/gobou/gobou_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=gobou] run function give:spawn/gobou/gobou_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=gobou] run function give:spawn/gobou/gobou_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=kyurem] run function give:spawn/kyurem/kyurem_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=kyurem] run function give:spawn/kyurem/kyurem_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=kyurem] run function give:spawn/kyurem/kyurem_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=kyurem] run function give:spawn/kyurem/kyurem_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=kyurem] run function give:spawn/kyurem/kyurem_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=kyurem_noir] run function give:spawn/kyurem_noir/kyurem_noir_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=kyurem_noir] run function give:spawn/kyurem_noir/kyurem_noir_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=kyurem_noir] run function give:spawn/kyurem_noir/kyurem_noir_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=kyurem_noir] run function give:spawn/kyurem_noir/kyurem_noir_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=kyurem_noir] run function give:spawn/kyurem_noir/kyurem_noir_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=necrozma] run function give:spawn/necrozma/necrozma_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=necrozma] run function give:spawn/necrozma/necrozma_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=necrozma] run function give:spawn/necrozma/necrozma_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=necrozma] run function give:spawn/necrozma/necrozma_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=necrozma] run function give:spawn/necrozma/necrozma_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=zekrom] run function give:spawn/zekrom/zekrom_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=zekrom] run function give:spawn/zekrom/zekrom_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=zekrom] run function give:spawn/zekrom/zekrom_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=zekrom] run function give:spawn/zekrom/zekrom_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=zekrom] run function give:spawn/zekrom/zekrom_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=coatox] run function give:spawn/coatox/coatox_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=coatox] run function give:spawn/coatox/coatox_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=coatox] run function give:spawn/coatox/coatox_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=coatox] run function give:spawn/coatox/coatox_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=coatox] run function give:spawn/coatox/coatox_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=lunala] run function give:spawn/lunala/lunala_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=lunala] run function give:spawn/lunala/lunala_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=lunala] run function give:spawn/lunala/lunala_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=lunala] run function give:spawn/lunala/lunala_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=lunala] run function give:spawn/lunala/lunala_hyperball

execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captPokeball,tag=necrozma_ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_pokeball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captMasterball,tag=necrozma_ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_masterball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHonorball,tag=necrozma_ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_honorball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captSuperball,tag=necrozma_ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_superball
execute if entity @a[scores={tour=2}] as @e[tag=combat,tag=captHyperball,tag=necrozma_ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_hyperball


execute if entity @e[tag=combat,tag=captPokeball] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute if entity @e[tag=combat,tag=captMasterball] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute if entity @e[tag=combat,tag=captHonorball] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute if entity @e[tag=combat,tag=captSuperball] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute if entity @e[tag=combat,tag=captHyperball] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1