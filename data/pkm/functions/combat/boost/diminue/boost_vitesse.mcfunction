


scoreboard players set @s[scores={boost_vitesse=7..12}] multiplieur2 2
scoreboard players operation @s[scores={boost_vitesse=7..12}] multiplieur1 = @s[scores={boost_vitesse=7..12}] vitesse_origine
scoreboard players operation @s[scores={boost_vitesse=7..12}] multiplieur1 /= @s[scores={boost_vitesse=7..12}] multiplieur2
scoreboard players operation @s[scores={boost_vitesse=7..12}] multiplieur2 = @s[scores={boost_vitesse=7..12}] multiplieur1
execute store result score @s[scores={boost_vitesse=7..12}] multiplieur1 run data get entity @s[scores={boost_vitesse=7..12}] ArmorItems[3].tag.vitesse
scoreboard players operation @s[scores={boost_vitesse=7..12}] multiplieur1 -= @s[scores={boost_vitesse=7..12}] multiplieur2
execute store result entity @s[scores={boost_vitesse=7..12}] ArmorItems[3].tag.vitesse float 1 run scoreboard players get @s[scores={boost_vitesse=7..12}] multiplieur1
#enleve 50% de la vitesse d'origine



scoreboard players set @s[scores={boost_vitesse=6}] multiplieur2 33
scoreboard players set @s[scores={boost_vitesse=5}] multiplieur2 50
scoreboard players set @s[scores={boost_vitesse=4}] multiplieur2 60
scoreboard players set @s[scores={boost_vitesse=3}] multiplieur2 66
scoreboard players set @s[scores={boost_vitesse=2}] multiplieur2 71
scoreboard players set @s[scores={boost_vitesse=1}] multiplieur2 75

scoreboard players operation @s[scores={boost_vitesse=1..6}] multiplieur1 = @s[scores={boost_vitesse=1..6}] vitesse_origine
scoreboard players operation @s[scores={boost_vitesse=1..6}] multiplieur1 *= @s[scores={boost_vitesse=1..6}] multiplieur2
scoreboard players set @s[scores={boost_vitesse=1..6}] multiplieur2 100
scoreboard players operation @s[scores={boost_vitesse=1..6}] multiplieur1 /= @s[scores={boost_vitesse=1..6}] multiplieur2
scoreboard players operation @s[scores={boost_vitesse=1..6}] multiplieur2 = @s[scores={boost_vitesse=1..6}] multiplieur1
scoreboard players operation @s[scores={boost_vitesse=1..6}] multiplieur1 = @s[scores={boost_vitesse=1..6}] vitesse_origine
scoreboard players operation @s[scores={boost_vitesse=1..6}] multiplieur1 -= @s[scores={boost_vitesse=1..6}] multiplieur2
execute store result entity @s[scores={boost_vitesse=1..6}] ArmorItems[3].tag.vitesse float 1 run scoreboard players get @s[scores={boost_vitesse=1..6}] multiplieur1
#differents sur les boosts < stat origine







scoreboard players remove @s[scores={boost_vitesse=1..12}] boost_vitesse 1