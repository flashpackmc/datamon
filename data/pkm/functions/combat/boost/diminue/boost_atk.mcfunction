


scoreboard players set @s[scores={boost_atk=7..12}] multiplieur2 2
scoreboard players operation @s[scores={boost_atk=7..12}] multiplieur1 = @s[scores={boost_atk=7..12}] atk_origine
scoreboard players operation @s[scores={boost_atk=7..12}] multiplieur1 /= @s[scores={boost_atk=7..12}] multiplieur2
scoreboard players operation @s[scores={boost_atk=7..12}] multiplieur2 = @s[scores={boost_atk=7..12}] multiplieur1
execute store result score @s[scores={boost_atk=7..12}] multiplieur1 run data get entity @s[scores={boost_atk=7..12}] ArmorItems[3].tag.atk
scoreboard players operation @s[scores={boost_atk=7..12}] multiplieur1 -= @s[scores={boost_atk=7..12}] multiplieur2
execute store result entity @s[scores={boost_atk=7..12}] ArmorItems[3].tag.atk float 1 run scoreboard players get @s[scores={boost_atk=7..12}] multiplieur1
#enleve 50% de l'atk d'origine



scoreboard players set @s[scores={boost_atk=6}] multiplieur2 33
scoreboard players set @s[scores={boost_atk=5}] multiplieur2 50
scoreboard players set @s[scores={boost_atk=4}] multiplieur2 60
scoreboard players set @s[scores={boost_atk=3}] multiplieur2 66
scoreboard players set @s[scores={boost_atk=2}] multiplieur2 71
scoreboard players set @s[scores={boost_atk=1}] multiplieur2 75

scoreboard players operation @s[scores={boost_atk=1..6}] multiplieur1 = @s[scores={boost_atk=1..6}] atk_origine
scoreboard players operation @s[scores={boost_atk=1..6}] multiplieur1 *= @s[scores={boost_atk=1..6}] multiplieur2
scoreboard players set @s[scores={boost_atk=1..6}] multiplieur2 100
scoreboard players operation @s[scores={boost_atk=1..6}] multiplieur1 /= @s[scores={boost_atk=1..6}] multiplieur2
scoreboard players operation @s[scores={boost_atk=1..6}] multiplieur2 = @s[scores={boost_atk=1..6}] multiplieur1
scoreboard players operation @s[scores={boost_atk=1..6}] multiplieur1 = @s[scores={boost_atk=1..6}] atk_origine
scoreboard players operation @s[scores={boost_atk=1..6}] multiplieur1 -= @s[scores={boost_atk=1..6}] multiplieur2
execute store result entity @s[scores={boost_atk=1..6}] ArmorItems[3].tag.atk float 1 run scoreboard players get @s[scores={boost_atk=1..6}] multiplieur1
#differents sur les boosts < stat origine







scoreboard players remove @s[scores={boost_atk=1..12}] boost_atk 1