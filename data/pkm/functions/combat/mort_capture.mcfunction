execute as @e[tag=captureAS,tag=kyogreAS,tag=HonorAS] run function give:spawn/kyogre/kyogre_honorball
execute as @e[tag=captureAS,tag=kyogreAS,tag=PokeAS] run function give:spawn/kyogre/kyogre_pokeball
execute as @e[tag=captureAS,tag=kyogreAS,tag=MasterAS] run function give:spawn/kyogre/kyogre_masterball
execute as @e[tag=captureAS,tag=kyogreAS,tag=SuperAS] run function give:spawn/kyogre/kyogre_superball
execute as @e[tag=captureAS,tag=kyogreAS,tag=HyperAS] run function give:spawn/kyogre/kyogre_hyperball

execute as @e[tag=captureAS,tag=groudonAS,tag=HonorAS] run function give:spawn/groudon/groudon_honorball
execute as @e[tag=captureAS,tag=groudonAS,tag=PokeAS] run function give:spawn/groudon/groudon_pokeball
execute as @e[tag=captureAS,tag=groudonAS,tag=MasterAS] run function give:spawn/groudon/groudon_masterball
execute as @e[tag=captureAS,tag=groudonAS,tag=SuperAS] run function give:spawn/groudon/groudon_superball
execute as @e[tag=captureAS,tag=groudonAS,tag=HyperAS] run function give:spawn/groudon/groudon_hyperball

execute as @e[tag=captureAS,tag=leviatorAS,tag=HonorAS] run function give:spawn/leviator/leviator_honorball
execute as @e[tag=captureAS,tag=leviatorAS,tag=PokeAS] run function give:spawn/leviator/leviator_pokeball
execute as @e[tag=captureAS,tag=leviatorAS,tag=MasterAS] run function give:spawn/leviator/leviator_masterball
execute as @e[tag=captureAS,tag=leviatorAS,tag=SuperAS] run function give:spawn/leviator/leviator_superball
execute as @e[tag=captureAS,tag=leviatorAS,tag=HyperAS] run function give:spawn/leviator/leviator_hyperball

execute as @e[tag=captureAS,tag=marshadowAS,tag=HonorAS] run function give:spawn/marshadow/marshadow_honorball
execute as @e[tag=captureAS,tag=marshadowAS,tag=PokeAS] run function give:spawn/marshadow/marshadow_pokeball
execute as @e[tag=captureAS,tag=marshadowAS,tag=MasterAS] run function give:spawn/marshadow/marshadow_masterball
execute as @e[tag=captureAS,tag=marshadowAS,tag=SuperAS] run function give:spawn/marshadow/marshadow_superball
execute as @e[tag=captureAS,tag=marshadowAS,tag=HyperAS] run function give:spawn/marshadow/marshadow_hyperball

execute as @e[tag=captureAS,tag=regigigasAS,tag=HonorAS] run function give:spawn/regigigas/regigigas_honorball
execute as @e[tag=captureAS,tag=regigigasAS,tag=PokeAS] run function give:spawn/regigigas/regigigas_pokeball
execute as @e[tag=captureAS,tag=regigigasAS,tag=MasterAS] run function give:spawn/regigigas/regigigas_masterball
execute as @e[tag=captureAS,tag=regigigasAS,tag=SuperAS] run function give:spawn/regigigas/regigigas_superball
execute as @e[tag=captureAS,tag=regigigasAS,tag=HyperAS] run function give:spawn/regigigas/regigigas_hyperball

execute as @e[tag=captureAS,tag=cancreloveAS,tag=HonorAS] run function give:spawn/cancrelove/cancrelove_honorball
execute as @e[tag=captureAS,tag=cancreloveAS,tag=PokeAS] run function give:spawn/cancrelove/cancrelove_pokeball
execute as @e[tag=captureAS,tag=cancreloveAS,tag=MasterAS] run function give:spawn/cancrelove/cancrelove_masterball
execute as @e[tag=captureAS,tag=cancreloveAS,tag=SuperAS] run function give:spawn/cancrelove/cancrelove_superball
execute as @e[tag=captureAS,tag=cancreloveAS,tag=HyperAS] run function give:spawn/cancrelove/cancrelove_hyperball

execute as @e[tag=captureAS,tag=mouscotoAS,tag=HonorAS] run function give:spawn/mouscoto/mouscoto_honorball
execute as @e[tag=captureAS,tag=mouscotoAS,tag=PokeAS] run function give:spawn/mouscoto/mouscoto_pokeball
execute as @e[tag=captureAS,tag=mouscotoAS,tag=MasterAS] run function give:spawn/mouscoto/mouscoto_masterball
execute as @e[tag=captureAS,tag=mouscotoAS,tag=SuperAS] run function give:spawn/mouscoto/mouscoto_superball
execute as @e[tag=captureAS,tag=mouscotoAS,tag=HyperAS] run function give:spawn/mouscoto/mouscoto_hyperball

execute as @e[tag=captureAS,tag=bamboiselleAS,tag=HonorAS] run function give:spawn/bamboiselle/bamboiselle_honorball
execute as @e[tag=captureAS,tag=bamboiselleAS,tag=PokeAS] run function give:spawn/bamboiselle/bamboiselle_pokeball
execute as @e[tag=captureAS,tag=bamboiselleAS,tag=MasterAS] run function give:spawn/bamboiselle/bamboiselle_masterball
execute as @e[tag=captureAS,tag=bamboiselleAS,tag=SuperAS] run function give:spawn/bamboiselle/bamboiselle_superball
execute as @e[tag=captureAS,tag=bamboiselleAS,tag=HyperAS] run function give:spawn/bamboiselle/bamboiselle_hyperball

execute as @e[tag=captureAS,tag=katagamiAS,tag=HonorAS] run function give:spawn/katagami/katagami_honorball
execute as @e[tag=captureAS,tag=katagamiAS,tag=PokeAS] run function give:spawn/katagami/katagami_pokeball
execute as @e[tag=captureAS,tag=katagamiAS,tag=MasterAS] run function give:spawn/katagami/katagami_masterball
execute as @e[tag=captureAS,tag=katagamiAS,tag=SuperAS] run function give:spawn/katagami/katagami_superball
execute as @e[tag=captureAS,tag=katagamiAS,tag=HyperAS] run function give:spawn/katagami/katagami_hyperball

execute as @e[tag=captureAS,tag=magearnaAS,tag=HonorAS] run function give:spawn/magearna/magearna_honorball
execute as @e[tag=captureAS,tag=magearnaAS,tag=PokeAS] run function give:spawn/magearna/magearna_pokeball
execute as @e[tag=captureAS,tag=magearnaAS,tag=MasterAS] run function give:spawn/magearna/magearna_masterball
execute as @e[tag=captureAS,tag=magearnaAS,tag=SuperAS] run function give:spawn/magearna/magearna_superball
execute as @e[tag=captureAS,tag=magearnaAS,tag=HyperAS] run function give:spawn/magearna/magearna_hyperball

execute as @e[tag=captureAS,tag=demeterosAS,tag=HonorAS] run function give:spawn/demeteros/demeteros_honorball
execute as @e[tag=captureAS,tag=demeterosAS,tag=PokeAS] run function give:spawn/demeteros/demeteros_pokeball
execute as @e[tag=captureAS,tag=demeterosAS,tag=MasterAS] run function give:spawn/demeteros/demeteros_masterball
execute as @e[tag=captureAS,tag=demeterosAS,tag=SuperAS] run function give:spawn/demeteros/demeteros_superball
execute as @e[tag=captureAS,tag=demeterosAS,tag=HyperAS] run function give:spawn/demeteros/demeteros_hyperball

execute as @e[tag=captureAS,tag=fulgurisAS,tag=HonorAS] run function give:spawn/fulguris/fulguris_honorball
execute as @e[tag=captureAS,tag=fulgurisAS,tag=PokeAS] run function give:spawn/fulguris/fulguris_pokeball
execute as @e[tag=captureAS,tag=fulgurisAS,tag=MasterAS] run function give:spawn/fulguris/fulguris_masterball
execute as @e[tag=captureAS,tag=fulgurisAS,tag=SuperAS] run function give:spawn/fulguris/fulguris_superball
execute as @e[tag=captureAS,tag=fulgurisAS,tag=HyperAS] run function give:spawn/fulguris/fulguris_hyperball

execute as @e[tag=captureAS,tag=boreasAS,tag=HonorAS] run function give:spawn/boreas/boreas_honorball
execute as @e[tag=captureAS,tag=boreasAS,tag=PokeAS] run function give:spawn/boreas/boreas_pokeball
execute as @e[tag=captureAS,tag=boreasAS,tag=MasterAS] run function give:spawn/boreas/boreas_masterball
execute as @e[tag=captureAS,tag=boreasAS,tag=SuperAS] run function give:spawn/boreas/boreas_superball
execute as @e[tag=captureAS,tag=boreasAS,tag=HyperAS] run function give:spawn/boreas/boreas_hyperball

execute as @e[tag=captureAS,tag=pierroteknikAS,tag=HonorAS] run function give:spawn/pierroteknik/pierroteknik_honorball
execute as @e[tag=captureAS,tag=pierroteknikAS,tag=PokeAS] run function give:spawn/pierroteknik/pierroteknik_pokeball
execute as @e[tag=captureAS,tag=pierroteknikAS,tag=MasterAS] run function give:spawn/pierroteknik/pierroteknik_masterball
execute as @e[tag=captureAS,tag=pierroteknikAS,tag=SuperAS] run function give:spawn/pierroteknik/pierroteknik_superball
execute as @e[tag=captureAS,tag=pierroteknikAS,tag=HyperAS] run function give:spawn/pierroteknik/pierroteknik_hyperball

execute as @e[tag=captureAS,tag=evoliAS,tag=HonorAS] run function give:spawn/evoli/evoli_honorball
execute as @e[tag=captureAS,tag=evoliAS,tag=PokeAS] run function give:spawn/evoli/evoli_pokeball
execute as @e[tag=captureAS,tag=evoliAS,tag=MasterAS] run function give:spawn/evoli/evoli_masterball
execute as @e[tag=captureAS,tag=evoliAS,tag=SuperAS] run function give:spawn/evoli/evoli_superball
execute as @e[tag=captureAS,tag=evoliAS,tag=HyperAS] run function give:spawn/evoli/evoli_hyperball

execute as @e[tag=captureAS,tag=necrozma_criniereAS,tag=HonorAS] run function give:spawn/necrozma_criniere/necrozma_criniere_honorball
execute as @e[tag=captureAS,tag=necrozma_criniereAS,tag=PokeAS] run function give:spawn/necrozma_criniere/necrozma_criniere_pokeball
execute as @e[tag=captureAS,tag=necrozma_criniereAS,tag=MasterAS] run function give:spawn/necrozma_criniere/necrozma_criniere_masterball
execute as @e[tag=captureAS,tag=necrozma_criniereAS,tag=SuperAS] run function give:spawn/necrozma_criniere/necrozma_criniere_superball
execute as @e[tag=captureAS,tag=necrozma_criniereAS,tag=HyperAS] run function give:spawn/necrozma_criniere/necrozma_criniere_hyperball

execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=HonorAS,tag=ailesAS] run function give:spawn/necrozma_ailes/necrozma_ailes_honorball
execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=PokeAS,tag=ailesAS] run function give:spawn/necrozma_ailes/necrozma_ailes_pokeball
execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=MasterAS,tag=ailesAS] run function give:spawn/necrozma_ailes/necrozma_ailes_masterball
execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=SuperAS,tag=ailesAS] run function give:spawn/necrozma_ailes/necrozma_ailes_superball
execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=HyperAS,tag=ailesAS] run function give:spawn/necrozma_ailes/necrozma_ailes_hyperball

execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=HonorAS,tag=criniereAS] run function give:spawn/necrozma_criniere/necrozma_criniere_honorball
execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=PokeAS,tag=criniereAS] run function give:spawn/necrozma_criniere/necrozma_criniere_pokeball
execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=MasterAS,tag=criniereAS] run function give:spawn/necrozma_criniere/necrozma_criniere_masterball
execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=SuperAS,tag=criniereAS] run function give:spawn/necrozma_criniere/necrozma_criniere_superball
execute as @e[tag=captureAS,tag=ultra_necrozmaAS,tag=HyperAS,tag=criniereAS] run function give:spawn/necrozma_criniere/necrozma_criniere_hyperball

execute as @e[tag=captureAS,tag=crefolletAS,tag=HonorAS] run function give:spawn/crefollet/crefollet_honorball
execute as @e[tag=captureAS,tag=crefolletAS,tag=PokeAS] run function give:spawn/crefollet/crefollet_pokeball
execute as @e[tag=captureAS,tag=crefolletAS,tag=MasterAS] run function give:spawn/crefollet/crefollet_masterball
execute as @e[tag=captureAS,tag=crefolletAS,tag=SuperAS] run function give:spawn/crefollet/crefollet_superball
execute as @e[tag=captureAS,tag=crefolletAS,tag=HyperAS] run function give:spawn/crefollet/crefollet_hyperball

execute as @e[tag=captureAS,tag=crehelfAS,tag=HonorAS] run function give:spawn/crehelf/crehelf_honorball
execute as @e[tag=captureAS,tag=crehelfAS,tag=PokeAS] run function give:spawn/crehelf/crehelf_pokeball
execute as @e[tag=captureAS,tag=crehelfAS,tag=MasterAS] run function give:spawn/crehelf/crehelf_masterball
execute as @e[tag=captureAS,tag=crehelfAS,tag=SuperAS] run function give:spawn/crehelf/crehelf_superball
execute as @e[tag=captureAS,tag=crehelfAS,tag=HyperAS] run function give:spawn/crehelf/crehelf_hyperball

execute as @e[tag=captureAS,tag=crefadetAS,tag=HonorAS] run function give:spawn/crefadet/crefadet_honorball
execute as @e[tag=captureAS,tag=crefadetAS,tag=PokeAS] run function give:spawn/crefadet/crefadet_pokeball
execute as @e[tag=captureAS,tag=crefadetAS,tag=MasterAS] run function give:spawn/crefadet/crefadet_masterball
execute as @e[tag=captureAS,tag=crefadetAS,tag=SuperAS] run function give:spawn/crefadet/crefadet_superball
execute as @e[tag=captureAS,tag=crefadetAS,tag=HyperAS] run function give:spawn/crefadet/crefadet_hyperball

execute as @e[tag=captureAS,tag=kyurem_blancAS,tag=HonorAS] run function give:spawn/kyurem_blanc/kyurem_blanc_honorball
execute as @e[tag=captureAS,tag=kyurem_blancAS,tag=PokeAS] run function give:spawn/kyurem_blanc/kyurem_blanc_pokeball
execute as @e[tag=captureAS,tag=kyurem_blancAS,tag=MasterAS] run function give:spawn/kyurem_blanc/kyurem_blanc_masterball
execute as @e[tag=captureAS,tag=kyurem_blancAS,tag=SuperAS] run function give:spawn/kyurem_blanc/kyurem_blanc_superball
execute as @e[tag=captureAS,tag=kyurem_blancAS,tag=HyperAS] run function give:spawn/kyurem_blanc/kyurem_blanc_hyperball

execute as @e[tag=captureAS,tag=porygon_zAS,tag=HonorAS] run function give:spawn/porygon_z/porygon_z_honorball
execute as @e[tag=captureAS,tag=porygon_zAS,tag=PokeAS] run function give:spawn/porygon_z/porygon_z_pokeball
execute as @e[tag=captureAS,tag=porygon_zAS,tag=MasterAS] run function give:spawn/porygon_z/porygon_z_masterball
execute as @e[tag=captureAS,tag=porygon_zAS,tag=SuperAS] run function give:spawn/porygon_z/porygon_z_superball
execute as @e[tag=captureAS,tag=porygon_zAS,tag=HyperAS] run function give:spawn/porygon_z/porygon_z_hyperball

execute as @e[tag=captureAS,tag=porygon_2AS,tag=HonorAS] run function give:spawn/porygon_2/porygon_2_honorball
execute as @e[tag=captureAS,tag=porygon_2AS,tag=PokeAS] run function give:spawn/porygon_2/porygon_2_pokeball
execute as @e[tag=captureAS,tag=porygon_2AS,tag=MasterAS] run function give:spawn/porygon_2/porygon_2_masterball
execute as @e[tag=captureAS,tag=porygon_2AS,tag=SuperAS] run function give:spawn/porygon_2/porygon_2_superball
execute as @e[tag=captureAS,tag=porygon_2AS,tag=HyperAS] run function give:spawn/porygon_2/porygon_2_hyperball

execute as @e[tag=captureAS,tag=porygonAS,tag=HonorAS] run function give:spawn/porygon/porygon_honorball
execute as @e[tag=captureAS,tag=porygonAS,tag=PokeAS] run function give:spawn/porygon/porygon_pokeball
execute as @e[tag=captureAS,tag=porygonAS,tag=MasterAS] run function give:spawn/porygon/porygon_masterball
execute as @e[tag=captureAS,tag=porygonAS,tag=SuperAS] run function give:spawn/porygon/porygon_superball
execute as @e[tag=captureAS,tag=porygonAS,tag=HyperAS] run function give:spawn/porygon/porygon_hyperball

execute as @e[tag=captureAS,tag=mentaliAS,tag=HonorAS] run function give:spawn/mentali/mentali_honorball
execute as @e[tag=captureAS,tag=mentaliAS,tag=PokeAS] run function give:spawn/mentali/mentali_pokeball
execute as @e[tag=captureAS,tag=mentaliAS,tag=MasterAS] run function give:spawn/mentali/mentali_masterball
execute as @e[tag=captureAS,tag=mentaliAS,tag=SuperAS] run function give:spawn/mentali/mentali_superball
execute as @e[tag=captureAS,tag=mentaliAS,tag=HyperAS] run function give:spawn/mentali/mentali_hyperball

execute as @e[tag=captureAS,tag=phyllaliAS,tag=HonorAS] run function give:spawn/phyllali/phyllali_honorball
execute as @e[tag=captureAS,tag=phyllaliAS,tag=PokeAS] run function give:spawn/phyllali/phyllali_pokeball
execute as @e[tag=captureAS,tag=phyllaliAS,tag=MasterAS] run function give:spawn/phyllali/phyllali_masterball
execute as @e[tag=captureAS,tag=phyllaliAS,tag=SuperAS] run function give:spawn/phyllali/phyllali_superball
execute as @e[tag=captureAS,tag=phyllaliAS,tag=HyperAS] run function give:spawn/phyllali/phyllali_hyperball

execute as @e[tag=captureAS,tag=givraliAS,tag=HonorAS] run function give:spawn/givrali/givrali_honorball
execute as @e[tag=captureAS,tag=givraliAS,tag=PokeAS] run function give:spawn/givrali/givrali_pokeball
execute as @e[tag=captureAS,tag=givraliAS,tag=MasterAS] run function give:spawn/givrali/givrali_masterball
execute as @e[tag=captureAS,tag=givraliAS,tag=SuperAS] run function give:spawn/givrali/givrali_superball
execute as @e[tag=captureAS,tag=givraliAS,tag=HyperAS] run function give:spawn/givrali/givrali_hyperball

execute as @e[tag=captureAS,tag=voltaliAS,tag=HonorAS] run function give:spawn/voltali/voltali_honorball
execute as @e[tag=captureAS,tag=voltaliAS,tag=PokeAS] run function give:spawn/voltali/voltali_pokeball
execute as @e[tag=captureAS,tag=voltaliAS,tag=MasterAS] run function give:spawn/voltali/voltali_masterball
execute as @e[tag=captureAS,tag=voltaliAS,tag=SuperAS] run function give:spawn/voltali/voltali_superball
execute as @e[tag=captureAS,tag=voltaliAS,tag=HyperAS] run function give:spawn/voltali/voltali_hyperball

execute as @e[tag=captureAS,tag=reshiramAS,tag=HonorAS] run function give:spawn/reshiram/reshiram_honorball
execute as @e[tag=captureAS,tag=reshiramAS,tag=PokeAS] run function give:spawn/reshiram/reshiram_pokeball
execute as @e[tag=captureAS,tag=reshiramAS,tag=MasterAS] run function give:spawn/reshiram/reshiram_masterball
execute as @e[tag=captureAS,tag=reshiramAS,tag=SuperAS] run function give:spawn/reshiram/reshiram_superball
execute as @e[tag=captureAS,tag=reshiramAS,tag=HyperAS] run function give:spawn/reshiram/reshiram_hyperball

execute as @e[tag=captureAS,tag=aqualiAS,tag=HonorAS] run function give:spawn/aquali/aquali_honorball
execute as @e[tag=captureAS,tag=aqualiAS,tag=PokeAS] run function give:spawn/aquali/aquali_pokeball
execute as @e[tag=captureAS,tag=aqualiAS,tag=MasterAS] run function give:spawn/aquali/aquali_masterball
execute as @e[tag=captureAS,tag=aqualiAS,tag=SuperAS] run function give:spawn/aquali/aquali_superball
execute as @e[tag=captureAS,tag=aqualiAS,tag=HyperAS] run function give:spawn/aquali/aquali_hyperball

execute as @e[tag=captureAS,tag=pyroliAS,tag=HonorAS] run function give:spawn/pyroli/pyroli_honorball
execute as @e[tag=captureAS,tag=pyroliAS,tag=PokeAS] run function give:spawn/pyroli/pyroli_pokeball
execute as @e[tag=captureAS,tag=pyroliAS,tag=MasterAS] run function give:spawn/pyroli/pyroli_masterball
execute as @e[tag=captureAS,tag=pyroliAS,tag=SuperAS] run function give:spawn/pyroli/pyroli_superball
execute as @e[tag=captureAS,tag=pyroliAS,tag=HyperAS] run function give:spawn/pyroli/pyroli_hyperball

execute as @e[tag=captureAS,tag=nymphaliAS,tag=HonorAS] run function give:spawn/nymphali/nymphali_honorball
execute as @e[tag=captureAS,tag=nymphaliAS,tag=PokeAS] run function give:spawn/nymphali/nymphali_pokeball
execute as @e[tag=captureAS,tag=nymphaliAS,tag=MasterAS] run function give:spawn/nymphali/nymphali_masterball
execute as @e[tag=captureAS,tag=nymphaliAS,tag=SuperAS] run function give:spawn/nymphali/nymphali_superball
execute as @e[tag=captureAS,tag=nymphaliAS,tag=HyperAS] run function give:spawn/nymphali/nymphali_hyperball

execute as @e[tag=captureAS,tag=minidracoAS,tag=HonorAS] run function give:spawn/minidraco/minidraco_honorball
execute as @e[tag=captureAS,tag=minidracoAS,tag=PokeAS] run function give:spawn/minidraco/minidraco_pokeball
execute as @e[tag=captureAS,tag=minidracoAS,tag=MasterAS] run function give:spawn/minidraco/minidraco_masterball
execute as @e[tag=captureAS,tag=minidracoAS,tag=SuperAS] run function give:spawn/minidraco/minidraco_superball
execute as @e[tag=captureAS,tag=minidracoAS,tag=HyperAS] run function give:spawn/minidraco/minidraco_hyperball

execute as @e[tag=captureAS,tag=dracoAS,tag=HonorAS] run function give:spawn/draco/draco_honorball
execute as @e[tag=captureAS,tag=dracoAS,tag=PokeAS] run function give:spawn/draco/draco_pokeball
execute as @e[tag=captureAS,tag=dracoAS,tag=MasterAS] run function give:spawn/draco/draco_masterball
execute as @e[tag=captureAS,tag=dracoAS,tag=SuperAS] run function give:spawn/draco/draco_superball
execute as @e[tag=captureAS,tag=dracoAS,tag=HyperAS] run function give:spawn/draco/draco_hyperball

execute as @e[tag=captureAS,tag=dracolosseAS,tag=HonorAS] run function give:spawn/dracolosse/dracolosse_honorball
execute as @e[tag=captureAS,tag=dracolosseAS,tag=PokeAS] run function give:spawn/dracolosse/dracolosse_pokeball
execute as @e[tag=captureAS,tag=dracolosseAS,tag=MasterAS] run function give:spawn/dracolosse/dracolosse_masterball
execute as @e[tag=captureAS,tag=dracolosseAS,tag=SuperAS] run function give:spawn/dracolosse/dracolosse_superball
execute as @e[tag=captureAS,tag=dracolosseAS,tag=HyperAS] run function give:spawn/dracolosse/dracolosse_hyperball

execute as @e[tag=captureAS,tag=dracaufeuAS,tag=HonorAS] run function give:spawn/dracaufeu/dracaufeu_honorball
execute as @e[tag=captureAS,tag=dracaufeuAS,tag=PokeAS] run function give:spawn/dracaufeu/dracaufeu_pokeball
execute as @e[tag=captureAS,tag=dracaufeuAS,tag=MasterAS] run function give:spawn/dracaufeu/dracaufeu_masterball
execute as @e[tag=captureAS,tag=dracaufeuAS,tag=SuperAS] run function give:spawn/dracaufeu/dracaufeu_superball
execute as @e[tag=captureAS,tag=dracaufeuAS,tag=HyperAS] run function give:spawn/dracaufeu/dracaufeu_hyperball

execute as @e[tag=captureAS,tag=pachirisuAS,tag=HonorAS] run function give:spawn/pachirisu/pachirisu_honorball
execute as @e[tag=captureAS,tag=pachirisuAS,tag=PokeAS] run function give:spawn/pachirisu/pachirisu_pokeball
execute as @e[tag=captureAS,tag=pachirisuAS,tag=MasterAS] run function give:spawn/pachirisu/pachirisu_masterball
execute as @e[tag=captureAS,tag=pachirisuAS,tag=SuperAS] run function give:spawn/pachirisu/pachirisu_superball
execute as @e[tag=captureAS,tag=pachirisuAS,tag=HyperAS] run function give:spawn/pachirisu/pachirisu_hyperball

execute as @e[tag=captureAS,tag=mimiquiAS,tag=HonorAS] run function give:spawn/mimiqui/mimiqui_honorball
execute as @e[tag=captureAS,tag=mimiquiAS,tag=PokeAS] run function give:spawn/mimiqui/mimiqui_pokeball
execute as @e[tag=captureAS,tag=mimiquiAS,tag=MasterAS] run function give:spawn/mimiqui/mimiqui_masterball
execute as @e[tag=captureAS,tag=mimiquiAS,tag=SuperAS] run function give:spawn/mimiqui/mimiqui_superball
execute as @e[tag=captureAS,tag=mimiquiAS,tag=HyperAS] run function give:spawn/mimiqui/mimiqui_hyperball

execute as @e[tag=captureAS,tag=skelenoxAS,tag=HonorAS] run function give:spawn/skelenox/skelenox_honorball
execute as @e[tag=captureAS,tag=skelenoxAS,tag=PokeAS] run function give:spawn/skelenox/skelenox_pokeball
execute as @e[tag=captureAS,tag=skelenoxAS,tag=MasterAS] run function give:spawn/skelenox/skelenox_masterball
execute as @e[tag=captureAS,tag=skelenoxAS,tag=SuperAS] run function give:spawn/skelenox/skelenox_superball
execute as @e[tag=captureAS,tag=skelenoxAS,tag=HyperAS] run function give:spawn/skelenox/skelenox_hyperball

execute as @e[tag=captureAS,tag=magicarpeAS,tag=HonorAS] run function give:spawn/magicarpe/magicarpe_honorball
execute as @e[tag=captureAS,tag=magicarpeAS,tag=PokeAS] run function give:spawn/magicarpe/magicarpe_pokeball
execute as @e[tag=captureAS,tag=magicarpeAS,tag=MasterAS] run function give:spawn/magicarpe/magicarpe_masterball
execute as @e[tag=captureAS,tag=magicarpeAS,tag=SuperAS] run function give:spawn/magicarpe/magicarpe_superball
execute as @e[tag=captureAS,tag=magicarpeAS,tag=HyperAS] run function give:spawn/magicarpe/magicarpe_hyperball

execute as @e[tag=captureAS,tag=noctunoirAS,tag=HonorAS] run function give:spawn/noctunoir/noctunoir_honorball
execute as @e[tag=captureAS,tag=noctunoirAS,tag=PokeAS] run function give:spawn/noctunoir/noctunoir_pokeball
execute as @e[tag=captureAS,tag=noctunoirAS,tag=MasterAS] run function give:spawn/noctunoir/noctunoir_masterball
execute as @e[tag=captureAS,tag=noctunoirAS,tag=SuperAS] run function give:spawn/noctunoir/noctunoir_superball
execute as @e[tag=captureAS,tag=noctunoirAS,tag=HyperAS] run function give:spawn/noctunoir/noctunoir_hyperball

execute as @e[tag=captureAS,tag=teraclopeAS,tag=HonorAS] run function give:spawn/teraclope/teraclope_honorball
execute as @e[tag=captureAS,tag=teraclopeAS,tag=PokeAS] run function give:spawn/teraclope/teraclope_pokeball
execute as @e[tag=captureAS,tag=teraclopeAS,tag=MasterAS] run function give:spawn/teraclope/teraclope_masterball
execute as @e[tag=captureAS,tag=teraclopeAS,tag=SuperAS] run function give:spawn/teraclope/teraclope_superball
execute as @e[tag=captureAS,tag=teraclopeAS,tag=HyperAS] run function give:spawn/teraclope/teraclope_hyperball

execute as @e[tag=captureAS,tag=noctaliAS,tag=HonorAS] run function give:spawn/noctali/noctali_honorball
execute as @e[tag=captureAS,tag=noctaliAS,tag=PokeAS] run function give:spawn/noctali/noctali_pokeball
execute as @e[tag=captureAS,tag=noctaliAS,tag=MasterAS] run function give:spawn/noctali/noctali_masterball
execute as @e[tag=captureAS,tag=noctaliAS,tag=SuperAS] run function give:spawn/noctali/noctali_superball
execute as @e[tag=captureAS,tag=noctaliAS,tag=HyperAS] run function give:spawn/noctali/noctali_hyperball

execute as @e[tag=captureAS,tag=laggronAS,tag=HonorAS] run function give:spawn/laggron/laggron_honorball
execute as @e[tag=captureAS,tag=laggronAS,tag=PokeAS] run function give:spawn/laggron/laggron_pokeball
execute as @e[tag=captureAS,tag=laggronAS,tag=MasterAS] run function give:spawn/laggron/laggron_masterball
execute as @e[tag=captureAS,tag=laggronAS,tag=SuperAS] run function give:spawn/laggron/laggron_superball
execute as @e[tag=captureAS,tag=laggronAS,tag=HyperAS] run function give:spawn/laggron/laggron_hyperball

execute as @e[tag=captureAS,tag=reptincelAS,tag=HonorAS] run function give:spawn/reptincel/reptincel_honorball
execute as @e[tag=captureAS,tag=reptincelAS,tag=PokeAS] run function give:spawn/reptincel/reptincel_pokeball
execute as @e[tag=captureAS,tag=reptincelAS,tag=MasterAS] run function give:spawn/reptincel/reptincel_masterball
execute as @e[tag=captureAS,tag=reptincelAS,tag=SuperAS] run function give:spawn/reptincel/reptincel_superball
execute as @e[tag=captureAS,tag=reptincelAS,tag=HyperAS] run function give:spawn/reptincel/reptincel_hyperball

execute as @e[tag=captureAS,tag=salamecheAS,tag=HonorAS] run function give:spawn/salameche/salameche_honorball
execute as @e[tag=captureAS,tag=salamecheAS,tag=PokeAS] run function give:spawn/salameche/salameche_pokeball
execute as @e[tag=captureAS,tag=salamecheAS,tag=MasterAS] run function give:spawn/salameche/salameche_masterball
execute as @e[tag=captureAS,tag=salamecheAS,tag=SuperAS] run function give:spawn/salameche/salameche_superball
execute as @e[tag=captureAS,tag=salamecheAS,tag=HyperAS] run function give:spawn/salameche/salameche_hyperball

execute as @e[tag=captureAS,tag=gobouAS,tag=HonorAS] run function give:spawn/gobou/gobou_honorball
execute as @e[tag=captureAS,tag=gobouAS,tag=PokeAS] run function give:spawn/gobou/gobou_pokeball
execute as @e[tag=captureAS,tag=gobouAS,tag=MasterAS] run function give:spawn/gobou/gobou_masterball
execute as @e[tag=captureAS,tag=gobouAS,tag=SuperAS] run function give:spawn/gobou/gobou_superball
execute as @e[tag=captureAS,tag=gobouAS,tag=HyperAS] run function give:spawn/gobou/gobou_hyperball

execute as @e[tag=captureAS,tag=flobioAS,tag=HonorAS] run function give:spawn/flobio/flobio_honorball
execute as @e[tag=captureAS,tag=flobioAS,tag=PokeAS] run function give:spawn/flobio/flobio_pokeball
execute as @e[tag=captureAS,tag=flobioAS,tag=MasterAS] run function give:spawn/flobio/flobio_masterball
execute as @e[tag=captureAS,tag=flobioAS,tag=SuperAS] run function give:spawn/flobio/flobio_superball
execute as @e[tag=captureAS,tag=flobioAS,tag=HyperAS] run function give:spawn/flobio/flobio_hyperball

execute as @e[tag=captureAS,tag=poussifeuAS,tag=HonorAS] run function give:spawn/poussifeu/poussifeu_honorball
execute as @e[tag=captureAS,tag=poussifeuAS,tag=PokeAS] run function give:spawn/poussifeu/poussifeu_pokeball
execute as @e[tag=captureAS,tag=poussifeuAS,tag=MasterAS] run function give:spawn/poussifeu/poussifeu_masterball
execute as @e[tag=captureAS,tag=poussifeuAS,tag=SuperAS] run function give:spawn/poussifeu/poussifeu_superball
execute as @e[tag=captureAS,tag=poussifeuAS,tag=HyperAS] run function give:spawn/poussifeu/poussifeu_hyperball

execute as @e[tag=captureAS,tag=galifeuAS,tag=HonorAS] run function give:spawn/galifeu/galifeu_honorball
execute as @e[tag=captureAS,tag=galifeuAS,tag=PokeAS] run function give:spawn/galifeu/galifeu_pokeball
execute as @e[tag=captureAS,tag=galifeuAS,tag=MasterAS] run function give:spawn/galifeu/galifeu_masterball
execute as @e[tag=captureAS,tag=galifeuAS,tag=SuperAS] run function give:spawn/galifeu/galifeu_superball
execute as @e[tag=captureAS,tag=galifeuAS,tag=HyperAS] run function give:spawn/galifeu/galifeu_hyperball

execute as @e[tag=captureAS,tag=brasegaliAS,tag=HonorAS] run function give:spawn/brasegali/brasegali_honorball
execute as @e[tag=captureAS,tag=brasegaliAS,tag=PokeAS] run function give:spawn/brasegali/brasegali_pokeball
execute as @e[tag=captureAS,tag=brasegaliAS,tag=MasterAS] run function give:spawn/brasegali/brasegali_masterball
execute as @e[tag=captureAS,tag=brasegaliAS,tag=SuperAS] run function give:spawn/brasegali/brasegali_superball
execute as @e[tag=captureAS,tag=brasegaliAS,tag=HyperAS] run function give:spawn/brasegali/brasegali_hyperball

execute as @e[tag=captureAS,tag=necrozmaAS,tag=HonorAS] run function give:spawn/necrozma/necrozma_honorball
execute as @e[tag=captureAS,tag=necrozmaAS,tag=PokeAS] run function give:spawn/necrozma/necrozma_pokeball
execute as @e[tag=captureAS,tag=necrozmaAS,tag=MasterAS] run function give:spawn/necrozma/necrozma_masterball
execute as @e[tag=captureAS,tag=necrozmaAS,tag=SuperAS] run function give:spawn/necrozma/necrozma_superball
execute as @e[tag=captureAS,tag=necrozmaAS,tag=HyperAS] run function give:spawn/necrozma/necrozma_hyperball

execute as @e[tag=captureAS,tag=zekromAS,tag=HonorAS] run function give:spawn/zekrom/zekrom_honorball
execute as @e[tag=captureAS,tag=zekromAS,tag=PokeAS] run function give:spawn/zekrom/zekrom_pokeball
execute as @e[tag=captureAS,tag=zekromAS,tag=MasterAS] run function give:spawn/zekrom/zekrom_masterball
execute as @e[tag=captureAS,tag=zekromAS,tag=SuperAS] run function give:spawn/zekrom/zekrom_superball
execute as @e[tag=captureAS,tag=zekromAS,tag=HyperAS] run function give:spawn/zekrom/zekrom_hyperball

execute as @e[tag=captureAS,tag=coatoxAS,tag=HonorAS] run function give:spawn/coatox/coatox_honorball
execute as @e[tag=captureAS,tag=coatoxAS,tag=PokeAS] run function give:spawn/coatox/coatox_pokeball
execute as @e[tag=captureAS,tag=coatoxAS,tag=MasterAS] run function give:spawn/coatox/coatox_masterball
execute as @e[tag=captureAS,tag=coatoxAS,tag=SuperAS] run function give:spawn/coatox/coatox_superball
execute as @e[tag=captureAS,tag=coatoxAS,tag=HyperAS] run function give:spawn/coatox/coatox_hyperball

execute as @e[tag=captureAS,tag=lunalaAS,tag=HonorAS] run function give:spawn/lunala/lunala_honorball
execute as @e[tag=captureAS,tag=lunalaAS,tag=PokeAS] run function give:spawn/lunala/lunala_pokeball
execute as @e[tag=captureAS,tag=lunalaAS,tag=MasterAS] run function give:spawn/lunala/lunala_masterball
execute as @e[tag=captureAS,tag=lunalaAS,tag=SuperAS] run function give:spawn/lunala/lunala_superball
execute as @e[tag=captureAS,tag=lunalaAS,tag=HyperAS] run function give:spawn/lunala/lunala_hyperball

execute as @e[tag=captureAS,tag=necrozma_ailesAS,tag=HonorAS] run function give:spawn/necrozma_ailes/necrozma_ailes_honorball
execute as @e[tag=captureAS,tag=necrozma_ailesAS,tag=PokeAS] run function give:spawn/necrozma_ailes/necrozma_ailes_pokeball
execute as @e[tag=captureAS,tag=necrozma_ailesAS,tag=MasterAS] run function give:spawn/necrozma_ailes/necrozma_ailes_masterball
execute as @e[tag=captureAS,tag=necrozma_ailesAS,tag=SuperAS] run function give:spawn/necrozma_ailes/necrozma_ailes_superball
execute as @e[tag=captureAS,tag=necrozma_ailesAS,tag=HyperAS] run function give:spawn/necrozma_ailes/necrozma_ailes_hyperball

execute as @e[tag=captureAS,tag=kyuremAS,tag=HonorAS] run function give:spawn/kyurem/kyurem_honorball
execute as @e[tag=captureAS,tag=kyuremAS,tag=PokeAS] run function give:spawn/kyurem/kyurem_pokeball
execute as @e[tag=captureAS,tag=kyuremAS,tag=MasterAS] run function give:spawn/kyurem/kyurem_masterball
execute as @e[tag=captureAS,tag=kyuremAS,tag=SuperAS] run function give:spawn/kyurem/kyurem_superball
execute as @e[tag=captureAS,tag=kyuremAS,tag=HyperAS] run function give:spawn/kyurem/kyurem_hyperball

execute as @e[tag=captureAS,tag=kyurem_noirAS,tag=HonorAS] run function give:spawn/kyurem_noir/kyurem_noir_honorball
execute as @e[tag=captureAS,tag=kyurem_noirAS,tag=PokeAS] run function give:spawn/kyurem_noir/kyurem_noir_pokeball
execute as @e[tag=captureAS,tag=kyurem_noirAS,tag=MasterAS] run function give:spawn/kyurem_noir/kyurem_noir_masterball
execute as @e[tag=captureAS,tag=kyurem_noirAS,tag=SuperAS] run function give:spawn/kyurem_noir/kyurem_noir_superball
execute as @e[tag=captureAS,tag=kyurem_noirAS,tag=HyperAS] run function give:spawn/kyurem_noir/kyurem_noir_hyperball

execute as @e[tag=captureAS,tag=genesectAS,tag=HonorAS] run function give:spawn/genesect/genesect_honorball
execute as @e[tag=captureAS,tag=genesectAS,tag=PokeAS] run function give:spawn/genesect/genesect_pokeball
execute as @e[tag=captureAS,tag=genesectAS,tag=MasterAS] run function give:spawn/genesect/genesect_masterball
execute as @e[tag=captureAS,tag=genesectAS,tag=SuperAS] run function give:spawn/genesect/genesect_superball
execute as @e[tag=captureAS,tag=genesectAS,tag=HyperAS] run function give:spawn/genesect/genesect_hyperball


execute if entity @e[tag=captureAS,tag=PokeAS] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}} 1
execute if entity @e[tag=captureAS,tag=MasterAS] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}} 1
execute if entity @e[tag=captureAS,tag=HonorAS] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}} 1
execute if entity @e[tag=captureAS,tag=SuperAS] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}} 1
execute if entity @e[tag=captureAS,tag=HyperAS] run clear @a[scores={tour=2}] minecraft:player_head{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}} 1