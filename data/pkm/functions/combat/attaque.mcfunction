function pkm:combat/attaque_pkm/liste_pkm_attaque
execute as @a[scores={attaque=6},tag=combat,tag=!action,tag=!mega,nbt={Inventory:[{id:"minecraft:player_head",tag:{display:{Name:"{\"text\":\"Mega Gemme\",\"color\":\"light_purple\"}"}}}]}] if entity @e[tag=capture,tag=combat,tag=peutMega] run tellraw @s {"text":" Mega evolution","color":"light_purple","clickEvent":{"action":"run_command","value":"/tag @a[tag=action,limit=1,tag=!mega] add addMega"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action,nbt={Inventory:[{id:"minecraft:player_head",tag:{display:{Name:"{\"text\":\"Orbe Dragon\",\"color\":\"green\"}"}}}]}] if entity @e[tag=capture,tag=combat,tag=necrozma_ailes] run tellraw @s {"text":" Ultra-Exmplosion","color":"black","clickEvent":{"action":"run_command","value":"/tag @a[tag=action,limit=1] add addUltra"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action,nbt={Inventory:[{id:"minecraft:player_head",tag:{display:{Name:"{\"text\":\"Orbe Dragon\",\"color\":\"green\"}"}}}]}] if entity @e[tag=capture,tag=combat,tag=necrozma_criniere] run tellraw @s {"text":" Ultra-Exmplosion","color":"black","clickEvent":{"action":"run_command","value":"/tag @a[tag=action,limit=1] add addUltra"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=capture,tag=combat] run tellraw @s {"text":" Annule","color":"yellow","clickEvent":{"action":"run_command","value":"/tag @a[tag=action,limit=1] add annulAttaque"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=capture,tag=combat] run tellraw @s {"text":" "}
tag @a[tag=annulAttaque] remove action
tag @a[tag=annulAttaque] remove addMega
scoreboard players set @a[tag=annulAttaque] tour 1
scoreboard players set @a[tag=annulAttaque] attaque 0
tag @a[tag=annulAttaque] remove annulAttaque

execute as @a[scores={attaque=6},tag=combat,tag=!action] run scoreboard players set @s attaque 5
execute as @a[scores={attaque=5},tag=combat,tag=!action] run tag @s add action
execute as @e[tag=combat,tag=pkm] store result score @s PV run data get entity @s Health



execute as @a[scores={attaque=1..4},tag=combat,tag=action,tag=addUltra] run tag @s add ultra
execute as @a[tag=ultra,tag=addUltra] run function pkm:combat/formes_combat/liste_necrozma

tag @a[tag=ultra,tag=addUltra] remove addUltra


execute as @a[scores={attaque=1..4},tag=combat,tag=action,tag=addMega] run tag @s add mega
execute as @a[tag=mega,tag=addMega] run function pkm:combat/formes_combat/liste_mega 

tag @a[tag=mega,tag=addMega] remove addMega


execute if entity @e[tag=combat,tag=pkm] run scoreboard players set max RND_Attack 4

execute as @e[tag=!capture,tag=combat,tag=pkm] if entity @p[scores={attaque=1..4},tag=action] run function pkm:fonctions/random/rnd_attaque


#check la vitesse des 2
execute if entity @p[scores={attaque=1..4},tag=action] store result score @e[tag=!capture,tag=combat,tag=pkm] vitesse run data get entity @e[tag=!capture,tag=combat,tag=pkm,limit=1] ArmorItems[3].tag.vitesse
execute if entity @p[scores={attaque=1..4},tag=action] store result score @e[tag=capture,tag=combat,tag=pkm] vitesse run data get entity @e[tag=capture,tag=combat,tag=pkm,limit=1] ArmorItems[3].tag.vitesse

execute if entity @p[scores={attaque=1..4},tag=action] run scoreboard players operation @e[tag=capture,tag=combat,tag=pkm] vitesse -= @e[tag=!capture,tag=combat,tag=pkm] vitesse



execute if entity @p[scores={attaque=1..4},tag=action] run scoreboard players set @e[tag=!paralysie] RND_Paralysie 0
execute if entity @p[scores={attaque=1..4},tag=action] as @e[tag=paralysie] run function pkm:combat/statut/paralysie



execute if entity @p[tag=distorsion] if entity @e[tag=!capture,tag=combat,tag=pkm,scores={RND_Paralysie=0}] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..}] run execute as @e[tag=!capture,tag=combat,tag=pkm] if entity @p[scores={attaque=1..4},tag=action] run tag @s add attSauvage
execute if entity @p[tag=distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..}] run execute if entity @e[tag=attSauvage] run function pkm:combat/attaque_pkm/liste_pkm_attaque
execute if entity @p[tag=distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..}] run tag @e[tag=attSauvage] remove attSauvage
execute if entity @p[tag=distorsion] run tag @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..}] add attSauvage1
execute if entity @p[tag=distorsion] run scoreboard players set @e[tag=attSauvage1] vitesse -1

execute if entity @p[tag=distorsion] if entity @e[tag=capture,tag=combat,tag=pkm,scores={RND_Paralysie=0}] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1}] run tag @e[tag=capture,tag=combat,tag=!attCapture] add attCapture
execute if entity @p[tag=distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1}] run execute if entity @e[tag=attCapture] run function pkm:combat/attaque_pkm/liste_pkm_attaque
execute if entity @p[tag=distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1}] run tag @e[tag=capture,tag=combat,tag=attCapture] remove attCapture
execute if entity @p[tag=distorsion] run scoreboard players set @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1}] vitesse 1


execute if entity @p[tag=distorsion] if entity @e[tag=!capture,tag=combat,tag=pkm,scores={RND_Paralysie=0}] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..},tag=!attSauvage1] run execute as @e[tag=!capture,tag=combat,tag=pkm] if entity @p[scores={attaque=1..4},tag=action] run tag @s add attSauvage
execute if entity @p[tag=distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..},tag=!attSauvage1] run execute if entity @e[tag=attSauvage] run function pkm:combat/attaque_pkm/liste_pkm_attaque
execute if entity @p[tag=distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..},tag=!attSauvage1] run tag @e[tag=attSauvage] remove attSauvage


execute if entity @p[tag=distorsion] run tag @e[tag=attSauvage1] remove attSauvage1








execute if entity @p[tag=!distorsion] if entity @e[tag=!capture,tag=combat,tag=pkm,scores={RND_Paralysie=0}] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1}] run execute as @e[tag=!capture,tag=combat,tag=pkm] if entity @p[scores={attaque=1..4},tag=action] run tag @s add attSauvage
execute if entity @p[tag=!distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1}] run execute if entity @e[tag=attSauvage] run function pkm:combat/attaque_pkm/liste_pkm_attaque
execute if entity @p[tag=!distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1}] run tag @e[tag=attSauvage] remove attSauvage
execute if entity @p[tag=!distorsion] run tag @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1}] add attSauvage1
execute if entity @p[tag=!distorsion] run scoreboard players set @e[tag=attSauvage1] vitesse 1

execute if entity @p[tag=!distorsion] if entity @e[tag=capture,tag=combat,tag=!attCapture,scores={RND_Paralysie=0}] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..}] run tag @e[tag=capture,tag=combat,tag=!attCapture] add attCapture
execute if entity @p[tag=!distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..}] run execute if entity @e[tag=attCapture] run function pkm:combat/attaque_pkm/liste_pkm_attaque
execute if entity @p[tag=!distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..}] run tag @e[tag=capture,tag=combat,tag=attCapture] remove attCapture
execute if entity @p[tag=!distorsion] run scoreboard players set @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=0..}] vitesse -1


execute if entity @p[tag=!distorsion] if entity @e[tag=!capture,tag=combat,tag=pkm,scores={RND_Paralysie=0}] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1},tag=!attSauvage1] run execute as @e[tag=!capture,tag=combat,tag=pkm] if entity @p[scores={attaque=1..4},tag=action] run tag @s add attSauvage
execute if entity @p[tag=!distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1},tag=!attSauvage1] run execute if entity @e[tag=attSauvage] run function pkm:combat/attaque_pkm/liste_pkm_attaque
execute if entity @p[tag=!distorsion] run execute if entity @e[tag=capture,tag=combat,tag=pkm,scores={vitesse=..-1},tag=!attSauvage1] run tag @e[tag=attSauvage] remove attSauvage


execute if entity @p[tag=!distorsion] run tag @e[tag=attSauvage1] remove attSauvage1






execute as @p[scores={attaque=1..4},tag=combat,tag=action] run tag @s add enlever
execute as @p[tag=enlever] run function pkm:combat/fin_tour
execute as @p[tag=enlever] run tag @s remove enlever




