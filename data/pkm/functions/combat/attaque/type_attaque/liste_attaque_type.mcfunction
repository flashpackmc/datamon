execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueNormal] run function pkm:combat/attaque/type_attaque/normal_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueNormal] run function pkm:combat/attaque/type_attaque/normal
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueElectrique] run function pkm:combat/attaque/type_attaque/electrique_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueElectrique] run function pkm:combat/attaque/type_attaque/electrique
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueEau] run function pkm:combat/attaque/type_attaque/eau_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueEau] run function pkm:combat/attaque/type_attaque/eau
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueFeu] run function pkm:combat/attaque/type_attaque/feu_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueFeu] run function pkm:combat/attaque/type_attaque/feu
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueVol] run function pkm:combat/attaque/type_attaque/vol_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueVol] run function pkm:combat/attaque/type_attaque/vol
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueGlace] run function pkm:combat/attaque/type_attaque/glace_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueGlace] run function pkm:combat/attaque/type_attaque/glace
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaquePlante] run function pkm:combat/attaque/type_attaque/plante_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaquePlante] run function pkm:combat/attaque/type_attaque/plante
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueSpectre] run function pkm:combat/attaque/type_attaque/spectre_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueSpectre] run function pkm:combat/attaque/type_attaque/spectre
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueSol] run function pkm:combat/attaque/type_attaque/sol_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueSol] run function pkm:combat/attaque/type_attaque/sol
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueInsecte] run function pkm:combat/attaque/type_attaque/insecte_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueInsecte] run function pkm:combat/attaque/type_attaque/insecte
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueDragon] run function pkm:combat/attaque/type_attaque/dragon_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueDragon] run function pkm:combat/attaque/type_attaque/dragon
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaquePsy] run function pkm:combat/attaque/type_attaque/psy_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaquePsy] run function pkm:combat/attaque/type_attaque/psy
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueFee] run function pkm:combat/attaque/type_attaque/fee_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueFee] run function pkm:combat/attaque/type_attaque/fee
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueCombat] run function pkm:combat/attaque/type_attaque/combat_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueCombat] run function pkm:combat/attaque/type_attaque/combat
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaquePoison] run function pkm:combat/attaque/type_attaque/poison_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaquePoison] run function pkm:combat/attaque/type_attaque/poison
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueTenebre] run function pkm:combat/attaque/type_attaque/tenebre_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueTenebre] run function pkm:combat/attaque/type_attaque/tenebre
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueRoche] run function pkm:combat/attaque/type_attaque/roche_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueRoche] run function pkm:combat/attaque/type_attaque/roche
execute if entity @s[tag=!capture] if entity @e[tag=capture,tag=combat,tag=pkm,tag=attaqueAcier] run function pkm:combat/attaque/type_attaque/acier_capture
execute if entity @s[tag=capture] if entity @e[tag=!capture,tag=combat,tag=pkm,tag=attaqueAcier] run function pkm:combat/attaque/type_attaque/acier

tag @e[tag=attaqueAcier] remove attaqueAcier
tag @e[tag=attaqueRoche] remove attaqueRoche
tag @e[tag=attaqueTenebre] remove attaqueTenebre
tag @e[tag=attaquePoison] remove attaquePoison
tag @e[tag=attaqueCombat] remove attaqueCombat
tag @e[tag=attaqueFee] remove attaqueFee
tag @e[tag=attaquePsy] remove attaquePsy
tag @e[tag=attaqueDragon] remove attaqueDragon
tag @e[tag=attaqueInsecte] remove attaqueInsecte
tag @e[tag=attaqueSol] remove attaqueSol
tag @e[tag=attaqueSpectre] remove attaqueSpectre
tag @e[tag=attaquePlante] remove attaquePlante
tag @e[tag=attaqueGlace] remove attaqueGlace
tag @e[tag=attaqueVol] remove attaqueVol
tag @e[tag=attaqueFeu] remove attaqueFeu
tag @e[tag=attaqueEau] remove attaqueEau
tag @e[tag=attaqueElectrique] remove attaqueElectrique
tag @e[tag=attaqueNormal] remove attaqueNormal
