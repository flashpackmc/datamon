execute as @e[tag=!capture,tag=combat,tag=pkm] run function pkm:fonctions/random/rnd_miss_attaque

execute as @e[tag=!capture,tag=combat,tag=pkm] run function pkm:fonctions/random/rnd_crit



scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur1 1
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 1
#le multiplieur 1 et 1 servent juste pour les multiplication a virgule
#multipieur1 est pour stocker les degats
#multiplieur2 est pour stocker le nombre par le quel il faut multiplier


#####################################stab###################################################
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur1 1
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 1


scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat,tag=Tsol] multiplieur1 = @e[tag=!capture,tag=pkm,tag=combat,tag=Tsol] degat
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat,tag=Tsol] multiplieur2 2
scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat,tag=Tsol] multiplieur1 /= @e[tag=!capture,tag=pkm,tag=combat,tag=Tsol] multiplieur2
scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat,tag=Tsol] degat += @e[tag=!capture,tag=pkm,tag=combat,tag=Tsol] multiplieur1
#multiplier par 1.5 (comme il n'y a pas de score a virgule, il faut ajoute au score degat sa motie a lui meme


#####################################faiblesse###################################################
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur1 1
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 1

execute if entity @s[tag=Tacier] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 2
execute if entity @s[tag=Tacier] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] degat *= @e[tag=!capture,tag=pkm,tag=combat] multiplieur2

execute if entity @s[tag=Tfeu] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 2
execute if entity @s[tag=Tfeu] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] degat *= @e[tag=!capture,tag=pkm,tag=combat] multiplieur2

execute if entity @s[tag=Telectrique] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 2
execute if entity @s[tag=Telectrique] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] degat *= @e[tag=!capture,tag=pkm,tag=combat] multiplieur2

execute if entity @s[tag=Tpoison] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 2
execute if entity @s[tag=Tpoison] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] degat *= @e[tag=!capture,tag=pkm,tag=combat] multiplieur2

execute if entity @s[tag=Troche] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 2
execute if entity @s[tag=Troche] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] degat *= @e[tag=!capture,tag=pkm,tag=combat] multiplieur2

#####################################resistance###################################################
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur1 1
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 1

execute if entity @s[tag=Teau] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 2
execute if entity @s[tag=Teau] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] degat /= @e[tag=!capture,tag=pkm,tag=combat] multiplieur2

execute if entity @s[tag=Tplante] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 2
execute if entity @s[tag=Tplante] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] degat /= @e[tag=!capture,tag=pkm,tag=combat] multiplieur2

#####################################critique###################################################
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur1 1
scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 1

execute if entity @e[tag=!capture,scores={RND_Critique=0},limit=1,tag=combat] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] multiplieur1 = @e[tag=!capture,tag=pkm,tag=combat] degat
execute if entity @e[tag=!capture,scores={RND_Critique=0},limit=1,tag=combat] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] multiplieur2 2
execute if entity @e[tag=!capture,scores={RND_Critique=0},limit=1,tag=combat] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] multiplieur1 /= @e[tag=!capture,tag=pkm,tag=combat] multiplieur2
execute if entity @e[tag=!capture,scores={RND_Critique=0},limit=1,tag=combat] run scoreboard players operation @e[tag=!capture,tag=pkm,tag=combat] degat += @e[tag=!capture,tag=pkm,tag=combat] multiplieur1

#####################################inefficace###################################################
execute if entity @s[tag=Tvol] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] degat 0


#####################################miss###################################################
scoreboard players operation @e[tag=capture,tag=pkm,tag=combat] miss_attaque -= @e[tag=capture,tag=pkm,tag=combat] RND_Miss_Attack
scoreboard players set @e[tag=capture,tag=pkm,tag=combat,scores={miss_attaque=..-1}] degat 0
#si miss_attaque est inferiieur ou egal a -1 cela veut dire que le nb alea est plus grand, donc doit miss



execute if entity @s[tag=Tvol] if entity @e[tag=!capture,scores={miss_attaque=0..},limit=1,tag=combat] run tellraw @p[tag=combat] [{"text":" Ca n'affecte pas "},{"selector":"@s"},{"text":"..."}]
execute if entity @e[tag=!capture,scores={miss_attaque=0..,RND_Critique=0},limit=1,tag=combat] run tellraw @p[tag=combat] {"text":" Coup critique !","color":"red"}
execute as @e[tag=!capture,scores={miss_attaque=..-1},limit=1] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"red"}