#scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
#scoreboard players set @e[tag=pkm,tag=combat] degat 0
#laisser commenter

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@e[tag=!capture,tag=pkm,tag=combat]","color":"red"},{"text":" utilise Représailles !","color":"red"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@e[tag=capture,tag=pkm,tag=combat]","color":"green"},{"text":" utilise Représailles !","color":"green"}]


execute as @s[tag=capture,scores={degat=1..}] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] degat 100
execute as @s[tag=!capture,scores={degat=1..}] run scoreboard players set @e[tag=capture,tag=pkm,tag=combat] degat 100

execute as @s[tag=capture,scores={degat=..0}] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] degat 50
execute as @s[tag=!capture,scores={degat=..0}] run scoreboard players set @e[tag=capture,tag=pkm,tag=combat] degat 50
#si la cible a utilise une capacite qui fait des degats avant et ne rate pas, double les degats

execute as @s[tag=capture] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] miss_attaque 99
execute as @s[tag=!capture] run scoreboard players set @e[tag=capture,tag=pkm,tag=combat] miss_attaque 99


#pour augmenter coup critque, il faut modifier le max de RND_Critique

execute as @s[tag=!capture] run tag @e[tag=capture,tag=pkm,tag=combat] add attaqueTenebre
execute as @s[tag=capture] run tag @e[tag=!capture,tag=pkm,tag=combat] add attaqueTenebre

execute as @s[tag=capture] run function pkm:combat/attaque/infliger_degat_physique
execute as @s[tag=!capture] run function pkm:combat/attaque/infliger_degat_physique_capture
#_capture est si c'est le capture qui attaque
#@s est la cible
