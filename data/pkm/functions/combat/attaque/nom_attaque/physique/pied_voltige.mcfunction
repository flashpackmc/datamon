scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@e[tag=!capture,tag=pkm,tag=combat]","color":"red"},{"text":" utilise Pied Voltige !","color":"red"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@e[tag=capture,tag=pkm,tag=combat]","color":"green"},{"text":" utilise Pied Voltige !","color":"green"}]


execute as @s[tag=capture] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] miss_attaque 89
execute as @s[tag=capture] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] degat 130

execute as @s[tag=!capture] run scoreboard players set @e[tag=capture,tag=pkm,tag=combat] miss_attaque 89
execute as @s[tag=!capture] run scoreboard players set @e[tag=capture,tag=pkm,tag=combat] degat 130

#pour augmenter coup critque, il faut modifier le max de RND_Critique

execute as @s[tag=!capture] run tag @e[tag=capture,tag=pkm,tag=combat] add attaqueCombat
execute as @s[tag=capture] run tag @e[tag=!capture,tag=pkm,tag=combat] add attaqueCombat

execute as @s[tag=capture] run function pkm:combat/attaque/infliger_degat_physique
execute as @s[tag=!capture] run function pkm:combat/attaque/infliger_degat_physique_capture
#_capture est si c'est le capture qui attaque
#@s est la cible


execute if entity @s[tag=capture] run scoreboard players set @e[tag=combat,tag=!capture] multiplieur2 1
execute if entity @s[tag=capture] run scoreboard players set @e[scores={miss_attaque=..-1},tag=combat,tag=!capture] multiplieur2 2
execute if entity @s[tag=capture,tag=Tspectre] run scoreboard players set @e[tag=combat,tag=!capture] multiplieur2 2
execute if entity @s[tag=capture] run scoreboard players operation @e[tag=combat,tag=!capture] PV = @e[tag=combat,tag=!capture] pv_origine
execute if entity @s[tag=capture] run scoreboard players operation @e[tag=combat,tag=!capture] PV /= @e[tag=combat,tag=!capture] multiplieur2
execute if entity @s[tag=capture] run execute store result entity @e[tag=combat,tag=!capture,limit=1] Health float 1 run scoreboard players get @e[tag=combat,tag=!capture,limit=1] PV


execute if entity @s[tag=!capture] run scoreboard players set @e[tag=combat,tag=capture] multiplieur2 1
execute if entity @s[tag=!capture] run scoreboard players set @e[scores={miss_attaque=..-1},tag=combat,tag=capture] multiplieur2 2
execute if entity @s[tag=!capture,tag=Tspectre] run scoreboard players set @e[tag=combat,tag=capture] multiplieur2 2
execute if entity @s[tag=capture] run scoreboard players operation @e[tag=combat,tag=capture] PV = @e[tag=combat,tag=capture] pv_origine
execute if entity @s[tag=!capture] run scoreboard players operation @e[tag=combat,tag=capture] PV /= @e[tag=combat,tag=capture] multiplieur2
execute if entity @s[tag=!capture] run execute store result entity @e[tag=combat,tag=capture,limit=1] Health float 1 run scoreboard players get @e[tag=combat,tag=capture,limit=1] PV