scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@e[tag=!capture,tag=pkm,tag=combat]","color":"red"},{"text":" utilise Câlinerie !","color":"red"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@e[tag=capture,tag=pkm,tag=combat]","color":"green"},{"text":" utilise Câlinerie !","color":"green"}]


execute as @s[tag=capture] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] miss_attaque 89
execute as @s[tag=capture] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] degat 90

execute as @s[tag=!capture] run scoreboard players set @e[tag=capture,tag=pkm,tag=combat] miss_attaque 89
execute as @s[tag=!capture] run scoreboard players set @e[tag=capture,tag=pkm,tag=combat] degat 90

#pour augmenter coup critque, il faut modifier le max de RND_Critique

execute as @s[tag=!capture] run tag @e[tag=capture,tag=pkm,tag=combat] add attaqueFee
execute as @s[tag=capture] run tag @e[tag=!capture,tag=pkm,tag=combat] add attaqueFee

execute as @s[tag=capture] run function pkm:combat/attaque/infliger_degat_physique
execute as @s[tag=!capture] run function pkm:combat/attaque/infliger_degat_physique_capture

scoreboard players set @s RND_Statut 100

execute if entity @e[scores={miss_attaque=0..},tag=combat] run function pkm:fonctions/random/rnd_statut


execute if entity @s[scores={RND_Statut=..9,boost_atk=1..}] run function pkm:fonctions/message/boost/dessous/boost_atk


execute if entity @s[scores={RND_Statut=..9,boost_atk=1..},tag=!capture] run function pkm:fonctions/message/diminution/sauvage/diminution_atk
execute if entity @s[scores={RND_Statut=..9,boost_atk=1..},tag=capture] run function pkm:fonctions/message/diminution/capture/diminution_atk


execute if entity @e[scores={RND_Statut=..9,boost_atk=1..},tag=combat] run function pkm:combat/boost/diminue/boost_atk