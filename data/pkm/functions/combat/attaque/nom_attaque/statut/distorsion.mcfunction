# la cible @s est le lanceur

function pkm:fonctions/random/rnd_miss_attaque


scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@s","color":"red"},{"text":" utilise Distorsion !","color":"green"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@s","color":"green"},{"text":" utilise Distorsion !","color":"red"}]


execute if entity @p[tag=!distorsion] run execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@s","color":"red"},{"text":" fausse les dimensions !","color":"green"}]
execute if entity @p[tag=!distorsion] run execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@s","color":"green"},{"text":" fausse les dimensions !","color":"red"}]

execute if entity @p[tag=distorsion] run execute as @s[tag=capture] run tellraw @a[tag=combat] {"text":" Mais cela échoue!","color":"none"}
execute if entity @p[tag=distorsion] run execute as @s[tag=!capture] run tellraw @a[tag=combat] {"text":" Mais cela échoue!","color":"none"}

execute at @s run tag @p[tag=!distorsion] add distorsion