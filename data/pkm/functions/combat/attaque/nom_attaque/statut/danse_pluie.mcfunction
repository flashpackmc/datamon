# la cible @s est le lanceur

execute at @s run summon area_effect_cloud ~ ~-1 ~ {Tags:["random"],Age:1}
data get entity @e[type=area_effect_cloud,tag=random,limit=1] UUID[0] 
scoreboard players operation @s RND_Miss_Attack %= max RND_Miss_Attack
kill @e[type=area_effect_cloud,tag=random]


scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@s","color":"red"},{"text":" utilise Danse Draco !","color":"green"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@s","color":"green"},{"text":" utilise Danse Draco !","color":"red"}]


execute store result score @s atk run data get entity @s ArmorItems[3].tag.atk
execute store result score @s vitesse run data get entity @s ArmorItems[3].tag.vitesse

scoreboard players set @s multiplieur2 2

scoreboard players operation @s atk *= @s multiplieur2
scoreboard players operation @s vitesse *= @s multiplieur2



scoreboard players operation @s miss_attaque -= @s RND_Miss_Attack




execute store result entity @s[scores={miss_attaque=0..}] ArmorItems[3].tag.atk int 1 run scoreboard players get @s atk
execute store result entity @s[scores={miss_attaque=0..}] ArmorItems[3].tag.vitesse int 1 run scoreboard players get @s vitesse
scoreboard players reset @s multiplieur2





execute as @s[tag=!capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"red"}
execute as @s[tag=capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"green"}

execute as @s[tag=!capture,scores={miss_attaque=0..}] run tellraw @p[tag=combat] [{"text":" L'Attaque de ","color":"red"},{"selector":"@s","color":"red"},{"text":" augmente !","color":"red"}]
execute as @s[tag=!capture,scores={miss_attaque=0..}] run tellraw @p[tag=combat] [{"text":" La Vitesse de ","color":"red"},{"selector":"@s","color":"red"},{"text":" augmente !","color":"red"}]

execute as @s[tag=capture,scores={miss_attaque=0..}] run tellraw @p[tag=combat] [{"text":" L'Attaque de ","color":"green"},{"selector":"@s","color":"green"},{"text":" augmente !","color":"green"}]
execute as @s[tag=capture,scores={miss_attaque=0..}] run tellraw @p[tag=combat] [{"text":" La Vitesse de ","color":"green"},{"selector":"@s","color":"green"},{"text":" augmente !","color":"green"}]