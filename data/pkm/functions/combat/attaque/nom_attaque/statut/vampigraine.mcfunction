# la cible @s est le lanceur

execute if entity @s[tag=!capture] as @e[tag=capture,tag=combat,tag=pkm] run function pkm:fonctions/random/rnd_miss_attaque
execute if entity @s[tag=capture] as @e[tag=!capture,tag=combat,tag=pkm] run function pkm:fonctions/random/rnd_miss_attaque

scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 89
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@e[tag=!capture,tag=combat,tag=pkm]","color":"red"},{"text":" utilise Vampigraine !","color":"red"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@e[tag=capture,tag=combat,tag=pkm]","color":"green"},{"text":" utilise Vampigraine !","color":"green"}]




scoreboard players operation @s miss_attaque -= @s RND_Miss_Attack


execute if entity @s[tag=!capture,tag=vampigraine] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"green"}
execute if entity @s[tag=capture,tag=vampigraine] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"red"}
execute if entity @s[tag=!capture,tag=Tplante] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"green"}
execute if entity @s[tag=capture,tag=Tplante] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"red"}

execute if entity @s[tag=!vampigraine,tag=!Tplante] if entity @e[tag=combat,tag=pkm,scores={miss_attaque=0..}] run function pkm:combat/statut/vampigraine




execute as @s[tag=capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"red"}
execute as @s[tag=!capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"green"}
