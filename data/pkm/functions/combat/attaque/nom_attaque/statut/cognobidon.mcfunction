function pkm:fonctions/random/rnd_miss_attaque

scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@s","color":"green"},{"text":" utilise Cognobidon !","color":"green"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@s","color":"red"},{"text":" utilise Cognobidon !","color":"red"}]






scoreboard players operation @s miss_attaque -= @s RND_Miss_Attack

execute as @e at @s store result score @s PV run data get entity @s Health
scoreboard players operation @s multiplieur2 = @s PV
scoreboard players operation @s multiplieur2 -= @s pv_origine


scoreboard players operation @s[scores={multiplieur2=1..}] PV -= @s[scores={multiplieur2=1..}] multiplieur2
execute store result entity @s[scores={multiplieur2=1..}] Health float 1 run scoreboard players get @s[scores={multiplieur2=1..}] PV


#augmente au maximum
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_atk=..11,multiplieur2=1..}] run function pkm:combat/boost/augmente/boost_atk


execute as @s[tag=!capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"red"}
execute as @s[tag=capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"green"}
execute as @s[tag=!capture,scores={multiplieur2=..0}] run tellraw @p[tag=combat] {"text":" Les PV sont trop faibles!","color":"red"}
execute as @s[tag=capture,scores={multiplieur2=..0}] run tellraw @p[tag=combat] {"text":" Les PV sont trop faibles!","color":"green"}


execute as @s[tag=!capture,scores={miss_attaque=0..,boost_atk=..12,multiplieur2=1..}] run function pkm:fonctions/message/augmentation/sauvage/augmentation_atk_beaucoup
execute as @s[tag=capture,scores={miss_attaque=0..,boost_atk=..12,multiplieur2=1..}] run function pkm:fonctions/message/augmentation/capture/augmentation_atk_beaucoup