# la cible @s est le lanceur

execute at @s run summon area_effect_cloud ~ ~-1 ~ {Tags:["random"],Age:1}
data get entity @e[type=area_effect_cloud,tag=random,limit=1] UUID[0] 
scoreboard players operation @s RND_Miss_Attack %= max RND_Miss_Attack
kill @e[type=area_effect_cloud,tag=random]


scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@s","color":"red"},{"text":" utilise Bouclier !","color":"green"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@s","color":"green"},{"text":" utilise Bouclier !","color":"red"}]







scoreboard players operation @s miss_attaque -= @s RND_Miss_Attack




execute if entity @s[scores={miss_attaque=0..,boost_def=..11}] run function pkm:combat/boost/augmente/boost_def
execute if entity @s[scores={miss_attaque=0..,boost_def=..11}] run function pkm:combat/boost/augmente/boost_def


execute as @s[tag=!capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"red"}
execute as @s[tag=capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"green"}


execute as @s[scores={miss_attaque=0..,boost_def=11..}] run function pkm:fonctions/message/boost/dessus/boost_def

execute as @s[tag=!capture,scores={miss_attaque=0..,boost_def=..10}] run function pkm:fonctions/message/augmentation/sauvage/augmentation_def_beaucoup
execute as @s[tag=capture,scores={miss_attaque=0..,boost_def=..10}] run function pkm:fonctions/message/augmentation/capture/augmentation_def_beaucoup