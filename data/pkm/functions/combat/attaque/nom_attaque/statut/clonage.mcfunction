# la cible @s est le lanceur

function pkm:fonctions/random/rnd_miss_attaque

scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@s","color":"green"},{"text":" utilise Clonage !","color":"green"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@s","color":"red"},{"text":" utilise Clonage !","color":"red"}]

execute if entity @s[tag=clone] run tellraw @a[tag=combat] [{"text":" Mais cela échoue !","color":"none"}]

execute store result score @s[tag=!clone] PV run data get entity @s[tag=!clone] Health
scoreboard players set @s[tag=!clone] multiplieur2 4
scoreboard players operation @s[tag=!clone] multiplieur1 = @s[tag=!clone] pv_origine
scoreboard players operation @s[tag=!clone] multiplieur1 /= @s[tag=!clone] multiplieur2
scoreboard players operation @s[tag=!clone] PV -= @s[tag=!clone] multiplieur1
scoreboard players operation @s[tag=!clone,scores={PV=1..}] vie_clone = @s[tag=!clone] multiplieur1
execute store result entity @s[tag=!clone,scores={PV=1..}] Health float 1 run scoreboard players get @s[tag=!clone,scores={PV=1..}] PV

execute as @s[tag=!clone,scores={PV=..0}] run tellraw @a[tag=combat] [{"text":" Mais cela échoue !","color":"none"}]


tag @s[tag=!clone,scores={PV=1..}] add clone

execute store result score @s[tag=!clone,scores={PV=..0}] PV run data get entity @s[tag=!clone,scores={PV=..0}] Health