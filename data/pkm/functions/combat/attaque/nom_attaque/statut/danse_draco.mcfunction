# la cible @s est le lanceur

function pkm:fonctions/random/rnd_miss_attaque

scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@s","color":"green"},{"text":" utilise Danse Draco !","color":"green"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@s","color":"red"},{"text":" utilise Danse Draco !","color":"red"}]




scoreboard players operation @s miss_attaque -= @s RND_Miss_Attack





execute if entity @s[scores={miss_attaque=0..,boost_atk=..11}] run function pkm:combat/boost/augmente/boost_atk
execute if entity @s[scores={miss_attaque=0..,boost_vitesse=..11}] run function pkm:combat/boost/augmente/boost_vitesse



execute as @s[tag=!capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"red"}
execute as @s[tag=capture,scores={miss_attaque=..-1}] run tellraw @p[tag=combat] {"text":" Mais cela échoue!","color":"green"}

execute as @s[scores={miss_attaque=0..,boost_atk=12..}] run function pkm:fonctions/message/boost/dessus/boost_atk
execute as @s[scores={miss_attaque=0..,boost_vitesse=12..}] run function pkm:fonctions/message/boost/dessus/boost_vitesse

execute as @s[tag=!capture,scores={miss_attaque=0..,boost_atk=..11}] run function pkm:fonctions/message/augmentation/sauvage/augmentation_atk
execute as @s[tag=!capture,scores={miss_attaque=0..,boost_vitesse=..11}] run function pkm:fonctions/message/augmentation/sauvage/augmentation_vitesse

execute as @s[tag=capture,scores={miss_attaque=0..,boost_atk=..11}] run function pkm:fonctions/message/augmentation/capture/augmentation_atk
execute as @s[tag=capture,scores={miss_attaque=0..,boost_vitesse=..11}] run function pkm:fonctions/message/augmentation/capture/augmentation_vitesse