scoreboard players set @e[tag=pkm,tag=combat] miss_attaque 99
scoreboard players set @e[tag=pkm,tag=combat] degat 0

execute as @s[tag=capture] run tellraw @a[tag=combat] [{"text":" ","color":"red"},{"selector":"@e[tag=!capture,tag=pkm,tag=combat]","color":"red"},{"text":" utilise Techno Buster !","color":"red"}]
execute as @s[tag=!capture] run tellraw @a[tag=combat] [{"text":" ","color":"green"},{"selector":"@e[tag=capture,tag=pkm,tag=combat]","color":"green"},{"text":" utilise Techno Buster !","color":"green"}]


execute as @s[tag=capture] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] miss_attaque 99
execute as @s[tag=capture] run scoreboard players set @e[tag=!capture,tag=pkm,tag=combat] degat 120

execute as @s[tag=!capture] run scoreboard players set @e[tag=capture,tag=pkm,tag=combat] miss_attaque 99
execute as @s[tag=!capture] run scoreboard players set @e[tag=capture,tag=pkm,tag=combat] degat 120

#pour augmenter coup critque, il faut modifier le max de RND_Critique

execute as @s[tag=!capture,tag=genesectFeu] run tag @e[tag=capture,tag=pkm,tag=combat] add attaqueFeu
execute as @s[tag=capture,tag=genesectFeu] run tag @e[tag=!capture,tag=pkm,tag=combat] add attaqueFeu

execute as @s[tag=!capture,tag=genesectElectrique] run tag @e[tag=capture,tag=pkm,tag=combat] add attaqueElectrique
execute as @s[tag=capture,tag=genesectElectrique] run tag @e[tag=!capture,tag=pkm,tag=combat] add attaqueElectrique

execute as @s[tag=!capture,tag=genesectEau] run tag @e[tag=capture,tag=pkm,tag=combat] add attaqueEau
execute as @s[tag=capture,tag=genesectEau] run tag @e[tag=!capture,tag=pkm,tag=combat] add attaqueEau

execute as @s[tag=!capture,tag=genesectGlace] run tag @e[tag=capture,tag=pkm,tag=combat] add attaqueGlace
execute as @s[tag=capture,tag=genesectGlace] run tag @e[tag=!capture,tag=pkm,tag=combat] add attaqueGlace

execute as @s[tag=!capture,tag=genesectNormal] run tag @e[tag=capture,tag=pkm,tag=combat] add attaqueNormal
execute as @s[tag=capture,tag=genesectNormal] run tag @e[tag=!capture,tag=pkm,tag=combat] add attaqueNormal

execute as @s[tag=capture] run function pkm:combat/attaque/infliger_degat_special
execute as @s[tag=!capture] run function pkm:combat/attaque/infliger_degat_special_capture
#_capture est si c'est le capture qui attaque
#@s est la cible
