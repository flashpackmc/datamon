

execute store result score @s defSpec run data get entity @s ArmorItems[3].tag.defSpec
#recupere stat defense stocke sur la tete

execute store result score @e[tag=capture,tag=pkm,tag=combat] atkSpec run data get entity @e[tag=capture,tag=pkm,tag=combat,limit=1] ArmorItems[3].tag.atkSpec
#recupere stat defense stocke sur la tete


scoreboard players operation @e[tag=capture,tag=pkm,tag=combat] degat *= @e[tag=capture,tag=pkm,tag=combat] atkSpec




scoreboard players set @e[tag=capture,tag=pkm,tag=combat] multiplieur2 42
scoreboard players operation @e[tag=capture,tag=pkm,tag=combat] degat *= @e[tag=capture,tag=pkm,tag=combat] multiplieur2

#42 correspont au niveau 100 d'un pkm

scoreboard players operation @e[tag=capture,tag=pkm,tag=combat] degat /= @s defSpec



scoreboard players set @e[tag=capture,tag=pkm,tag=combat] multiplieur2 50
scoreboard players operation @e[tag=capture,tag=pkm,tag=combat] degat /= @e[tag=capture,tag=pkm,tag=combat] multiplieur2
#50 est dans le calcul de base

scoreboard players add @e[tag=capture,tag=pkm,tag=combat] degat 2
#dans le calcul de base du jeu pkm


function pkm:combat/attaque/type_attaque/liste_attaque_type

execute as @e[tag=capture,tag=pkm,tag=combat] run function pkm:fonctions/random/rnd_degat
scoreboard players operation @e[tag=capture,tag=pkm,tag=combat] degat *= @e[tag=capture,tag=pkm,tag=combat] RND_Degat
scoreboard players set @e[tag=capture,tag=pkm,tag=combat] multiplieur2 100
scoreboard players operation @e[tag=capture,tag=pkm,tag=combat] degat /= @e[tag=capture,tag=pkm,tag=combat] multiplieur2
#random entre 0.85 et 1 pour les degats

execute store result score @s PV run data get entity @s Health

scoreboard players operation @s[tag=!clone] PV -= @e[tag=capture,tag=pkm,tag=combat] degat
#calcule la vie apres l'attaque
execute store result entity @s[tag=!clone] Health float 1 run scoreboard players get @s[tag=!clone] PV
#applique la vie

scoreboard players operation @s[tag=clone] vie_clone -= @e[tag=capture,tag=pkm,tag=combat] degat
execute as @s[tag=clone,scores={vie_clone=..0}] run tellraw @a[tag=combat] [{"text":" Le clone de ","color":"none"},{"selector":"@s","color":"none"},{"text":" disparait !","color":"none"}]
tag @s[tag=clone,scores={vie_clone=..0}] remove clone

#tellraw @p[tag=combat] ["",{"score":{"name":"@e[tag=capture,tag=pkm,tag=combat,limit=1]","objective":"degat"},"color":"none"}]