execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=porygon_2,tag=capture,tag=combat] run tellraw @s {"text":" Ball'Ombre","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 1"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=porygon_2,tag=capture,tag=combat] run tellraw @s {"text":" Tonnerre","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 2"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=porygon_2,tag=capture,tag=combat] run tellraw @s {"text":" Laser Glace","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 3"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=porygon_2,tag=capture,tag=combat] run tellraw @s {"text":" Psyko","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 4"}}


#attaque par capture
execute if entity @p[scores={attaque=1},tag=action] run execute as @e[tag=porygon_2,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/ball_ombre
execute if entity @p[scores={attaque=2},tag=action] run execute as @e[tag=porygon_2,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/tonnerre
execute if entity @p[scores={attaque=3},tag=action] run execute as @e[tag=porygon_2,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/laser_glace
execute if entity @p[scores={attaque=4},tag=action] run execute as @e[tag=porygon_2,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/psyko



#attaque par sauvage
execute as @e[tag=porygon_2,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=0}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/ball_ombre
execute as @e[tag=porygon_2,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=1}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/tonnerre
execute as @e[tag=porygon_2,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=2}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/laser_glace
execute as @e[tag=porygon_2,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=3}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/psyko

