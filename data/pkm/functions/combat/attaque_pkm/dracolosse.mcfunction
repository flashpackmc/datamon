execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=dracolosse,tag=capture,tag=combat] run tellraw @s {"text":" Cascade","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 1"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=dracolosse,tag=capture,tag=combat] run tellraw @s {"text":" Draco Charge","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 2"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=dracolosse,tag=capture,tag=combat] run tellraw @s {"text":" Vol","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 3"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=dracolosse,tag=capture,tag=combat] run tellraw @s {"text":" Danse Draco","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 4"}}


#attaque par capture
execute if entity @p[scores={attaque=1},tag=action] run execute as @e[tag=dracolosse,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/cascade
execute if entity @p[scores={attaque=2},tag=action] run execute as @e[tag=dracolosse,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/draco_charge
execute if entity @p[scores={attaque=3},tag=action] run execute as @e[tag=dracolosse,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/vol
execute if entity @p[scores={attaque=4},tag=action] run execute as @e[tag=dracolosse,tag=capture,tag=combat,tag=attCapture] at @s if entity @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/statut/danse_draco



#attaque par sauvage
execute as @e[tag=dracolosse,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=0}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/cascade
execute as @e[tag=dracolosse,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=1}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/draco_charge
execute as @e[tag=dracolosse,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=2}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/vol
execute as @e[tag=dracolosse,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=3}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s if entity @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/statut/danse_draco

