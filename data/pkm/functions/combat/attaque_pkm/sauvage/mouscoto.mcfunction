#attaque par sauvage
execute as @e[tag=mouscoto,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=0}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/poing_glace
execute as @e[tag=mouscoto,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=1}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/marto_poing
execute as @e[tag=mouscoto,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=2}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/furie_bond
execute as @e[tag=mouscoto,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=3}] at @s if entity @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/statut/gonflette
