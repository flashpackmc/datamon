execute if entity @e[tag=combat,tag=pkm] run scoreboard players set max RND_Attack 4

execute as @e[tag=!capture,tag=combat,tag=pkm] at @s run summon area_effect_cloud ~ ~-1 ~ {Tags:["random"],Age:1}
execute as @e[tag=!capture,tag=combat,tag=pkm] run execute store result score @s RND_Attack run data get entity @e[type=area_effect_cloud,tag=random,limit=1] UUID[0] 
execute as @e[tag=!capture,tag=combat,tag=pkm] run scoreboard players operation @s RND_Attack %= max RND_Attack
execute as @e[tag=!capture,tag=combat,tag=pkm] run kill @e[type=area_effect_cloud,tag=random]


execute if entity @e[tag=leviator,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/leviator
execute if entity @e[tag=noctali,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/noctali
execute if entity @e[tag=dracaufeu,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/dracaufeu
execute if entity @e[tag=mimiqui,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/mimiqui
execute if entity @e[tag=pachirisu,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/pachirisu
execute if entity @e[tag=skelenox,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/skelenox
execute if entity @e[tag=magicarpe,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/magicarpe
execute if entity @e[tag=minidraco,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/minidraco
execute if entity @e[tag=draco,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/draco
execute if entity @e[tag=dracolosse,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/dracolosse
execute if entity @e[tag=genesect,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/genesect
execute if entity @e[tag=laggron,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/laggron
execute if entity @e[tag=reptincel,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/reptincel
execute if entity @e[tag=brasegali,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/brasegali
execute if entity @e[tag=pierroteknik,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/pierroteknik
execute if entity @e[tag=katagami,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/katagami
execute if entity @e[tag=regigigas,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/regigigas
execute if entity @e[tag=marshadow,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/marshadow
execute if entity @e[tag=demeteros,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/demeteros
execute if entity @e[tag=boreas,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/boreas
execute if entity @e[tag=fulguris,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/fulguris
execute if entity @e[tag=cancrelove,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/cancrelove
execute if entity @e[tag=bamboiselle,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/bamboiselle
execute if entity @e[tag=magearna,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/magearna
execute if entity @e[tag=mouscoto,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/mouscoto
execute if entity @e[tag=flobio,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/flobio
execute if entity @e[tag=poussifeu,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/poussifeu
execute if entity @e[tag=galifeu,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/galifeu
execute if entity @e[tag=zekrom,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/zekrom
execute if entity @e[tag=necrozma,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/necrozma
execute if entity @e[tag=kyurem,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/kyurem
execute if entity @e[tag=lunala,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/lunala
execute if entity @e[tag=kyogre,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/kyogre
execute if entity @e[tag=mentali,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/mentali
execute if entity @e[tag=evoli,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/evoli
execute if entity @e[tag=pyroli,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/pyroli
execute if entity @e[tag=crehelf,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/crehelf
execute if entity @e[tag=crefollet,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/crefollet
execute if entity @e[tag=crefadet,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/crefadet
execute if entity @e[tag=aquali,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/aquali
execute if entity @e[tag=voltali,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/voltali
execute if entity @e[tag=givrali,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/givrali
execute if entity @e[tag=reshiram,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/reshiram
execute if entity @e[tag=phyllali,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/phyllali
execute if entity @e[tag=nymphali,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/nymphali
execute if entity @e[tag=porygon,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/porygon
execute if entity @e[tag=porygon_2,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/porygon_2
execute if entity @e[tag=porygon_z,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/porygon_z
execute if entity @e[tag=teraclope,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/teraclope
execute if entity @e[tag=groudon,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/groudon
execute if entity @e[tag=noctunoir,tag=combat,tag=pkm,tag=!capture] run function pkm:combat/attaque_pkm/sauvage/noctunoir

scoreboard players set @p[tag=combat] tour 1