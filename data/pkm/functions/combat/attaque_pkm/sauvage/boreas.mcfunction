#attaque par sauvage
execute as @e[tag=boreas,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=0}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/vent_violent
execute as @e[tag=boreas,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=1}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/surpuissance
execute as @e[tag=boreas,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=2}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/canicule
execute as @e[tag=boreas,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=3}] at @s if entity @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/statut/vent_arriere
