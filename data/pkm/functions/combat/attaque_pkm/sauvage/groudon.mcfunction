#attaque par sauvage
execute as @e[tag=groudon,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=0}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/lame_pangeenne
execute as @e[tag=groudon,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=1}] at @s run execute if entity @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/statut/gonflette
execute as @e[tag=groudon,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=2}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/marto_poing
execute as @e[tag=groudon,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=3}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/griffe_ombre
