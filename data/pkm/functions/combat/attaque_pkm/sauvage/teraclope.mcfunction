#attaque par sauvage
execute as @e[tag=teraclope,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=0}] at @s if entity @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/statut/plenitude
execute as @e[tag=teraclope,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=1}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/represailles
execute as @e[tag=teraclope,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=2}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/ball_ombre
execute as @e[tag=teraclope,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=3}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/statut/feu_follet
