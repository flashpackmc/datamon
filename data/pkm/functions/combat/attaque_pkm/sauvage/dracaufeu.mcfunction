#attaque par sauvage
execute as @e[tag=dracaufeu,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=0}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/lance_flammes
execute as @e[tag=dracaufeu,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=1}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/vol
execute as @e[tag=dracaufeu,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=2}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/deflagration
execute as @e[tag=dracaufeu,tag=!capture,tag=combat,tag=pkm,scores={RND_Attack=3}] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/charge
