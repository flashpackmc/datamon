execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=poussifeu,tag=capture,tag=combat] run tellraw @s {"text":" Tranche","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 1"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=poussifeu,tag=capture,tag=combat] run tellraw @s {"text":" Picpic","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 2"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=poussifeu,tag=capture,tag=combat] run tellraw @s {"text":" Feu Follet","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 3"}}
execute as @a[scores={attaque=6},tag=combat,tag=!action] if entity @e[tag=poussifeu,tag=capture,tag=combat] run tellraw @s {"text":" Flammeche","color":"aqua","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=action] attaque 4"}}



#attaque par capture
execute if entity @p[scores={attaque=1},tag=action] run execute as @e[tag=poussifeu,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/tranche
execute if entity @p[scores={attaque=2},tag=action] run execute as @e[tag=poussifeu,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/picpic
execute if entity @p[scores={attaque=3},tag=action] run execute as @e[tag=poussifeu,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/statut/feu_follet
execute if entity @p[scores={attaque=4},tag=action] run execute as @e[tag=poussifeu,tag=capture,tag=combat,tag=attCapture] at @s run execute as @e[tag=!capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/flammeche


#attaque par sauvage
execute as @e[tag=poussifeu,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=0}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/tranche
execute as @e[tag=poussifeu,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=1}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/physique/picpic
execute as @e[tag=poussifeu,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=2}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/statut/feu_follet
execute as @e[tag=poussifeu,tag=!capture,tag=combat,tag=pkm,tag=attSauvage,scores={RND_Attack=3}] if entity @p[scores={attaque=1..4},tag=action] run execute as @e[tag=!capture,tag=combat,tag=pkm,limit=1] at @s run execute as @e[tag=capture,tag=combat,tag=pkm,distance=..4,limit=1] run function pkm:combat/attaque/nom_attaque/special/flammeche

