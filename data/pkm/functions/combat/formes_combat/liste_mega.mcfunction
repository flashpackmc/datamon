tellraw @a[tag=combat] ["",{"text":" Le ","color":"light_purple"},{"selector":"@e[tag=combat,tag=capture,tag=peutMega]"},{"text":" réagit","color":"aqua"},{"text":" au","color":"blue"},{"text":" Méga-Anneau","color":"dark_red"},{"text":" de ","color":"yellow"},{"selector":"@p[tag=combat,tag=addMega]","color":"red"},{"text":"!","color":"gold"}]

execute as @e[tag=dracaufeu,tag=combat,tag=peutMega,tag=captPokeball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_pokeball
execute as @e[tag=dracaufeu,tag=combat,tag=peutMega,tag=captMasterball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_masterball
execute as @e[tag=dracaufeu,tag=combat,tag=peutMega,tag=captHonorball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_honorball
execute as @e[tag=dracaufeu,tag=combat,tag=peutMega,tag=captSuperball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_superball
execute as @e[tag=dracaufeu,tag=combat,tag=peutMega,tag=captHyperball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/dracaufeu_x/dracaufeu_x_hyperball

execute as @e[tag=brasegali,tag=combat,tag=peutMega,tag=captPokeball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_pokeball
execute as @e[tag=brasegali,tag=combat,tag=peutMega,tag=captMasterball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_masterball
execute as @e[tag=brasegali,tag=combat,tag=peutMega,tag=captHonorball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_honorball
execute as @e[tag=brasegali,tag=combat,tag=peutMega,tag=captSuperball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_superball
execute as @e[tag=brasegali,tag=combat,tag=peutMega,tag=captHyperball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_brasegali/mega_brasegali_hyperball

execute as @e[tag=laggron,tag=combat,tag=peutMega,tag=captPokeball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_laggron/mega_laggron_pokeball
execute as @e[tag=laggron,tag=combat,tag=peutMega,tag=captMasterball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_laggron/mega_laggron_masterball
execute as @e[tag=laggron,tag=combat,tag=peutMega,tag=captHonorball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_laggron/mega_laggron_honorball
execute as @e[tag=laggron,tag=combat,tag=peutMega,tag=captSuperball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_laggron/mega_laggron_superball
execute as @e[tag=laggron,tag=combat,tag=peutMega,tag=captHyperball] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/mega_laggron/mega_laggron_hyperball
####### grand X ne marche pas, il faut mettre petit x
####### ne pas toucher a : as @e[tag=combat,tag=!capture], je ne sais pas pourquoi


tellraw @a[tag=combat] ["",{"text":" ","color":"white"},{"selector":"@e[tag=combat,tag=capture,tag=peutMega]"},{"text":" méga-évolue en ","color":"white"},{"selector":"@e[tag=combat,tag=estMega,tag=capture,limit=1]"},{"text":"!","color":"white"}]
tellraw @a[tag=combat] {"text":" "}

kill @e[tag=combat,tag=peutMega,tag=capture]