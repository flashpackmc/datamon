execute if entity @a[nbt={Inventory:[{id:"minecraft:player_head",tag:{display:{Name:"{\"text\":\"Orbe Dragon\",\"color\":\"green\"}"}}}]}] as @e[tag=necrozma_criniere,tag=combat] run tag @s add orbeUltra
execute if entity @a[nbt={Inventory:[{id:"minecraft:player_head",tag:{display:{Name:"{\"text\":\"Orbe Dragon\",\"color\":\"green\"}"}}}]}] as @e[tag=necrozma_ailes,tag=combat] run tag @s add orbeUltra


execute as @e[tag=necrozma_ailes,tag=combat,tag=captPokeball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_pokeball
execute as @e[tag=necrozma_ailes,tag=combat,tag=captMasterball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_masterball
execute as @e[tag=necrozma_ailes,tag=combat,tag=captHonorball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_honorball
execute as @e[tag=necrozma_ailes,tag=combat,tag=captSuperball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_superball
execute as @e[tag=necrozma_ailes,tag=combat,tag=captHyperball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_hyperball

execute as @e[tag=necrozma_criniere,tag=combat,tag=captPokeball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_pokeball
execute as @e[tag=necrozma_criniere,tag=combat,tag=captMasterball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_masterball
execute as @e[tag=necrozma_criniere,tag=combat,tag=captHonorball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_honorball
execute as @e[tag=necrozma_criniere,tag=combat,tag=captSuperball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_superball
execute as @e[tag=necrozma_criniere,tag=combat,tag=captHyperball,tag=orbeUltra] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/ultra_necrozma/ultra_necrozma_hyperball



execute if entity @e[tag=orbeUltra,tag=necrozma_criniere] run tellraw @a[tag=combat] ["",{"text":" Une lumière éblouissante émane de ","color":"white"},{"selector":"@e[tag=combat,tag=necrozma_criniere,tag=capture,limit=1]"},{"text":" !","color":"white"}]
execute if entity @e[tag=orbeUltra,tag=necrozma_ailes] run tellraw @a[tag=combat] ["",{"text":" Une lumière éblouissante émane de ","color":"white"},{"selector":"@e[tag=combat,tag=necrozma_ailes,tag=capture,limit=1]"},{"text":" !","color":"white"}]
execute if entity @e[tag=orbeUltra] run tellraw @a[tag=combat] ["",{"text":" Le Necrozma a pris une nouvelle forme grâce à l'Ultra-Explosion!","color":"white"}]
execute if entity @e[tag=orbeUltra] run tellraw @a[tag=combat] {"text":" "}

execute if entity @e[tag=orbeUltra,tag=necrozma_criniere] run tag @e[tag=combat,tag=!ailes,tag=!criniere] add criniere
execute if entity @e[tag=orbeUltra,tag=necrozma_ailes] run tag @e[tag=combat,tag=!ailes,tag=!criniere] add ailes

kill @e[tag=combat,tag=capture,tag=orbeUltra]