execute if entity @a[nbt={Inventory:[{id:"minecraft:player_head",tag:{display:{Name:"{\"text\":\"Orbe Feu\",\"color\":\"green\"}"}}}]}] as @e[tag=groudon,tag=combat,tag=peutPrimal] run tag @s add orbeGroudon
execute if entity @a[nbt={Inventory:[{id:"minecraft:player_head",tag:{display:{Name:"{\"text\":\"Orbe Eau\",\"color\":\"green\"}"}}}]}] as @e[tag=kyogre,tag=combat,tag=peutPrimal] run tag @s add orbeKyogre


execute as @e[tag=kyogre,tag=combat,tag=peutPrimal,tag=captPokeball,tag=orbeKyogre] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_pokeball
execute as @e[tag=kyogre,tag=combat,tag=peutPrimal,tag=captMasterball,tag=orbeKyogre] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_masterball
execute as @e[tag=kyogre,tag=combat,tag=peutPrimal,tag=captHonorball,tag=orbeKyogre] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_honorball
execute as @e[tag=kyogre,tag=combat,tag=peutPrimal,tag=captSuperball,tag=orbeKyogre] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_superball
execute as @e[tag=kyogre,tag=combat,tag=peutPrimal,tag=captHyperball,tag=orbeKyogre] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_kyogre/primo_kyogre_hyperball

execute as @e[tag=groudon,tag=combat,tag=peutPrimal,tag=captPokeball,tag=orbeGroudon] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_groudon/primo_groudon_pokeball
execute as @e[tag=groudon,tag=combat,tag=peutPrimal,tag=captMasterball,tag=orbeGroudon] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_groudon/primo_groudon_masterball
execute as @e[tag=groudon,tag=combat,tag=peutPrimal,tag=captHonorball,tag=orbeGroudon] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_groudon/primo_groudon_honorball
execute as @e[tag=groudon,tag=combat,tag=peutPrimal,tag=captSuperball,tag=orbeGroudon] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_groudon/primo_groudon_superball
execute as @e[tag=groudon,tag=combat,tag=peutPrimal,tag=captHyperball,tag=orbeGroudon] at @e[tag=combat,tag=!capture,tag=pkm] run function pkm:summon/dresseur/primo_groudon/primo_groudon_hyperball



execute if entity @e[tag=peutPrimal,tag=orbeKyogre] run tellraw @a[tag=combat] ["",{"text":" Primo-Résurgence de ","color":"white"},{"selector":"@e[tag=combat,tag=estPrimal,tag=capture,limit=1]"},{"text":" !","color":"white"}]
execute if entity @e[tag=peutPrimal,tag=orbeKyogre] run tellraw @a[tag=combat] ["",{"text":" Il retrouve son apparence originelle !","color":"white"}]
execute if entity @e[tag=peutPrimal,tag=orbeKyogre] run tellraw @a[tag=combat] {"text":" "}

kill @e[tag=combat,tag=peutPrimal,tag=capture,tag=orbeKyogre]


execute if entity @e[tag=peutPrimal,tag=orbeGroudon] run tellraw @a[tag=combat] ["",{"text":" Primo-Résurgence de ","color":"white"},{"selector":"@e[tag=combat,tag=estPrimal,tag=capture,limit=1]"},{"text":" !","color":"white"}]
execute if entity @e[tag=peutPrimal,tag=orbeGroudon] run tellraw @a[tag=combat] ["",{"text":" Il retrouve son apparence originelle !","color":"white"}]
execute if entity @e[tag=peutPrimal,tag=orbeGroudon] run tellraw @a[tag=combat] {"text":" "}

kill @e[tag=combat,tag=peutPrimal,tag=capture,tag=orbeGroudon]