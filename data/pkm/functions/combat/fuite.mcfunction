execute unless entity @e[tag=combat,tag=capture] if entity @a[tag=combat] run function pkm:combat/mort_capture

execute at @p[scores={tour=2}] run tp @e[tag=combat,tag=!capture,tag=pkm] ~ ~-600 ~
execute as @a[scores={tour=2}] run function pkm:combat/liste_ball
execute as @a[scores={tour=2}] run kill @e[tag=combat,tag=capture,tag=pkm]
execute as @a[scores={tour=2}] run tag @s remove combat
execute as @a[scores={tour=2}] run tag @s remove action
execute as @a[scores={tour=2}] run tag @s remove en_capture
execute as @a[scores={tour=2}] run tag @s remove mega
execute as @a[scores={tour=2}] run tag @s remove ultra
execute as @a[scores={tour=2}] run tag @s remove distorsion
execute as @a[scores={tour=2}] run tag @s remove attrape
execute as @a[scores={tour=2}] run tag @s remove en_fuite
#trouve solution si un jour multijoueur

stopsound @a[scores={tour=2}] record
execute as @a[scores={tour=2}] run scoreboard players set @s attaque 0
execute as @a[scores={tour=2}] run scoreboard players set @s tour 0

kill @e[tag=captureAS]
