#tour = 0 rien ne ce passe
#tour = 1 affichage du menu
#tour = 2 fuite

#execute if entity @a[tag=combat] run gamerule sendCommandFeedback false
#pour le debug ou developpement, mettre ligne du dessus en commentaire

execute if entity @a[tag=combat,tag=!en_capture,tag=!en_objet] run execute as @e[type=item,tag=!processed] run data modify entity @s Owner set from entity @s Thrower
execute if entity @a[tag=combat,tag=!en_capture,tag=!en_objet] run execute as @e[type=item,tag=!processed] run data modify entity @s PickupDelay set value 0
execute if entity @a[tag=combat,tag=!en_capture,tag=!en_objet] run tag @e[type=item] add processed
#permet de ne pas jeter des items au sol durant combat

execute as @e[tag=captureAS] at @e[tag=capture,tag=combat] run tp ~ ~ ~
stopsound @p[tag=combat] music

execute as @e[tag=capture,tag=!combat,tag=!estMega,tag=!estPrimal,tag=!ultra_necrozma] at @s if entity @e[tag=pkm,tag=!capture,distance=..4,limit=1,tag=!combat,tag=!ancien] if entity @p[distance=..4,tag=!combat] run tag @s add prep_combat
execute as @e[tag=capture,tag=prep_combat] at @s if entity @e[tag=pkm,tag=!capture,distance=..4,limit=1,tag=!combat,tag=!ancien] as @e[tag=pkm,tag=!capture,distance=..4,limit=1,tag=!combat] if entity @p[distance=..5,tag=!combat] run tag @s add prep_combat
execute at @e[tag=capture,tag=prep_combat] as @p[distance=..5,tag=!combat] run tag @s add prep_combat
scoreboard players set @a[tag=prep_combat] tour 1

effect give @e[tag=prep_combat,tag=!capture,tag=pkm] minecraft:instant_health 10 10 true
execute at @e[tag=prep_combat,tag=!capture,tag=!legendaire,tag=pkm] run playsound minecraft:music_disc.blocks record @p[tag=prep_combat,distance=..5]
execute at @e[tag=prep_combat,tag=!capture,tag=legendaire,tag=pkm] run playsound minecraft:music.credits record @p[tag=prep_combat,distance=..5]
execute if entity @e[tag=prep_combat,tag=capture] run function pkm:combat/capture_debut
execute as @e[tag=prep_combat] run tag @s add combat
tag @a[tag=prep_combat] remove action
tag @a[tag=prep_combat] remove messCapture
tag @a[tag=prep_combat] remove messObjet
tag @a[tag=prep_combat] remove messSac
tag @a[tag=prep_combat] remove en_objet
tag @a[tag=prep_combat] remove en_sac
tag @a[tag=prep_combat] remove en_capture

tellraw @a[tag=prep_combat] [{"text":" Un ","color":"none"},{"selector":"@e[tag=prep_combat,tag=!capture,tag=pkm]"},{"text":" sauvage apparaît!","color":"none"}]
tellraw @a[tag=prep_combat] [{"text":" ","color":"none"},{"selector":"@e[tag=prep_combat,tag=capture]"},{"text":"! Go!","color":"none"}]

execute if entity @a[tag=prep_combat] as @e[tag=peutPrimal,tag=capture,tag=combat] run function pkm:combat/formes_combat/liste_primal

tag @e[tag=prep_combat] remove prep_combat

execute as @e[tag=combat] run effect give @s slowness 1 255 true
execute as @e[tag=combat] run effect give @s jump_boost 1 128 true

tellraw @a[tag=combat,scores={tour=1}] {"text":" "}
execute at @e[tag=!capture,tag=combat,tag=pkm] run tellraw @a[tag=combat,scores={tour=1}] [{"text":" Que doit faire ","color":"light_purple","bold":true},{"selector":"@e[tag=combat,tag=capture,limit=1,sort=nearest]"},{"text":"?","color":"light_purple","bold":true}]
tellraw @a[tag=combat,scores={tour=1}] {"text":" Attaque","color":"yellow","clickEvent":{"action":"run_command","value":"/scoreboard players set @a[tag=combat,tag=!action,tag=!en_sac,tag=!en_capture,tag=!en_objet] attaque 6"}}
tellraw @a[tag=combat,scores={tour=1}] {"text":" Sac","color":"yellow","clickEvent":{"action":"run_command","value":"/tag @a[tag=combat,tag=!action,tag=!en_sac,tag=!en_capture,tag=!en_objet] add en_sac"}}
tellraw @a[tag=combat,scores={tour=1}] {"text":" Fuite","color":"yellow","clickEvent":{"action":"run_command","value":"/tag @a[tag=combat,tag=!action,tag=!en_sac,tag=!en_capture,tag=!en_objet] add en_fuite"}}
tellraw @a[tag=combat,scores={tour=1}] {"text":" "}



scoreboard players set @a[tag=combat,scores={tour=1}] tour 0
tellraw @a[tag=combat,tag=en_fuite] {"text":" Vous prenez la fuite!"}
scoreboard players set @a[tag=combat,tag=en_fuite] tour 2




execute as @a[tag=combat,tag=en_sac] run function pkm:sac/lancer_sac
function pkm:combat/attaque



execute if entity @p[tag=combat] unless entity @e[tag=pkm,tag=combat,tag=capture] run scoreboard players set @a tour 2
execute if entity @p[tag=combat] unless entity @e[tag=pkm,tag=combat,tag=!capture] run scoreboard players set @a tour 2


execute unless entity @e[tag=pkm,tag=combat,tag=capture] run tellraw @a[tag=combat] {"text":" Votre Pokémon est K.O.!","color":"none"}
execute unless entity @e[tag=pkm,tag=combat,tag=!capture] unless entity @p[tag=attrape] run tellraw @a[tag=combat] {"text":" Le Pokémon sauvage est K.O.!","color":"none"}



execute unless entity @e[tag=pkm,tag=combat,tag=!capture] run scoreboard players add @e[tag=capture,tag=combat,limit=1] XP 1

execute if entity @a[scores={tour=2}] run function pkm:combat/fuite

execute if entity @a[tag=!combat] run gamerule sendCommandFeedback true


execute as @e[tag=pkm,tag=combat,tag=capture] at @s unless block ~ ~ ~ #pkm:spawnable run tp @s @e[tag=combat,tag=pkm,tag=!capture,distance=..4,limit=1]
execute as @e[tag=pkm,tag=combat,tag=capture] at @s unless block ~ ~1 ~ #pkm:spawnable run tp @s @e[tag=combat,tag=pkm,tag=!capture,distance=..4,limit=1]
execute as @e[tag=pkm,tag=combat,tag=!capture] at @s unless block ~ ~ ~ #pkm:spawnable run tp @s @e[tag=combat,tag=pkm,tag=capture,distance=..4,limit=1]
execute as @e[tag=pkm,tag=combat,tag=!capture] at @s unless block ~ ~1 ~ #pkm:spawnable run tp @s @e[tag=combat,tag=pkm,tag=capture,distance=..4,limit=1]


execute at @e[tag=pkm,tag=combat,tag=!capture] if entity @e[tag=pkm,tag=combat,tag=capture,distance=..0.5] run spreadplayers ~ ~ 1 1 false @e[tag=combat,tag=pkm]
#separe les 2 pkm de 1 block