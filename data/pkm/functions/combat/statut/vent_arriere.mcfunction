scoreboard players add @s[tag=vent_arriere,scores={tour_vent_arr=..4}] tour_vent_arr 1
tag @s[tag=vent_arriere,scores={tour_vent_arr=5..}] remove vent_arriere

#on reset a chaque tour car cela permet de fonctionner lors du switch
scoreboard players remove @s boost_vitesse 2
scoreboard players set @s multiplieur2 2


scoreboard players operation @s vitesse /= @s multiplieur2
execute store result entity @s ArmorItems[3].tag.vitesse int 1 run scoreboard players get @s vitesse
scoreboard players reset @s multiplieur2



scoreboard players add @s[tag=vent_arriere] boost_vitesse 2
scoreboard players set @s[tag=vent_arriere] multiplieur2 2


scoreboard players operation @s[tag=vent_arriere] vitesse *= @s[tag=vent_arriere] multiplieur2
execute store result entity @s[tag=vent_arriere] ArmorItems[3].tag.vitesse int 1 run scoreboard players get @s[tag=vent_arriere] vitesse
scoreboard players reset @s[tag=vent_arriere] multiplieur2