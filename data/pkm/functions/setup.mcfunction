
#scoreboard objectives add Temps dummy
#scoreboard objectives add Health health
#scoreboard objectives add MobScore dummy

scoreboard objectives add tour dummy
scoreboard objectives add attaque dummy
scoreboard objectives add PV dummy
scoreboard objectives add XP dummy
scoreboard objectives add LVL dummy

scoreboard objectives add RND_Attack dummy
scoreboard players set max RND_Attack 4

scoreboard objectives add RND_Miss_Attack dummy
scoreboard players set max RND_Miss_Attack 100

scoreboard objectives add RND_Critique dummy
scoreboard players set max RND_Critique 16

scoreboard objectives add RND_Capture dummy
scoreboard players set max RND_Capture 10

scoreboard objectives add RND_Spawn dummy
scoreboard players set max RND_Spawn 1000

scoreboard objectives add RND_Lvl dummy
scoreboard players set max RND_Lvl 100

scoreboard objectives add RND_Statut dummy
scoreboard players set max RND_Statut 100

scoreboard objectives add RND_Paralysie dummy
scoreboard players set max RND_Paralysie 2

scoreboard objectives add RND_Degat dummy
scoreboard players set max RND_Degat 16
#laisser 16, car generer entre 0 et 15
#sert a faire du random dans le calcul de degat

scoreboard objectives add EntityHealth dummy
scoreboard objectives add EntityLevel dummy
team add pokemon
team join pokemon @a
team modify pokemon seeFriendlyInvisibles false

scoreboard players set @a[tag=combat] tour 1

scoreboard objectives add degat dummy
scoreboard objectives add atk dummy
scoreboard objectives add def dummy
scoreboard objectives add atkSpec dummy
scoreboard objectives add defSpec dummy
scoreboard objectives add vitesse dummy
scoreboard objectives add miss_attaque dummy
scoreboard objectives add multiplieur1 dummy
scoreboard objectives add multiplieur2 dummy
scoreboard objectives add tour_poison dummy
scoreboard objectives add vie_clone dummy
scoreboard objectives add pv_origine dummy
scoreboard objectives add atk_origine dummy
scoreboard objectives add def_origine dummy
scoreboard objectives add atkSpec_origine dummy
scoreboard objectives add defSpec_origine dummy
scoreboard objectives add vitesse_origine dummy


#permet de stocker la stat, utile si une stat doit etre modifier temporairement
scoreboard objectives add memorise_pv dummy
scoreboard objectives add memorise_atk dummy
scoreboard objectives add memorise_def dummy
scoreboard objectives add memorise_atkSpec dummy
scoreboard objectives add memorise_defSpec dummy
scoreboard objectives add memorise_vitesse dummy



#pv n'existe pas
scoreboard objectives add boost_atk dummy
scoreboard objectives add boost_def dummy
scoreboard objectives add boost_atkSpec dummy
scoreboard objectives add boost_defSpec dummy
scoreboard objectives add boost_vitesse dummy

#gamerule sendCommandFeedback false
#a mettre si pas de commande executer par le joueur dans sa partie

title @a actionbar [{"text":"Data pack Pokemon reload!"}]