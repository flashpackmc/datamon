scoreboard players operation @p XP = @s XP
execute store result entity @e[type=minecraft:item,limit=1,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{aGive:1}}}] Item.tag.xp int 1 run scoreboard players get @p XP
scoreboard players reset @p XP

scoreboard players operation @p LVL = @s LVL
execute store result entity @e[type=minecraft:item,limit=1,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{aGive:1}}}] Item.tag.lvl int 1 run scoreboard players get @p LVL
scoreboard players reset @p LVL