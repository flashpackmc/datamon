execute as @e[nbt={Item:{tag:{display:{Name:"{\"text\":\"Kyogre\",\"color\":\"dark_purple\"}"}}}}] run summon drowned ~ ~ ~ {Silent:1b,CustomNameVisible:0b,PersistenceRequired:1,CanPickUpLoot:0b,Health:310f,CustomName:"{\"text\":\"Kyogre\",\"color\":\"dark_purple\"}",Tags:["kyogre","pkm","capture","Teau","captMasterball","peutPrimal"],DeathLootTable:"pkm/",ArmorItems:[{},{},{},{id:"minecraft:player_head",Count:1b,tag:{pv:310,atk:205,def:185,atkSpec:305,defSpec:285,vitesse:185,SkullOwner:{Id:[I;1754043529,1817200057,-1698850539,321061177],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjUzYWRiOWM3ZDc4ZDQxYjQxZmMxY2RkM2VhYjdkYTg5YzA4OTI5MDgxZGVmNDg1MWQ3NTIwMjhmNjRlNGNiOSJ9fX0="}]}}}}],ArmorDropChances:[0.85F,0.85F,0.85F,0.0F],ActiveEffects:[{Id:14b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:12b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:28b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:13b,Amplifier:1b,Duration:99999,ShowParticles:0b}],Attributes:[{Name:"generic.max_health",Base:310}]}
execute as @e[tag=kyogre,tag=estPrimal,tag=primalKyogre] at @s run summon drowned ~ ~ ~ {Silent:1b,CustomNameVisible:0b,PersistenceRequired:1,CanPickUpLoot:0b,Health:310f,CustomName:"{\"text\":\"Kyogre\",\"color\":\"dark_purple\"}",Tags:["kyogre","pkm","capture","Teau","captMasterball","peutPrimal"],DeathLootTable:"pkm/",ArmorItems:[{},{},{},{id:"minecraft:player_head",Count:1b,tag:{pv:310,atk:205,def:185,atkSpec:305,defSpec:285,vitesse:185,SkullOwner:{Id:[I;1754043529,1817200057,-1698850539,321061177],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjUzYWRiOWM3ZDc4ZDQxYjQxZmMxY2RkM2VhYjdkYTg5YzA4OTI5MDgxZGVmNDg1MWQ3NTIwMjhmNjRlNGNiOSJ9fX0="}]}}}}],ArmorDropChances:[0.85F,0.85F,0.85F,0.0F],ActiveEffects:[{Id:14b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:12b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:28b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:13b,Amplifier:1b,Duration:99999,ShowParticles:0b}],Attributes:[{Name:"generic.max_health",Base:310}]}

execute unless entity @e[tag=kyogre,tag=estPrimal,tag=primalKyogre] run function pkm:summon/summon_vie

execute as @p[tag=!combat] unless entity @e[tag=primalKyogre,tag=kyogre] run function give:item/masterball_colore
execute unless entity @e[tag=kyogre,tag=estPrimal,tag=primalKyogre] run tp @s[type=!player] ~ ~-600 ~





execute if entity @e[tag=kyogre,tag=estPrimal,tag=primalKyogre] run execute store result score @e[tag=kyogre,tag=estPrimal,tag=captMasterball,limit=1] PV run data get entity @e[tag=kyogre,tag=estPrimal,tag=captMasterball,limit=1] Health

execute if entity @e[tag=kyogre,tag=estPrimal,tag=primalKyogre] run execute store result entity @e[tag=kyogre,tag=capture,tag=peutPrimal,tag=captMasterball,limit=1] Health float 1 run scoreboard players get @e[tag=kyogre,tag=estPrimal,tag=captMasterball,limit=1] PV
#applique la vie

execute if entity @e[tag=kyogre,tag=estPrimal,tag=primalKyogre] run scoreboard players operation @e[tag=kyogre,tag=capture,tag=peutPrimal,tag=captMasterball,limit=1] XP = @e[tag=kyogre,tag=estPrimal,tag=captMasterball,limit=1] XP
execute if entity @e[tag=kyogre,tag=estPrimal,tag=primalKyogre] run scoreboard players operation @e[tag=kyogre,tag=capture,tag=peutPrimal,tag=captMasterball,limit=1] LVL = @e[tag=kyogre,tag=estPrimal,tag=captMasterball,limit=1] LVL
