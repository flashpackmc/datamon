execute as @e[nbt={Item:{tag:{display:{Name:"{\"text\":\"Groudon\",\"color\":\"black\"}"}}}}] run summon zombified_piglin ~ ~ ~ {Silent:1b,CustomNameVisible:0b,PersistenceRequired:1,CanPickUpLoot:0b,Health:310f,CustomName:"{\"text\":\"Groudon\",\"color\":\"black\"}",Tags:["groudon","pkm","capture","Tsol","captHyperball","peutPrimal"],DeathLootTable:"pkm/",ArmorItems:[{},{},{},{id:"minecraft:player_head",Count:1b,tag:{pv:310,atk:305,def:285,atkSpec:205,defSpec:185,vitesse:185,SkullOwner:{Id:[I;1611000747,1049183142,-1123711481,1155502093],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmVmZGMxOTUzYTNmNDkyN2I0MDUzMGE4ZjJiMGU1MDkyNzkxMTc3NTA3YmNlZDU3Y2VmOTE1ODZjM2ExZTc1MSJ9fX0="}]}}}}],ArmorDropChances:[0.85F,0.85F,0.85F,0.0F],ActiveEffects:[{Id:14b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:12b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:28b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:13b,Amplifier:1b,Duration:99999,ShowParticles:0b}],Attributes:[{Name:"generic.max_health",Base:310}]}
execute as @e[tag=groudon,tag=estPrimal,tag=primalGroudon] at @s run summon zombified_piglin ~ ~ ~ {Silent:1b,CustomNameVisible:0b,PersistenceRequired:1,CanPickUpLoot:0b,Health:310f,CustomName:"{\"text\":\"Groudon\",\"color\":\"black\"}",Tags:["groudon","pkm","capture","Tsol","captHyperball","peutPrimal"],DeathLootTable:"pkm/",ArmorItems:[{},{},{},{id:"minecraft:player_head",Count:1b,tag:{pv:310,atk:305,def:285,atkSpec:205,defSpec:185,vitesse:185,SkullOwner:{Id:[I;1611000747,1049183142,-1123711481,1155502093],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmVmZGMxOTUzYTNmNDkyN2I0MDUzMGE4ZjJiMGU1MDkyNzkxMTc3NTA3YmNlZDU3Y2VmOTE1ODZjM2ExZTc1MSJ9fX0="}]}}}}],ArmorDropChances:[0.85F,0.85F,0.85F,0.0F],ActiveEffects:[{Id:14b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:12b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:28b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:13b,Amplifier:1b,Duration:99999,ShowParticles:0b}],Attributes:[{Name:"generic.max_health",Base:310}]}

execute unless entity @e[tag=groudon,tag=estPrimal,tag=primalGroudon] run function pkm:summon/summon_vie

execute as @p[tag=!combat] unless entity @e[tag=primalGroudon,tag=groudon] run function give:item/hyperball_colore
execute unless entity @e[tag=groudon,tag=estPrimal,tag=primalGroudon] run tp @s[type=!player] ~ ~-600 ~





execute if entity @e[tag=groudon,tag=estPrimal,tag=primalGroudon] run execute store result score @e[tag=groudon,tag=estPrimal,tag=captHyperball,limit=1] PV run data get entity @e[tag=groudon,tag=estPrimal,tag=captHyperball,limit=1] Health

execute if entity @e[tag=groudon,tag=estPrimal,tag=primalGroudon] run execute store result entity @e[tag=groudon,tag=capture,tag=peutPrimal,tag=captHyperball,limit=1] Health float 1 run scoreboard players get @e[tag=groudon,tag=estPrimal,tag=captHyperball,limit=1] PV
#applique la vie

execute if entity @e[tag=groudon,tag=estPrimal,tag=primalGroudon] run scoreboard players operation @e[tag=groudon,tag=capture,tag=peutPrimal,tag=captHyperball,limit=1] XP = @e[tag=groudon,tag=estPrimal,tag=captHyperball,limit=1] XP
execute if entity @e[tag=groudon,tag=estPrimal,tag=primalGroudon] run scoreboard players operation @e[tag=groudon,tag=capture,tag=peutPrimal,tag=captHyperball,limit=1] LVL = @e[tag=groudon,tag=estPrimal,tag=captHyperball,limit=1] LVL
