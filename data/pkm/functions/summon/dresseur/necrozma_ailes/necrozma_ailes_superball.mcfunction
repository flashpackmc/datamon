execute as @e[nbt={Item:{tag:{display:{Name:"{\"text\":\"Necrozma Ailes\",\"color\":\"dark_blue\"}"}}}}] run summon zombified_piglin ~ ~ ~ {Silent:1b,CustomNameVisible:0b,PersistenceRequired:1,CanPickUpLoot:0b,Health:304f,CustomName:"{\"text\":\"Necrozma Ailes\",\"color\":\"dark_blue\"}",Tags:["necrozma_ailes","pkm","capture","Tpsy","Tspectre","captSuperball"],DeathLootTable:"pkm/",ArmorItems:[{},{},{},{id:"minecraft:player_head",Count:1b,tag:{pv:304,atk:231,def:223,atkSpec:319,defSpec:259,vitesse:159,SkullOwner:{Id:[I;357997509,1998604321,-1374442663,-1129129394],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzE4NmM4YmQzZDU0MmVkZGJmZDVmNzRkMGFmZmVmNmNmYTgyZTY4NzFhMjVkMGY3Mjc0NTA1M2RmOGE3ZGIyMyJ9fX0="}]}}}}],ArmorDropChances:[0.85F,0.85F,0.85F,0.0F],ActiveEffects:[{Id:14b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:12b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:28b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:13b,Amplifier:1b,Duration:99999,ShowParticles:0b}],Attributes:[{Name:"generic.max_health",Base:304}]}
execute as @e[tag=ultra_necrozma,tag=necrozmaU] at @s run summon zombified_piglin ~ ~ ~ {Silent:1b,CustomNameVisible:0b,PersistenceRequired:1,CanPickUpLoot:0b,Health:304f,CustomName:"{\"text\":\"Necrozma Ailes\",\"color\":\"dark_blue\"}",Tags:["necrozma_ailes","pkm","capture","Tpsy","Tspectre","captSuperball"],DeathLootTable:"pkm/",ArmorItems:[{},{},{},{id:"minecraft:player_head",Count:1b,tag:{pv:304,atk:231,def:223,atkSpec:319,defSpec:259,vitesse:159,SkullOwner:{Id:[I;357997509,1998604321,-1374442663,-1129129394],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzE4NmM4YmQzZDU0MmVkZGJmZDVmNzRkMGFmZmVmNmNmYTgyZTY4NzFhMjVkMGY3Mjc0NTA1M2RmOGE3ZGIyMyJ9fX0="}]}}}}],ArmorDropChances:[0.85F,0.85F,0.85F,0.0F],ActiveEffects:[{Id:14b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:12b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:28b,Amplifier:1b,Duration:99999,ShowParticles:0b},{Id:13b,Amplifier:1b,Duration:99999,ShowParticles:0b}],Attributes:[{Name:"generic.max_health",Base:304}]}

execute unless entity @e[tag=ultra_necrozma,tag=necrozmaU] run function pkm:summon/summon_vie

execute as @p[tag=!combat] unless entity @e[tag=necrozmaU] run function give:item/superball_colore
execute unless entity @e[tag=ultra_necrozma,tag=necrozmaU] run tp @s[type=!player] ~ ~-600 ~




execute if entity @e[tag=ultra_necrozma,tag=necrozmaU] run execute store result score @e[tag=ultra_necrozma,tag=captSuperball,limit=1] PV run data get entity @e[tag=ultra_necrozma,tag=captSuperball,limit=1] Health

execute if entity @e[tag=ultra_necrozma,tag=necrozmaU] run execute store result entity @e[tag=necrozma_ailes,tag=capture,tag=captSuperball,limit=1] Health float 1 run scoreboard players get @e[tag=ultra_necrozma,tag=captSuperball,limit=1] PV
#applique la vie

execute if entity @e[tag=ultra_necrozma,tag=necrozmaU] run scoreboard players operation @e[tag=necrozma_ailes,tag=capture,tag=captSuperball,limit=1] XP = @e[tag=ultra_necrozma,tag=captSuperball,limit=1] XP
execute if entity @e[tag=ultra_necrozma,tag=necrozmaU] run scoreboard players operation @e[tag=necrozma_ailes,tag=capture,tag=captSuperball,limit=1] LVL = @e[tag=ultra_necrozma,tag=captSuperball,limit=1] LVL