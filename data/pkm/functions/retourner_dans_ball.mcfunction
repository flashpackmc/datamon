execute as @e[tag=captPokeball,tag=!combat] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}}}}] run tag @s add retourner
execute as @e[tag=captMasterball,tag=!combat] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}}}}] run tag @s add retourner
execute as @e[tag=captHonorball,tag=!combat] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}}}}] run tag @s add retourner
execute as @e[tag=captSuperball,tag=!combat] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}}}}] run tag @s add retourner
execute as @e[tag=captHyperball,tag=!combat] at @s if entity @e[distance=..3,type=item,nbt={OnGround:1b,Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}}}}] run tag @s add retourner

execute as @e[tag=retourner,tag=captPokeball] at @s run kill @e[distance=..3,type=item,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Poke ball\",\"color\":\"red\"}"}}}}]
execute as @e[tag=retourner,tag=captMasterball] at @s run kill @e[distance=..3,type=item,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Master ball\",\"color\":\"dark_purple\"}"}}}}]
execute as @e[tag=retourner,tag=captHonorball] at @s run kill @e[distance=..3,type=item,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Honor ball\",\"color\":\"white\"}"}}}}]
execute as @e[tag=retourner,tag=captSuperball] at @s run kill @e[distance=..3,type=item,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Super ball\",\"color\":\"dark_blue\"}"}}}}]
execute as @e[tag=retourner,tag=captHyperball] at @s run kill @e[distance=..3,type=item,nbt={Item:{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"{\"text\":\"Hyper ball\",\"color\":\"black\"}"}}}}]



execute as @e[tag=leviator,tag=retourner,tag=captPokeball] run function give:spawn/leviator/leviator_pokeball
execute as @e[tag=leviator,tag=retourner,tag=captMasterball] run function give:spawn/leviator/leviator_masterball
execute as @e[tag=leviator,tag=retourner,tag=captHonorball] run function give:spawn/leviator/leviator_honorball
execute as @e[tag=leviator,tag=retourner,tag=captSuperball] run function give:spawn/leviator/leviator_superball
execute as @e[tag=leviator,tag=retourner,tag=captHyperball] run function give:spawn/leviator/leviator_hyperball


execute as @e[tag=regigigas,tag=retourner,tag=captPokeball] run function give:spawn/regigigas/regigigas_pokeball
execute as @e[tag=regigigas,tag=retourner,tag=captMasterball] run function give:spawn/regigigas/regigigas_masterball
execute as @e[tag=regigigas,tag=retourner,tag=captHonorball] run function give:spawn/regigigas/regigigas_honorball
execute as @e[tag=regigigas,tag=retourner,tag=captSuperball] run function give:spawn/regigigas/regigigas_superball
execute as @e[tag=regigigas,tag=retourner,tag=captHyperball] run function give:spawn/regigigas/regigigas_hyperball

execute as @e[tag=pierroteknik,tag=retourner,tag=captPokeball] run function give:spawn/pierroteknik/pierroteknik_pokeball
execute as @e[tag=pierroteknik,tag=retourner,tag=captMasterball] run function give:spawn/pierroteknik/pierroteknik_masterball
execute as @e[tag=pierroteknik,tag=retourner,tag=captHonorball] run function give:spawn/pierroteknik/pierroteknik_honorball
execute as @e[tag=pierroteknik,tag=retourner,tag=captSuperball] run function give:spawn/pierroteknik/pierroteknik_superball
execute as @e[tag=pierroteknik,tag=retourner,tag=captHyperball] run function give:spawn/pierroteknik/pierroteknik_hyperball

execute as @e[tag=marshadow,tag=retourner,tag=captPokeball] run function give:spawn/marshadow/marshadow_pokeball
execute as @e[tag=marshadow,tag=retourner,tag=captMasterball] run function give:spawn/marshadow/marshadow_masterball
execute as @e[tag=marshadow,tag=retourner,tag=captHonorball] run function give:spawn/marshadow/marshadow_honorball
execute as @e[tag=marshadow,tag=retourner,tag=captSuperball] run function give:spawn/marshadow/marshadow_superball
execute as @e[tag=marshadow,tag=retourner,tag=captHyperball] run function give:spawn/marshadow/marshadow_hyperball

execute as @e[tag=magearna,tag=retourner,tag=captPokeball] run function give:spawn/magearna/magearna_pokeball
execute as @e[tag=magearna,tag=retourner,tag=captMasterball] run function give:spawn/magearna/magearna_masterball
execute as @e[tag=magearna,tag=retourner,tag=captHonorball] run function give:spawn/magearna/magearna_honorball
execute as @e[tag=magearna,tag=retourner,tag=captSuperball] run function give:spawn/magearna/magearna_superball
execute as @e[tag=magearna,tag=retourner,tag=captHyperball] run function give:spawn/magearna/magearna_hyperball

execute as @e[tag=katagami,tag=retourner,tag=captPokeball] run function give:spawn/katagami/katagami_pokeball
execute as @e[tag=katagami,tag=retourner,tag=captMasterball] run function give:spawn/katagami/katagami_masterball
execute as @e[tag=katagami,tag=retourner,tag=captHonorball] run function give:spawn/katagami/katagami_honorball
execute as @e[tag=katagami,tag=retourner,tag=captSuperball] run function give:spawn/katagami/katagami_superball
execute as @e[tag=katagami,tag=retourner,tag=captHyperball] run function give:spawn/katagami/katagami_hyperball

execute as @e[tag=bamboiselle,tag=retourner,tag=captPokeball] run function give:spawn/bamboiselle/bamboiselle_pokeball
execute as @e[tag=bamboiselle,tag=retourner,tag=captMasterball] run function give:spawn/bamboiselle/bamboiselle_masterball
execute as @e[tag=bamboiselle,tag=retourner,tag=captHonorball] run function give:spawn/bamboiselle/bamboiselle_honorball
execute as @e[tag=bamboiselle,tag=retourner,tag=captSuperball] run function give:spawn/bamboiselle/bamboiselle_superball
execute as @e[tag=bamboiselle,tag=retourner,tag=captHyperball] run function give:spawn/bamboiselle/bamboiselle_hyperball

execute as @e[tag=cancrelove,tag=retourner,tag=captPokeball] run function give:spawn/cancrelove/cancrelove_pokeball
execute as @e[tag=cancrelove,tag=retourner,tag=captMasterball] run function give:spawn/cancrelove/cancrelove_masterball
execute as @e[tag=cancrelove,tag=retourner,tag=captHonorball] run function give:spawn/cancrelove/cancrelove_honorball
execute as @e[tag=cancrelove,tag=retourner,tag=captSuperball] run function give:spawn/cancrelove/cancrelove_superball
execute as @e[tag=cancrelove,tag=retourner,tag=captHyperball] run function give:spawn/cancrelove/cancrelove_hyperball

execute as @e[tag=mouscoto,tag=retourner,tag=captPokeball] run function give:spawn/mouscoto/mouscoto_pokeball
execute as @e[tag=mouscoto,tag=retourner,tag=captMasterball] run function give:spawn/mouscoto/mouscoto_masterball
execute as @e[tag=mouscoto,tag=retourner,tag=captHonorball] run function give:spawn/mouscoto/mouscoto_honorball
execute as @e[tag=mouscoto,tag=retourner,tag=captSuperball] run function give:spawn/mouscoto/mouscoto_superball
execute as @e[tag=mouscoto,tag=retourner,tag=captHyperball] run function give:spawn/mouscoto/mouscoto_hyperball

execute as @e[tag=boreas,tag=retourner,tag=captPokeball] run function give:spawn/boreas/boreas_pokeball
execute as @e[tag=boreas,tag=retourner,tag=captMasterball] run function give:spawn/boreas/boreas_masterball
execute as @e[tag=boreas,tag=retourner,tag=captHonorball] run function give:spawn/boreas/boreas_honorball
execute as @e[tag=boreas,tag=retourner,tag=captSuperball] run function give:spawn/boreas/boreas_superball
execute as @e[tag=boreas,tag=retourner,tag=captHyperball] run function give:spawn/boreas/boreas_hyperball


execute as @e[tag=fulguris,tag=retourner,tag=captPokeball] run function give:spawn/fulguris/fulguris_pokeball
execute as @e[tag=fulguris,tag=retourner,tag=captMasterball] run function give:spawn/fulguris/fulguris_masterball
execute as @e[tag=fulguris,tag=retourner,tag=captHonorball] run function give:spawn/fulguris/fulguris_honorball
execute as @e[tag=fulguris,tag=retourner,tag=captSuperball] run function give:spawn/fulguris/fulguris_superball
execute as @e[tag=fulguris,tag=retourner,tag=captHyperball] run function give:spawn/fulguris/fulguris_hyperball


execute as @e[tag=demeteros,tag=retourner,tag=captPokeball] run function give:spawn/demeteros/demeteros_pokeball
execute as @e[tag=demeteros,tag=retourner,tag=captMasterball] run function give:spawn/demeteros/demeteros_masterball
execute as @e[tag=demeteros,tag=retourner,tag=captHonorball] run function give:spawn/demeteros/demeteros_honorball
execute as @e[tag=demeteros,tag=retourner,tag=captSuperball] run function give:spawn/demeteros/demeteros_superball
execute as @e[tag=demeteros,tag=retourner,tag=captHyperball] run function give:spawn/demeteros/demeteros_hyperball


execute as @e[tag=reshiram,tag=retourner,tag=captPokeball] run function give:spawn/reshiram/reshiram_pokeball
execute as @e[tag=reshiram,tag=retourner,tag=captMasterball] run function give:spawn/reshiram/reshiram_masterball
execute as @e[tag=reshiram,tag=retourner,tag=captHonorball] run function give:spawn/reshiram/reshiram_honorball
execute as @e[tag=reshiram,tag=retourner,tag=captSuperball] run function give:spawn/reshiram/reshiram_superball
execute as @e[tag=reshiram,tag=retourner,tag=captHyperball] run function give:spawn/reshiram/reshiram_hyperball

execute as @e[tag=porygon_z,tag=retourner,tag=captPokeball] run function give:spawn/porygon_z/porygon_z_pokeball
execute as @e[tag=porygon_z,tag=retourner,tag=captMasterball] run function give:spawn/porygon_z/porygon_z_masterball
execute as @e[tag=porygon_z,tag=retourner,tag=captHonorball] run function give:spawn/porygon_z/porygon_z_honorball
execute as @e[tag=porygon_z,tag=retourner,tag=captSuperball] run function give:spawn/porygon_z/porygon_z_superball
execute as @e[tag=porygon_z,tag=retourner,tag=captHyperball] run function give:spawn/porygon_z/porygon_z_hyperball

execute as @e[tag=porygon_2,tag=retourner,tag=captPokeball] run function give:spawn/porygon_2/porygon_2_pokeball
execute as @e[tag=porygon_2,tag=retourner,tag=captMasterball] run function give:spawn/porygon_2/porygon_2_masterball
execute as @e[tag=porygon_2,tag=retourner,tag=captHonorball] run function give:spawn/porygon_2/porygon_2_honorball
execute as @e[tag=porygon_2,tag=retourner,tag=captSuperball] run function give:spawn/porygon_2/porygon_2_superball
execute as @e[tag=porygon_2,tag=retourner,tag=captHyperball] run function give:spawn/porygon_2/porygon_2_hyperball

execute as @e[tag=porygon,tag=retourner,tag=captPokeball] run function give:spawn/porygon/porygon_pokeball
execute as @e[tag=porygon,tag=retourner,tag=captMasterball] run function give:spawn/porygon/porygon_masterball
execute as @e[tag=porygon,tag=retourner,tag=captHonorball] run function give:spawn/porygon/porygon_honorball
execute as @e[tag=porygon,tag=retourner,tag=captSuperball] run function give:spawn/porygon/porygon_superball
execute as @e[tag=porygon,tag=retourner,tag=captHyperball] run function give:spawn/porygon/porygon_hyperball

execute as @e[tag=nymphali,tag=retourner,tag=captPokeball] run function give:spawn/nymphali/nymphali_pokeball
execute as @e[tag=nymphali,tag=retourner,tag=captMasterball] run function give:spawn/nymphali/nymphali_masterball
execute as @e[tag=nymphali,tag=retourner,tag=captHonorball] run function give:spawn/nymphali/nymphali_honorball
execute as @e[tag=nymphali,tag=retourner,tag=captSuperball] run function give:spawn/nymphali/nymphali_superball
execute as @e[tag=nymphali,tag=retourner,tag=captHyperball] run function give:spawn/nymphali/nymphali_hyperball

execute as @e[tag=evoli,tag=retourner,tag=captPokeball] run function give:spawn/evoli/evoli_pokeball
execute as @e[tag=evoli,tag=retourner,tag=captMasterball] run function give:spawn/evoli/evoli_masterball
execute as @e[tag=evoli,tag=retourner,tag=captHonorball] run function give:spawn/evoli/evoli_honorball
execute as @e[tag=evoli,tag=retourner,tag=captSuperball] run function give:spawn/evoli/evoli_superball
execute as @e[tag=evoli,tag=retourner,tag=captHyperball] run function give:spawn/evoli/evoli_hyperball


execute as @e[tag=phyllali,tag=retourner,tag=captPokeball] run function give:spawn/phyllali/phyllali_pokeball
execute as @e[tag=phyllali,tag=retourner,tag=captMasterball] run function give:spawn/phyllali/phyllali_masterball
execute as @e[tag=phyllali,tag=retourner,tag=captHonorball] run function give:spawn/phyllali/phyllali_honorball
execute as @e[tag=phyllali,tag=retourner,tag=captSuperball] run function give:spawn/phyllali/phyllali_superball
execute as @e[tag=phyllali,tag=retourner,tag=captHyperball] run function give:spawn/phyllali/phyllali_hyperball

execute as @e[tag=givrali,tag=retourner,tag=captPokeball] run function give:spawn/givrali/givrali_pokeball
execute as @e[tag=givrali,tag=retourner,tag=captMasterball] run function give:spawn/givrali/givrali_masterball
execute as @e[tag=givrali,tag=retourner,tag=captHonorball] run function give:spawn/givrali/givrali_honorball
execute as @e[tag=givrali,tag=retourner,tag=captSuperball] run function give:spawn/givrali/givrali_superball
execute as @e[tag=givrali,tag=retourner,tag=captHyperball] run function give:spawn/givrali/givrali_hyperball


execute as @e[tag=pyroli,tag=retourner,tag=captPokeball] run function give:spawn/pyroli/pyroli_pokeball
execute as @e[tag=pyroli,tag=retourner,tag=captMasterball] run function give:spawn/pyroli/pyroli_masterball
execute as @e[tag=pyroli,tag=retourner,tag=captHonorball] run function give:spawn/pyroli/pyroli_honorball
execute as @e[tag=pyroli,tag=retourner,tag=captSuperball] run function give:spawn/pyroli/pyroli_superball
execute as @e[tag=pyroli,tag=retourner,tag=captHyperball] run function give:spawn/pyroli/pyroli_hyperball

execute as @e[tag=aquali,tag=retourner,tag=captPokeball] run function give:spawn/aquali/aquali_pokeball
execute as @e[tag=aquali,tag=retourner,tag=captMasterball] run function give:spawn/aquali/aquali_masterball
execute as @e[tag=aquali,tag=retourner,tag=captHonorball] run function give:spawn/aquali/aquali_honorball
execute as @e[tag=aquali,tag=retourner,tag=captSuperball] run function give:spawn/aquali/aquali_superball
execute as @e[tag=aquali,tag=retourner,tag=captHyperball] run function give:spawn/aquali/aquali_hyperball

execute as @e[tag=mentali,tag=retourner,tag=captPokeball] run function give:spawn/mentali/mentali_pokeball
execute as @e[tag=mentali,tag=retourner,tag=captMasterball] run function give:spawn/mentali/mentali_masterball
execute as @e[tag=mentali,tag=retourner,tag=captHonorball] run function give:spawn/mentali/mentali_honorball
execute as @e[tag=mentali,tag=retourner,tag=captSuperball] run function give:spawn/mentali/mentali_superball
execute as @e[tag=mentali,tag=retourner,tag=captHyperball] run function give:spawn/mentali/mentali_hyperball

execute as @e[tag=voltali,tag=retourner,tag=captPokeball] run function give:spawn/voltali/voltali_pokeball
execute as @e[tag=voltali,tag=retourner,tag=captMasterball] run function give:spawn/voltali/voltali_masterball
execute as @e[tag=voltali,tag=retourner,tag=captHonorball] run function give:spawn/voltali/voltali_honorball
execute as @e[tag=voltali,tag=retourner,tag=captSuperball] run function give:spawn/voltali/voltali_superball
execute as @e[tag=voltali,tag=retourner,tag=captHyperball] run function give:spawn/voltali/voltali_hyperball

execute as @e[tag=groudon,tag=retourner,tag=captPokeball] run function give:spawn/groudon/groudon_pokeball
execute as @e[tag=groudon,tag=retourner,tag=captMasterball] run function give:spawn/groudon/groudon_masterball
execute as @e[tag=groudon,tag=retourner,tag=captHonorball] run function give:spawn/groudon/groudon_honorball
execute as @e[tag=groudon,tag=retourner,tag=captSuperball] run function give:spawn/groudon/groudon_superball
execute as @e[tag=groudon,tag=retourner,tag=captHyperball] run function give:spawn/groudon/groudon_hyperball

execute as @e[tag=kyogre,tag=retourner,tag=captPokeball] run function give:spawn/kyogre/kyogre_pokeball
execute as @e[tag=kyogre,tag=retourner,tag=captMasterball] run function give:spawn/kyogre/kyogre_masterball
execute as @e[tag=kyogre,tag=retourner,tag=captHonorball] run function give:spawn/kyogre/kyogre_honorball
execute as @e[tag=kyogre,tag=retourner,tag=captSuperball] run function give:spawn/kyogre/kyogre_superball
execute as @e[tag=kyogre,tag=retourner,tag=captHyperball] run function give:spawn/kyogre/kyogre_hyperball

execute as @e[tag=noctali,tag=retourner,tag=captPokeball] run function give:spawn/noctali/noctali_pokeball
execute as @e[tag=noctali,tag=retourner,tag=captMasterball] run function give:spawn/noctali/noctali_masterball
execute as @e[tag=noctali,tag=retourner,tag=captHonorball] run function give:spawn/noctali/noctali_honorball
execute as @e[tag=noctali,tag=retourner,tag=captSuperball] run function give:spawn/noctali/noctali_superball
execute as @e[tag=noctali,tag=retourner,tag=captHyperball] run function give:spawn/noctali/noctali_hyperball

execute as @e[tag=dracaufeu,tag=retourner,tag=captPokeball] run function give:spawn/dracaufeu/dracaufeu_pokeball
execute as @e[tag=dracaufeu,tag=retourner,tag=captMasterball] run function give:spawn/dracaufeu/dracaufeu_masterball
execute as @e[tag=dracaufeu,tag=retourner,tag=captHonorball] run function give:spawn/dracaufeu/dracaufeu_honorball
execute as @e[tag=dracaufeu,tag=retourner,tag=captSuperball] run function give:spawn/dracaufeu/dracaufeu_superball
execute as @e[tag=dracaufeu,tag=retourner,tag=captHyperball] run function give:spawn/dracaufeu/dracaufeu_hyperball

execute as @e[tag=teraclope,tag=retourner,tag=captPokeball] run function give:spawn/teraclope/teraclope_pokeball
execute as @e[tag=teraclope,tag=retourner,tag=captMasterball] run function give:spawn/teraclope/teraclope_masterball
execute as @e[tag=teraclope,tag=retourner,tag=captHonorball] run function give:spawn/teraclope/teraclope_honorball
execute as @e[tag=teraclope,tag=retourner,tag=captSuperball] run function give:spawn/teraclope/teraclope_superball
execute as @e[tag=teraclope,tag=retourner,tag=captHyperball] run function give:spawn/teraclope/teraclope_hyperball

execute as @e[tag=noctunoir,tag=retourner,tag=captPokeball] run function give:spawn/noctunoir/noctunoir_pokeball
execute as @e[tag=noctunoir,tag=retourner,tag=captMasterball] run function give:spawn/noctunoir/noctunoir_masterball
execute as @e[tag=noctunoir,tag=retourner,tag=captHonorball] run function give:spawn/noctunoir/noctunoir_honorball
execute as @e[tag=noctunoir,tag=retourner,tag=captSuperball] run function give:spawn/noctunoir/noctunoir_superball
execute as @e[tag=noctunoir,tag=retourner,tag=captHyperball] run function give:spawn/noctunoir/noctunoir_hyperball

execute as @e[tag=mimiqui,tag=retourner,tag=captPokeball] run function give:spawn/mimiqui/mimiqui_pokeball
execute as @e[tag=mimiqui,tag=retourner,tag=captMasterball] run function give:spawn/mimiqui/mimiqui_masterball
execute as @e[tag=mimiqui,tag=retourner,tag=captHonorball] run function give:spawn/mimiqui/mimiqui_honorball
execute as @e[tag=mimiqui,tag=retourner,tag=captSuperball] run function give:spawn/mimiqui/mimiqui_superball
execute as @e[tag=mimiqui,tag=retourner,tag=captHyperball] run function give:spawn/mimiqui/mimiqui_hyperball

execute as @e[tag=pachirisu,tag=retourner,tag=captPokeball] run function give:spawn/pachirisu/pachirisu_pokeball
execute as @e[tag=pachirisu,tag=retourner,tag=captMasterball] run function give:spawn/pachirisu/pachirisu_masterball
execute as @e[tag=pachirisu,tag=retourner,tag=captHonorball] run function give:spawn/pachirisu/pachirisu_honorball
execute as @e[tag=pachirisu,tag=retourner,tag=captSuperball] run function give:spawn/pachirisu/pachirisu_superball
execute as @e[tag=pachirisu,tag=retourner,tag=captHyperball] run function give:spawn/pachirisu/pachirisu_hyperball

execute as @e[tag=skelenox,tag=retourner,tag=captPokeball] run function give:spawn/skelenox/skelenox_pokeball
execute as @e[tag=skelenox,tag=retourner,tag=captMasterball] run function give:spawn/skelenox/skelenox_masterball
execute as @e[tag=skelenox,tag=retourner,tag=captHonorball] run function give:spawn/skelenox/skelenox_honorball
execute as @e[tag=skelenox,tag=retourner,tag=captSuperball] run function give:spawn/skelenox/skelenox_superball
execute as @e[tag=skelenox,tag=retourner,tag=captHyperball] run function give:spawn/skelenox/skelenox_hyperball

execute as @e[tag=magicarpe,tag=retourner,tag=captPokeball] run function give:spawn/magicarpe/magicarpe_pokeball
execute as @e[tag=magicarpe,tag=retourner,tag=captMasterball] run function give:spawn/magicarpe/magicarpe_masterball
execute as @e[tag=magicarpe,tag=retourner,tag=captHonorball] run function give:spawn/magicarpe/magicarpe_honorball
execute as @e[tag=magicarpe,tag=retourner,tag=captSuperball] run function give:spawn/magicarpe/magicarpe_superball
execute as @e[tag=magicarpe,tag=retourner,tag=captHyperball] run function give:spawn/magicarpe/magicarpe_hyperball

execute as @e[tag=minidraco,tag=retourner,tag=captPokeball] run function give:spawn/minidraco/minidraco_pokeball
execute as @e[tag=minidraco,tag=retourner,tag=captMasterball] run function give:spawn/minidraco/minidraco_masterball
execute as @e[tag=minidraco,tag=retourner,tag=captHonorball] run function give:spawn/minidraco/minidraco_honorball
execute as @e[tag=minidraco,tag=retourner,tag=captSuperball] run function give:spawn/minidraco/minidraco_superball
execute as @e[tag=minidraco,tag=retourner,tag=captHyperball] run function give:spawn/minidraco/minidraco_hyperball

execute as @e[tag=draco,tag=retourner,tag=captPokeball] run function give:spawn/draco/draco_pokeball
execute as @e[tag=draco,tag=retourner,tag=captMasterball] run function give:spawn/draco/draco_masterball
execute as @e[tag=draco,tag=retourner,tag=captHonorball] run function give:spawn/draco/draco_honorball
execute as @e[tag=draco,tag=retourner,tag=captSuperball] run function give:spawn/draco/draco_superball
execute as @e[tag=draco,tag=retourner,tag=captHyperball] run function give:spawn/draco/draco_hyperball

execute as @e[tag=dracolosse,tag=retourner,tag=captPokeball] run function give:spawn/dracolosse/dracolosse_pokeball
execute as @e[tag=dracolosse,tag=retourner,tag=captMasterball] run function give:spawn/dracolosse/dracolosse_masterball
execute as @e[tag=dracolosse,tag=retourner,tag=captHonorball] run function give:spawn/dracolosse/dracolosse_honorball
execute as @e[tag=dracolosse,tag=retourner,tag=captSuperball] run function give:spawn/dracolosse/dracolosse_superball
execute as @e[tag=dracolosse,tag=retourner,tag=captHyperball] run function give:spawn/dracolosse/dracolosse_hyperball

execute as @e[tag=genesect,tag=retourner,tag=captPokeball] run function give:spawn/genesect/genesect_pokeball
execute as @e[tag=genesect,tag=retourner,tag=captMasterball] run function give:spawn/genesect/genesect_masterball
execute as @e[tag=genesect,tag=retourner,tag=captHonorball] run function give:spawn/genesect/genesect_honorball
execute as @e[tag=genesect,tag=retourner,tag=captSuperball] run function give:spawn/genesect/genesect_superball
execute as @e[tag=genesect,tag=retourner,tag=captHyperball] run function give:spawn/genesect/genesect_hyperball

execute as @e[tag=laggron,tag=retourner,tag=captPokeball] run function give:spawn/laggron/laggron_pokeball
execute as @e[tag=laggron,tag=retourner,tag=captMasterball] run function give:spawn/laggron/laggron_masterball
execute as @e[tag=laggron,tag=retourner,tag=captHonorball] run function give:spawn/laggron/laggron_honorball
execute as @e[tag=laggron,tag=retourner,tag=captSuperball] run function give:spawn/laggron/laggron_superball
execute as @e[tag=laggron,tag=retourner,tag=captHyperball] run function give:spawn/laggron/laggron_hyperball

execute as @e[tag=reptincel,tag=retourner,tag=captPokeball] run function give:spawn/reptincel/reptincel_pokeball
execute as @e[tag=reptincel,tag=retourner,tag=captMasterball] run function give:spawn/reptincel/reptincel_masterball
execute as @e[tag=reptincel,tag=retourner,tag=captHonorball] run function give:spawn/reptincel/reptincel_honorball
execute as @e[tag=reptincel,tag=retourner,tag=captSuperball] run function give:spawn/reptincel/reptincel_superball
execute as @e[tag=reptincel,tag=retourner,tag=captHyperball] run function give:spawn/reptincel/reptincel_hyperball

execute as @e[tag=salameche,tag=retourner,tag=captPokeball] run function give:spawn/salameche/salameche_pokeball
execute as @e[tag=salameche,tag=retourner,tag=captMasterball] run function give:spawn/salameche/salameche_masterball
execute as @e[tag=salameche,tag=retourner,tag=captHonorball] run function give:spawn/salameche/salameche_honorball
execute as @e[tag=salameche,tag=retourner,tag=captSuperball] run function give:spawn/salameche/salameche_superball
execute as @e[tag=salameche,tag=retourner,tag=captHyperball] run function give:spawn/salameche/salameche_hyperball

execute as @e[tag=brasegali,tag=retourner,tag=captPokeball] run function give:spawn/brasegali/brasegali_pokeball
execute as @e[tag=brasegali,tag=retourner,tag=captMasterball] run function give:spawn/brasegali/brasegali_masterball
execute as @e[tag=brasegali,tag=retourner,tag=captHonorball] run function give:spawn/brasegali/brasegali_honorball
execute as @e[tag=brasegali,tag=retourner,tag=captSuperball] run function give:spawn/brasegali/brasegali_superball
execute as @e[tag=brasegali,tag=retourner,tag=captHyperball] run function give:spawn/brasegali/brasegali_hyperball

execute as @e[tag=flobio,tag=retourner,tag=captPokeball] run function give:spawn/flobio/flobio_pokeball
execute as @e[tag=flobio,tag=retourner,tag=captMasterball] run function give:spawn/flobio/flobio_masterball
execute as @e[tag=flobio,tag=retourner,tag=captHonorball] run function give:spawn/flobio/flobio_honorball
execute as @e[tag=flobio,tag=retourner,tag=captSuperball] run function give:spawn/flobio/flobio_superball
execute as @e[tag=flobio,tag=retourner,tag=captHyperball] run function give:spawn/flobio/flobio_hyperball

execute as @e[tag=gobou,tag=retourner,tag=captPokeball] run function give:spawn/gobou/gobou_pokeball
execute as @e[tag=gobou,tag=retourner,tag=captMasterball] run function give:spawn/gobou/gobou_masterball
execute as @e[tag=gobou,tag=retourner,tag=captHonorball] run function give:spawn/gobou/gobou_honorball
execute as @e[tag=gobou,tag=retourner,tag=captSuperball] run function give:spawn/gobou/gobou_superball
execute as @e[tag=gobou,tag=retourner,tag=captHyperball] run function give:spawn/gobou/gobou_hyperball

execute as @e[tag=galifeu,tag=retourner,tag=captPokeball] run function give:spawn/galifeu/galifeu_pokeball
execute as @e[tag=galifeu,tag=retourner,tag=captMasterball] run function give:spawn/galifeu/galifeu_masterball
execute as @e[tag=galifeu,tag=retourner,tag=captHonorball] run function give:spawn/galifeu/galifeu_honorball
execute as @e[tag=galifeu,tag=retourner,tag=captSuperball] run function give:spawn/galifeu/galifeu_superball
execute as @e[tag=galifeu,tag=retourner,tag=captHyperball] run function give:spawn/galifeu/galifeu_hyperball

execute as @e[tag=kyurem,tag=retourner,tag=captPokeball] run function give:spawn/kyurem/kyurem_pokeball
execute as @e[tag=kyurem,tag=retourner,tag=captMasterball] run function give:spawn/kyurem/kyurem_masterball
execute as @e[tag=kyurem,tag=retourner,tag=captHonorball] run function give:spawn/kyurem/kyurem_honorball
execute as @e[tag=kyurem,tag=retourner,tag=captSuperball] run function give:spawn/kyurem/kyurem_superball
execute as @e[tag=kyurem,tag=retourner,tag=captHyperball] run function give:spawn/kyurem/kyurem_hyperball

execute as @e[tag=kyurem_noir,tag=retourner,tag=captPokeball] run function give:spawn/kyurem_noir/kyurem_noir_pokeball
execute as @e[tag=kyurem_noir,tag=retourner,tag=captMasterball] run function give:spawn/kyurem_noir/kyurem_noir_masterball
execute as @e[tag=kyurem_noir,tag=retourner,tag=captHonorball] run function give:spawn/kyurem_noir/kyurem_noir_honorball
execute as @e[tag=kyurem_noir,tag=retourner,tag=captSuperball] run function give:spawn/kyurem_noir/kyurem_noir_superball
execute as @e[tag=kyurem_noir,tag=retourner,tag=captHyperball] run function give:spawn/kyurem_noir/kyurem_noir_hyperball

execute as @e[tag=zekrom,tag=retourner,tag=captPokeball] run function give:spawn/zekrom/zekrom_pokeball
execute as @e[tag=zekrom,tag=retourner,tag=captMasterball] run function give:spawn/zekrom/zekrom_masterball
execute as @e[tag=zekrom,tag=retourner,tag=captHonorball] run function give:spawn/zekrom/zekrom_honorball
execute as @e[tag=zekrom,tag=retourner,tag=captSuperball] run function give:spawn/zekrom/zekrom_superball
execute as @e[tag=zekrom,tag=retourner,tag=captHyperball] run function give:spawn/zekrom/zekrom_hyperball

execute as @e[tag=necrozma,tag=retourner,tag=captPokeball] run function give:spawn/necrozma/necrozma_pokeball
execute as @e[tag=necrozma,tag=retourner,tag=captMasterball] run function give:spawn/necrozma/necrozma_masterball
execute as @e[tag=necrozma,tag=retourner,tag=captHonorball] run function give:spawn/necrozma/necrozma_honorball
execute as @e[tag=necrozma,tag=retourner,tag=captSuperball] run function give:spawn/necrozma/necrozma_superball
execute as @e[tag=necrozma,tag=retourner,tag=captHyperball] run function give:spawn/necrozma/necrozma_hyperball

execute as @e[tag=coatox,tag=retourner,tag=captPokeball] run function give:spawn/coatox/coatox_pokeball
execute as @e[tag=coatox,tag=retourner,tag=captMasterball] run function give:spawn/coatox/coatox_masterball
execute as @e[tag=coatox,tag=retourner,tag=captHonorball] run function give:spawn/coatox/coatox_honorball
execute as @e[tag=coatox,tag=retourner,tag=captSuperball] run function give:spawn/coatox/coatox_superball
execute as @e[tag=coatox,tag=retourner,tag=captHyperball] run function give:spawn/coatox/coatox_hyperball

execute as @e[tag=lunala,tag=retourner,tag=captPokeball] run function give:spawn/lunala/lunala_pokeball
execute as @e[tag=lunala,tag=retourner,tag=captMasterball] run function give:spawn/lunala/lunala_masterball
execute as @e[tag=lunala,tag=retourner,tag=captHonorball] run function give:spawn/lunala/lunala_honorball
execute as @e[tag=lunala,tag=retourner,tag=captSuperball] run function give:spawn/lunala/lunala_superball
execute as @e[tag=lunala,tag=retourner,tag=captHyperball] run function give:spawn/lunala/lunala_hyperball

execute as @e[tag=necrozma_ailes,tag=retourner,tag=captPokeball] run function give:spawn/necrozma_ailes/necrozma_ailes_pokeball
execute as @e[tag=necrozma_ailes,tag=retourner,tag=captMasterball] run function give:spawn/necrozma_ailes/necrozma_ailes_masterball
execute as @e[tag=necrozma_ailes,tag=retourner,tag=captHonorball] run function give:spawn/necrozma_ailes/necrozma_ailes_honorball
execute as @e[tag=necrozma_ailes,tag=retourner,tag=captSuperball] run function give:spawn/necrozma_ailes/necrozma_ailes_superball
execute as @e[tag=necrozma_ailes,tag=retourner,tag=captHyperball] run function give:spawn/necrozma_ailes/necrozma_ailes_hyperball

execute as @e[tag=necrozma_criniere,tag=retourner,tag=captPokeball] run function give:spawn/necrozma_criniere/necrozma_criniere_pokeball
execute as @e[tag=necrozma_criniere,tag=retourner,tag=captMasterball] run function give:spawn/necrozma_criniere/necrozma_criniere_masterball
execute as @e[tag=necrozma_criniere,tag=retourner,tag=captHonorball] run function give:spawn/necrozma_criniere/necrozma_criniere_honorball
execute as @e[tag=necrozma_criniere,tag=retourner,tag=captSuperball] run function give:spawn/necrozma_criniere/necrozma_criniere_superball
execute as @e[tag=necrozma_criniere,tag=retourner,tag=captHyperball] run function give:spawn/necrozma_criniere/necrozma_criniere_hyperball

execute as @e[tag=ultra_necrozma,tag=retourner,tag=captPokeball,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_pokeball
execute as @e[tag=ultra_necrozma,tag=retourner,tag=captMasterball,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_masterball
execute as @e[tag=ultra_necrozma,tag=retourner,tag=captHonorball,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_honorball
execute as @e[tag=ultra_necrozma,tag=retourner,tag=captSuperball,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_superball
execute as @e[tag=ultra_necrozma,tag=retourner,tag=captHyperball,tag=criniere] run function give:spawn/necrozma_criniere/necrozma_criniere_hyperball

execute as @e[tag=ultra_necrozma,tag=retourner,tag=captPokeball,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_pokeball
execute as @e[tag=ultra_necrozma,tag=retourner,tag=captMasterball,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_masterball
execute as @e[tag=ultra_necrozma,tag=retourner,tag=captHonorball,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_honorball
execute as @e[tag=ultra_necrozma,tag=retourner,tag=captSuperball,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_superball
execute as @e[tag=ultra_necrozma,tag=retourner,tag=captHyperball,tag=ailes] run function give:spawn/necrozma_ailes/necrozma_ailes_hyperball

execute as @e[tag=crefadet,tag=retourner,tag=captPokeball] run function give:spawn/crefadet/crefadet_pokeball
execute as @e[tag=crefadet,tag=retourner,tag=captMasterball] run function give:spawn/crefadet/crefadet_masterball
execute as @e[tag=crefadet,tag=retourner,tag=captHonorball] run function give:spawn/crefadet/crefadet_honorball
execute as @e[tag=crefadet,tag=retourner,tag=captSuperball] run function give:spawn/crefadet/crefadet_superball
execute as @e[tag=crefadet,tag=retourner,tag=captHyperball] run function give:spawn/crefadet/crefadet_hyperball

execute as @e[tag=crefollet,tag=retourner,tag=captPokeball] run function give:spawn/crefollet/crefollet_pokeball
execute as @e[tag=crefollet,tag=retourner,tag=captMasterball] run function give:spawn/crefollet/crefollet_masterball
execute as @e[tag=crefollet,tag=retourner,tag=captHonorball] run function give:spawn/crefollet/crefollet_honorball
execute as @e[tag=crefollet,tag=retourner,tag=captSuperball] run function give:spawn/crefollet/crefollet_superball
execute as @e[tag=crefollet,tag=retourner,tag=captHyperball] run function give:spawn/crefollet/crefollet_hyperball

execute as @e[tag=crehelf,tag=retourner,tag=captPokeball] run function give:spawn/crehelf/crehelf_pokeball
execute as @e[tag=crehelf,tag=retourner,tag=captMasterball] run function give:spawn/crehelf/crehelf_masterball
execute as @e[tag=crehelf,tag=retourner,tag=captHonorball] run function give:spawn/crehelf/crehelf_honorball
execute as @e[tag=crehelf,tag=retourner,tag=captSuperball] run function give:spawn/crehelf/crehelf_superball
execute as @e[tag=crehelf,tag=retourner,tag=captHyperball] run function give:spawn/crehelf/crehelf_hyperball

execute as @e[tag=kyurem_blanc,tag=retourner,tag=captPokeball] run function give:spawn/kyurem_blanc/kyurem_blanc_pokeball
execute as @e[tag=kyurem_blanc,tag=retourner,tag=captMasterball] run function give:spawn/kyurem_blanc/kyurem_blanc_masterball
execute as @e[tag=kyurem_blanc,tag=retourner,tag=captHonorball] run function give:spawn/kyurem_blanc/kyurem_blanc_honorball
execute as @e[tag=kyurem_blanc,tag=retourner,tag=captSuperball] run function give:spawn/kyurem_blanc/kyurem_blanc_superball
execute as @e[tag=kyurem_blanc,tag=retourner,tag=captHyperball] run function give:spawn/kyurem_blanc/kyurem_blanc_hyperball

execute at @e[tag=retourner] run playsound minecraft:item.bottle.fill_dragonbreath record @a[distance=..10]

kill @e[tag=retourner]